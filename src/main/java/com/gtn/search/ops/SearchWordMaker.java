package com.gtn.search.ops;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class SearchWordMaker {

	public static void main(String[] args) {
		String[] urlWords = {"https://s3.us-east-2.amazonaws.com/askbookman/words_spell_checked.txt"
		,"https://en.wikipedia.org/wiki/Cannabis_dispensaries_in_the_United_States"		
		,"https://en.wikipedia.org/wiki/San_Francisco_Cannabis_Buyers_Club"
		, "https://en.wikipedia.org/wiki/Cannabis"
		,"https://en.wikipedia.org/wiki/Cannabis_(drug)"
		,"http://localhost:8080/pronouns_not.txt"
		};
		HashMap<String, String> wordMap = new HashMap<String, String>();
		HashMap<String, String> wordMapRemove = new HashMap<String, String>();
		SearchWordMaker tmpmaker = new SearchWordMaker();
		for(int j=0;j<urlWords.length;j++){
			wordMap = tmpmaker.getWords(wordMap, urlWords[j]);
		}
		String[] uliWordsRemove = {"http://localhost:8080/pronouns.txt"};
		for(int j=0;j<uliWordsRemove.length;j++){
			wordMapRemove = tmpmaker.getWords(wordMapRemove, uliWordsRemove[j]);
		}	
		
        for (Map.Entry<String,String> entry : wordMapRemove.entrySet())  {
            if(wordMap.containsKey(entry.getKey().toLowerCase()))wordMap.remove(entry.getKey());
        }
		HashMap<String, String> wordMapSeed = new HashMap<String, String>();
        for (Map.Entry<String,String> entry : wordMap.entrySet())  {
        	if(entry.getKey().endsWith("s") && !wordMap.containsKey(entry.getKey().substring(0, entry.getKey().length()-1))){
        		wordMapSeed.put(entry.getKey(), entry.getKey());
        	}else if(!entry.getKey().endsWith("s")){
        		wordMapSeed.put(entry.getKey(), entry.getKey());
        	}
        	
        }
        String tmpJson = "";
        for (Map.Entry<String,String> entry : wordMapSeed.entrySet())  {
        	tmpJson = tmpJson + TypeAhead.camelCase(entry.getKey()) + " "; 
        }
        System.out.println(tmpJson);
        System.out.println(wordMap.size());
        System.out.println(wordMapSeed.size());
        try{
	       BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/armenmerikyan/Desktop/gettalentnow/get-talent-now/words_weed.txt"));
	       writer.write(tmpJson);
	       writer.close();		
        }catch(Exception Ex){
        	Ex.printStackTrace();
        }      
        
	}

	public HashMap<String, String> getWordsFromString(HashMap<String, String> wordMap, String words) {
	
		try{
			String[] specialRickyIs = {""};
	        String[] alphaList = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
	        words = words.replace(System.lineSeparator(), " ");
	        words = words.replace(".", " ");
	        words = words.replace(",", " ");
	        words = words.replace("?", " ");
	        words = words.replace(":", " ");
	        words = words.replace(";", " ");
	        words = words.replace("!", " ");
	        words = words.replace(System.lineSeparator(), " ");
	        String[] wordList =  words.split(" ");
	        for(int j=0;j<wordList.length;j++){
	        	boolean isGoodWord = true;
	        	for(int k=0;k<specialRickyIs.length;k++){
	        		if(wordList[j].equals(specialRickyIs[k])){
	        			isGoodWord = false;
	        		}
	        	}		
	        	for(int c=0;c<wordList[j].length();c++){
	        		boolean isCEnglish = false;
	        		System.out.println(wordList[j]);
		        	for(int k=0;k<alphaList.length;k++){
		        		if(wordList[j].toLowerCase().charAt(c) == alphaList[k].charAt(0)){
		        			isCEnglish = true;
		        		}
		        	}
		        	if(!isCEnglish)isGoodWord = false;
	        	}
	        	if(wordList[j].length()<2 || wordList[j].length()>15)isGoodWord = false;
	        	
		        if(isGoodWord)wordMap.put(wordList[j].toLowerCase(), wordList[j].toLowerCase()); 
		        
	        }
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}  
		return wordMap;
	}

	public HashMap<String, String> getWords(HashMap<String, String> wordMap, String urlWords) {
		// TODO Auto-generated method stub
		// https://en.wikipedia.org/wiki/List_of_conspiracy_theories
		try{	
			URL oracle = new URL(urlWords);
	        BufferedReader in = new BufferedReader(
	        new InputStreamReader(oracle.openStream()));
	        String inputLine;
	        String searchResult = "";
	        while ((inputLine = in.readLine()) != null){
	        	searchResult = searchResult + inputLine + " ";
	        }
	        in.close();	
	        wordMap =  this.getWordsFromString(wordMap, searchResult);
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}        
		return wordMap;
	}

}
