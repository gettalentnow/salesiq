package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA 
 *  Armen Merikyan
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.UserManagerInstagram;
import com.big.search.maryjane.persistence.dynamodb.UserManagerTwitter;

public class WebsiteCrawlerTwitterLikeAccount extends WebsiteCrawlerTwitterBase{
	private int currentAccount = 0;
	public WebsiteCrawlerTwitterLikeAccount() {
		super();
	}
	public static void main(String[] args) {
		WebsiteCrawlerTwitterLikeAccount web = new WebsiteCrawlerTwitterLikeAccount();		
		for(web.currentAccount=0;web.currentAccount<Constant.twitterAccounts.length;web.currentAccount++){
			web.getStart();
		}
	}
    public void getURLContent() {
    	System.out.println(this.getBaseUrl() + "/"+ Constant.twitterAccounts[this.currentAccount]);	        	
		try{
	    	this.getDriver().get(this.getBaseUrl() + "/"+ Constant.twitterAccounts[this.currentAccount]);  		        		    	
	        this.getDownTonight(); 
	        System.out.println("GET ALL ELEMENTS");
	        List<WebElement> element = this.getDriver().findElements(By.tagName("button"));		 
	        int count =0;
	        for (WebElement temp : element) {
				try{
					count +=1;
	        		System.out.println(temp.getAttribute("aria-describedby"));
		        	if(temp!=null && temp.getAttribute("aria-describedby")!=null && temp.getAttribute("aria-describedby").startsWith("profile-tweet-action-favorite-count-")){	       		    	        
			        		temp.click();
			        		System.out.println("like clicked");
			    	        Thread.sleep(200);	
		        	}	 
		        	if(count>400)break;
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }	        
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
}
