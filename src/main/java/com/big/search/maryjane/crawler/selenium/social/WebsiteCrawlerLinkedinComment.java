package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA 
 *  
 */

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.askweedman.maryjane.util.Names;

public class WebsiteCrawlerLinkedinComment extends WebsiteCrawlerLinkedinBase{
	private Names namesGnr ;
	public Names getNamesGnr() {
		return namesGnr;
	}
	public void setNamesGnr(Names namesGnr) {
		this.namesGnr = namesGnr;
	}
	public WebsiteCrawlerLinkedinComment() {
		// TODO Auto-generated constructor stub
		super();
		namesGnr = new Names();
	}
	public static void main(String[] args) {
    		
		WebsiteCrawlerLinkedinComment web = new WebsiteCrawlerLinkedinComment();		
	    web.getStart();
	}
    public void getURLContent() {
    	System.out.println(this.getBaseUrl());	        	
		try{
	    	this.getDriver().get(this.getBaseUrl() + "/feed/"); 
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };
	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 30);
	        wait.until(pageLoadCondition);
	        Thread.sleep(1000);	
	        
	        this.getDownTonight();

	        List<WebElement> element = this.getDriver().findElements(By.tagName("button"));	
	        System.out.println("GET ALL ELEMENTS");		 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("data-control-name"));
		        	if(temp!=null && temp.getAttribute("data-control-name")!=null && temp.getAttribute("data-control-name").startsWith("comment")){	       		    	        
		        			String tmpMsg = this.getNamesGnr().getNamesRandom() + "!!! this is ...";
		        			temp.click();
		        	        WebElement tmpElement =  this.getDriver().switchTo().activeElement();
		        	        if(!temp.getText().equals(tmpMsg) && !tmpElement.getText().equals(tmpMsg)){
			        	        tmpElement.sendKeys("check this out " + this.getNamesGnr().getNamesRandom() + "!!!");	
			        	        tmpElement.sendKeys(Keys.TAB);			        	        
			        	        wait.until(pageLoadCondition);	        	        	
		        	        }
		        			wait.until(pageLoadCondition);
		        	        Thread.sleep(2000);
			        		System.out.println("comment clicked");
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }	        
	        element = this.getDriver().findElements(By.tagName("button"));	
	        System.out.println("GET ALL ELEMENTS");		 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getText());
		        	if(temp!=null && temp.getText()!=null && temp.getText().startsWith("Post")){	       		    	        
		        			temp.click();
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }	        
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
}
