package com.big.search.maryjane.persistence.dynamodb;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

public class DomainManagerOutputNew {
	public static String outputFileName = "/Users/armenmerikyan/Desktop/gettalentnow/get-talent-now/src/main/webapp/data/domains_new.txt";
	public DomainManagerOutputNew() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DomainManager tmpMngr = new DomainManager();
		HashMap<String, String> tmpYes = tmpMngr.getDomainsIsRelevantYes();
		HashMap<String, String> tmpNo = tmpMngr.getDomainsIsRelevantNo();
		HashMap<String,String> tmpMap  =  tmpMngr.getDomainsNew();
		StringBuffer tmpbuf = new StringBuffer();
        for (Map.Entry<String,String> entry : tmpMap.entrySet())  {		
        	if(!tmpYes.containsKey(entry.getValue()) && !tmpNo.containsKey(entry.getValue()))
        	tmpbuf.append(entry.getValue() + System.lineSeparator());
        }
        try{
	        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFileName));
	        writer.write(tmpbuf.toString());
	        writer.close();		
        }catch(Exception Ex){
        	Ex.printStackTrace();
        }        
	}

}
