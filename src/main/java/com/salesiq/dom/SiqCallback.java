package com.salesiq.dom;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SiqCallback {
	private String siq_id ;
	private String name;
	private String desc;
	private Date date;
	private Timestamp ts;
	private String cust_id;
	private String user_id;
	private String bus_id;

	public SiqCallback() {
		// TODO Auto-generated constructor stub
	}

	public String getSiq_id() {
		return siq_id;
	}

	public void setSiq_id(String siq_id) {
		this.siq_id = siq_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Date getDate() {
		return date;
	}
	
	public String getDateString() {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy hh:mm a");
		return formatter.format(date);
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getBus_id() {
		return bus_id;
	}

	public void setBus_id(String bus_id) {
		this.bus_id = bus_id;
	}

	public String getTsString() {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy hh:mm a");
		return formatter.format(new Date(ts.getTime()));
	}
	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

}
