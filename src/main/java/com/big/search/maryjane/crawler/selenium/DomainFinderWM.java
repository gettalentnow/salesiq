package com.big.search.maryjane.crawler.selenium;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.gtn.search.dynamodb.WebSite_Index;
import com.gtn.search.ops.SearchWordMaker;
import com.gtn.search.ops.TypeAhead;
import com.gtn.search.sel.SeleniumWeb;
public class DomainFinderWM {
	public WebDriver driver = null;
	public String rootURL = "https://weedmaps.com";

	public DomainFinderWM() {
		// TODO Auto-generated constructor stub
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
		this.driver = new FirefoxDriver();
	}

	public static void main(String[] args) {
	  	
    	DomainFinderWM web = new DomainFinderWM();
    	web.getURLContent(web.rootURL);
    }
	    
	public void getURLContent(String url) {
	    		
	        	System.out.println("Start Get URL : " + url);	        	
				try{
			    	this.driver.get(url);  	        		    	
			        ExpectedCondition<Boolean> pageLoadCondition = new
			                ExpectedCondition<Boolean>() {
			                    public Boolean apply(WebDriver driver) {
			                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
			                    }
			                };	  
			        WebDriverWait wait = new WebDriverWait(this.driver, 30);
			        wait.until(pageLoadCondition);
			        Thread.sleep(3000);		        
			        List<WebElement> elementExpand = this.driver.findElements(By.tagName("button"));
			        int btnCount = elementExpand.size();
			        int btnCurrent = 0;
			        while(btnCurrent<btnCount){
				    	this.driver.get(url);  	
			        	elementExpand = this.driver.findElements(By.tagName("button"));
			        	int j=0;
				        for (WebElement temp : elementExpand) {
				        	if(j==btnCurrent){
				        		btnCurrent+=1;
				        		temp.click();      		    	
				        		break;
				        	}
				        	j+=1;
				        }
			        }
			             
				}catch(Exception ex){
					ex.printStackTrace();
				}
	}
}
