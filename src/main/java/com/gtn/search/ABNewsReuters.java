package com.gtn.search;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Vector;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

public class ABNewsReuters {


	public static Vector<NewsItem> searchString(Vector<NewsItem> newsItemList) {
        String urlListReturn ="";
		try{
		// TODO Auto-generated method stub
		URL oracle = new URL("http://feeds.reuters.com/reuters/topNews");
        BufferedReader in = new BufferedReader(
        new InputStreamReader(oracle.openStream()));
        String inputLine;
        String searchResult = "";
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
        // read the XML document
        String abn_title = "";
        String abn_des ="";
        String abn_link = "";
        while (eventReader.hasNext()) {
            XMLEvent event = eventReader.nextEvent();
            if (event.isStartElement()) {
            	String localPart = event.asStartElement().getName().getLocalPart();
            	//System.out.println(localPart);
            	if(localPart.equals("title")){
            		abn_title = ABNewsReuters.getCharacterData(event, eventReader);
            	}
            	if(localPart.equals("description")){
            		abn_des = ABNewsReuters.getCharacterData(event, eventReader);
            	}
            	if(localPart.equals("link")){
            		abn_link = ABNewsReuters.getCharacterData(event, eventReader);
            		if(!localPart.equals("") && !abn_title.equals("") && !abn_des.equals("")
            				&& !abn_title.startsWith("Reuters News") && !abn_des.startsWith("null")){
	            		urlListReturn = urlListReturn  
	            		+ "<a href=\"" + abn_link
	        		    + "\" >" + abn_title + "</a>" 
	        		    + "<BR>" + abn_des  + "<BR><BR>";       
	            		NewsItem ni = new NewsItem();
	            		ni.setLink(abn_link);
	            		ni.setName(abn_title);
	            		ni.setDesc(abn_des);
	            		ni.setSource("REUTERS");
	            		newsItemList.add(ni);
            		}            		
            	}
            }	
        }
        
//---        	TalentManager.logDynamoDb(searchString, user_ip);
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		//System.out.println(urlListReturn);
		return newsItemList;
	}
	private static String getCharacterData(XMLEvent event, XMLEventReader eventReader)
            throws XMLStreamException {
        String result = "";
        event = eventReader.nextEvent();
        if (event instanceof Characters) {
            result = event.asCharacters().getData();
        }
        return result;
    }	
	public static void main(String[] args) {
		try{
			 String urlListReturn = "";
			 Vector<NewsItem> shuffledList = ABNewsReuters.searchString(new Vector<NewsItem>());
			 for(int j=0;j<shuffledList.size();j++){
				 NewsItem tmpItem = shuffledList.elementAt(j);
		     		urlListReturn = urlListReturn  
		     		+ "<a href=\"" + tmpItem.getLink()
		 		    + "\" >" + tmpItem.getName() + "</a>" 
		 		    + "<BR>" + tmpItem.getDesc()  + "<BR><BR>"; 			 
			 }	
			 System.out.println(urlListReturn);
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}

}
