package com.salesiq.mysql.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.UUID;
import java.util.Vector;

import com.salesiq.dom.SiqCallback;
import com.salesiq.dom.SiqCustomer;

public class CustomerManager {

	public CustomerManager() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CustomerManager tmp = new CustomerManager();
		//tmp.addCustomer("armen+8588588888+None", "test");
		Vector<SiqCustomer> tmplist = tmp.getCustomers("fa0bd40c-eb36-481b-ae4f-6e44373fd233", false);
		for(int j=0;j<tmplist.size();j++){
			SiqCustomer tmpcb = tmplist.elementAt(j);
			System.out.println("HELL O " + tmpcb.getName());
			System.out.println("HELL O " + tmpcb.getTsString());
		}
	}

	public String addCustomer(String delimatedStr, String siq_user_id){
		String[] delimated = delimatedStr.split("\\+");
		return addCustomer(delimated[0].trim(),delimated[1].trim(),delimated[2].trim(), delimated[3].trim(),siq_user_id);
	}
	public String addCustomer(String custName, String custPhone, String custStatus, String custDesc, String siq_user_id){
		SiqCustomer tmpCust = new SiqCustomer();
		tmpCust.setName(custName);
		tmpCust.setPhone(custPhone);
		tmpCust.setStatus(custStatus);
		tmpCust.setDesc(custDesc);
		tmpCust.setUser_id(siq_user_id);
		return insertCustomer(tmpCust);
	}
	public SiqCustomer getCustomer(String siq_user_id, String siq_cust_id){
	    try
	    {

	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	     // String query = " insert into siq_customers (siq_cust_id, siq_cust_name, siq_cust_phone,
	      //siq_cust_status, siq_user_id, siq_bus_id)"
	      //  + " values (?, ?, ?, ?, ?, ?)";

	      String query = "select * from siq_customers where siq_customers.siq_user_id = ? and siq_cust_id = ?";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      preparedStmt.setString (2, siq_cust_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqCustomer tmpCust = new SiqCustomer();
	    	  tmpCust.setName(resultSet.getString("siq_cust_name"));
	    	  tmpCust.setPhone(resultSet.getString("siq_cust_phone"));
	    	  tmpCust.setStatus(resultSet.getString("siq_cust_status"));
	    	  tmpCust.setDesc(resultSet.getString("siq_cust_desc"));
	    	  tmpCust.setSiq_id(resultSet.getString("siq_cust_id"));
	    	  return tmpCust;
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return null;
	}
	public Vector<SiqCustomer> getCustomers(String siq_user_id, String filterByStatus){
		Vector<SiqCustomer> custList = new Vector<SiqCustomer>();
	    try
	    {
	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_customers where siq_customers.siq_user_id = ? and siq_customers.siq_cust_status = ? order by ts desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      preparedStmt.setString (2, filterByStatus);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqCustomer tmpCust = new SiqCustomer();
	    	  tmpCust.setName(resultSet.getString("siq_cust_name"));
	    	  tmpCust.setPhone(resultSet.getString("siq_cust_phone"));
	    	  tmpCust.setStatus(resultSet.getString("siq_cust_status"));
	    	  tmpCust.setSiq_id(resultSet.getString("siq_cust_id"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  tmpCust.setTs_update(resultSet.getTimestamp("ts_update"));
	    	  custList.add(tmpCust);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return custList;
	}
	public Vector<SiqCustomer> getCustomersMDM(String siq_user_id){
		Vector<SiqCustomer> custList = new Vector<SiqCustomer>();
	    try
	    {
	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
 	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_customers where siq_customers.siq_user_id = ? order by ts asc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqCustomer tmpCust = new SiqCustomer();
	    	  tmpCust.setName(resultSet.getString("siq_cust_name"));
	    	  tmpCust.setPhone(resultSet.getString("siq_cust_phone"));
	    	  tmpCust.setStatus(resultSet.getString("siq_cust_status"));
	    	  tmpCust.setSiq_id(resultSet.getString("siq_cust_id"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  tmpCust.setTs_update(resultSet.getTimestamp("ts_update"));
	    	  custList.add(tmpCust);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return custList;
	}
	public Vector<SiqCustomer> getCustomers(String siq_user_id, boolean sortByTs_update){
		Vector<SiqCustomer> custList = new Vector<SiqCustomer>();
	    try
	    {
	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_customers where siq_customers.siq_user_id = ? order by ts desc";
	      if(sortByTs_update)query = "select * from siq_customers where siq_customers.siq_user_id = ? order by ts_update desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqCustomer tmpCust = new SiqCustomer();
	    	  tmpCust.setName(resultSet.getString("siq_cust_name"));
	    	  tmpCust.setPhone(resultSet.getString("siq_cust_phone"));
	    	  tmpCust.setStatus(resultSet.getString("siq_cust_status"));
	    	  tmpCust.setSiq_id(resultSet.getString("siq_cust_id"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  tmpCust.setTs_update(resultSet.getTimestamp("ts_update"));
	    	  custList.add(tmpCust);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return custList;
	}

	public String editCustomer(String custId, String custName, String custPhone, String custStatus, String custDesc, String siq_user_id){
		SiqCustomer tmpCust = new SiqCustomer();
		tmpCust.setSiq_id(custId);
		tmpCust.setName(custName);
		tmpCust.setPhone(custPhone);
		tmpCust.setStatus(custStatus);
		tmpCust.setDesc(custDesc);
		tmpCust.setUser_id(siq_user_id);
		return updateCustomer(tmpCust);
	}
	
	
	public String removeCustomer(String user_id, String cust_id){
		try{
		      MySQLConstants tmpCon = new MySQLConstants();
		      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
		      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
		      String query = "delete from siq_customers where siq_customers.siq_cust_id = ? and siq_customers.siq_user_id = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, cust_id);
		      preparedStmt.setString (2, user_id);
		      preparedStmt.execute();		      
		      preparedStmt.close();
		      conn.close();
		      
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      return "Error: System Error";
	    }
		return "Customer Removed";
	}
	public String updateCustomer(SiqCustomer newCust){
		try{
		      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
		      MySQLConstants tmpCon = new MySQLConstants();
		      // create a mysql database connection
		      //String myDriver = "org.gjt.mm.mysql.Driver";
		      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
		      //Class.forName(myDriver);
		      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
		      String query = "update siq_customers set siq_cust_name = ?, siq_cust_phone = ?, siq_cust_status = ?, siq_cust_desc = ?, ts_update = ?"
		        + " where siq_customers.siq_cust_id = ? and siq_customers.siq_user_id = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, newCust.getName());
		      preparedStmt.setString (2, newCust.getPhone());
		      preparedStmt.setString (3, newCust.getStatus());
		      preparedStmt.setString (4, newCust.getDesc());
		      preparedStmt.setTimestamp(5, currentTimestamp);
		      preparedStmt.setString (6, newCust.getSiq_id());
		      preparedStmt.setString (7, newCust.getUser_id());
		      preparedStmt.execute();
		      preparedStmt.close();
		      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      return "Error: System Error";
	    }
		return "Customer Added";
	}
	public String insertCustomer(SiqCustomer newCust){
	    try
	    {

	      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = " insert into siq_customers (siq_cust_id, siq_cust_name, siq_cust_phone, siq_cust_status, siq_user_id, siq_bus_id, ts, siq_cust_desc, ts_update)"
	        + " values (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, UUID.randomUUID().toString());
	      preparedStmt.setString (2, newCust.getName());
	      preparedStmt.setString (3, newCust.getPhone());
	      preparedStmt.setString (4, newCust.getStatus());
	      preparedStmt.setString (5, newCust.getUser_id());
	      preparedStmt.setString (6, newCust.getBus_id());
	      preparedStmt.setTimestamp(7, currentTimestamp);
	      preparedStmt.setString (8, newCust.getDesc());
	      preparedStmt.setTimestamp(9, currentTimestamp);
	      preparedStmt.execute();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error " + e.getMessage();
	      return "Error: System Error ";
	    }
		return "Customer Added";
	}
}
