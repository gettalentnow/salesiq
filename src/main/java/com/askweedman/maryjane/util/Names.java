package com.askweedman.maryjane.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.big.search.maryjane.persistence.dynamodb.DataFirstNames;
import com.big.search.maryjane.persistence.dynamodb.DataLastNames;
import com.big.search.maryjane.persistence.dynamodb.UserManagerYoutube;
import com.big.search.maryjane.persistence.dynamodb.VideoManager;
import com.big.search.maryjane.persistence.dynamodb.VideoManagerOther;

public class Names {
	private DataFirstNames fnMgr = null;
	private DataLastNames lnMgr = null;
	private ArrayList<String> fnList = null;
	private ArrayList<String> lnList = null;

	public Names() {
		// TODO Auto-generated constructor stub
		fnMgr = new DataFirstNames();
		lnMgr = new DataLastNames();
		fnList = fnMgr.getAll();
		lnList = lnMgr.getAll();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Names tmpNames = new Names();
		for(int j=0;j<100;j++){
			System.out.println(tmpNames.getNameRandom());
			System.out.println(tmpNames.getNameRandomAsk());
			System.out.println(tmpNames.getNameRandomWeedman());
		}
	}

	public String getNamesRandom() {
		Random rand = new Random(); 
		int fvalue1 = rand.nextInt(this.fnList.size());
		int fvalue2 = rand.nextInt(this.fnList.size());
		int lvalue = rand.nextInt(this.lnList.size());		
		return this.fnList.get(fvalue2) + " and " + 
				this.fnList.get(fvalue1) + " " + this.lnList.get(lvalue);
	}
	public String getNameRandom() {
		Random rand = new Random(); 
		int fvalue = rand.nextInt(this.fnList.size());
		int lvalue = rand.nextInt(this.lnList.size());		
		return this.fnList.get(fvalue) + " " + this.lnList.get(lvalue);
	}
	public String getNameRandomAsk() {
		Random rand = new Random(); 
		int lvalue = rand.nextInt(this.lnList.size());		
		return "Ask " + this.lnList.get(lvalue);
	}
	public String getNameRandomWeedman() {
		Random rand = new Random(); 
		int fvalue = rand.nextInt(this.fnList.size());
		return this.fnList.get(fvalue) + " Weedman";
	}
	public DataFirstNames getFnMgr() {
		return fnMgr;
	}

	public void setFnMgr(DataFirstNames fnMgr) {
		this.fnMgr = fnMgr;
	}

	public DataLastNames getLnMgr() {
		return lnMgr;
	}

	public void setLnMgr(DataLastNames lnMgr) {
		this.lnMgr = lnMgr;
	}

	public ArrayList<String> getFnList() {
		return fnList;
	}

	public void setFnList(ArrayList<String> fnList) {
		this.fnList = fnList;
	}

	public ArrayList<String> getLnList() {
		return lnList;
	}

	public void setLnList(ArrayList<String> lnList) {
		this.lnList = lnList;
	}
	

}
