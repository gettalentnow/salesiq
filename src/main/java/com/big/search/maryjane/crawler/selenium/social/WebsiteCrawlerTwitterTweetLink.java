package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA 
 *  
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.big.search.maryjane.persistence.dynamodb.UserManagerTwitter;

public class WebsiteCrawlerTwitterTweetLink extends WebsiteCrawlerTwitterBase{
	private DomainManager dmMngr = null;
	private HashMap<String, String> isRelevantYes = null;
	private String hashtag = " #420 #weed #dispensary #cannabis #maryjane #askweedman #travel #harb #weedlover #hemp #highlife #cannabiscommunity #weedlife #growyourown #homegrown #gardening #stoner #sativa #indica #garden #cbd #cannabisculture #kush";
	public WebsiteCrawlerTwitterTweetLink() {
		// TODO Auto-generated constructor stub
		this.dmMngr = new DomainManager();
		this.isRelevantYes = dmMngr.getDomainsIsRelevantYes();		

		
		
	}
	public static void main(String[] args) {

		WebsiteCrawlerTwitterTweetLink web = new WebsiteCrawlerTwitterTweetLink();		
		web.getStart();
	}

	public void getStart(){
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
		this.setUsrMngr(new UserManagerTwitter());
		this.setUsrMap(this.getUsrMngr().getAllUsers());
		
		for (Map.Entry<String,String> entry : this.getUsrMap().entrySet())  {	
			this.setDriver(new FirefoxDriver());
	    	this.getLogin(entry.getKey(), entry.getValue());
	        for (Map.Entry<String,String> entryLink : this.isRelevantYes.entrySet())  {	
	        	this.getURLContent( entryLink.getValue());
	        }		
	    	this.getDriver().close();
	    }
	}	
    public void getURLContent() {
    	
    }
    public void getURLContent(String tweetlink ) {
		try{
	    	this.getDriver().get(this.getBaseUrl());  		        		    	
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };
	                
	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 30);
	        wait.until(pageLoadCondition);
	        // wait ten seconds for Capcha test to manually bypass
	        Thread.sleep(20000);		        
	        

	    	
	        List<WebElement> element = this.getDriver().findElements(By.tagName("div"));	
	        
	           
	        
	         
	         
	        
	        // wait ten seconds for Capcha test to manually bypass

	        	        
	        int i = 0;

	        System.out.println("GET ALL ELEMENTS");		 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("id"));
		        	if(temp!=null && temp.getAttribute("id")!=null && temp.getAttribute("id").startsWith("tweet-box-home-timeline")){	       		    	        
		        		//if(i%2==1){
		        			temp.click();
		        			Thread.sleep(1000);
			        		temp.sendKeys(tweetlink +   hashtag);
			        		System.out.println("like clicked");
		        			Thread.sleep(120000);
			    	        break;	
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }

	        element = this.getDriver().findElements(By.tagName("button"));	
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("class"));
		        	if(temp!=null && temp.getAttribute("class")!=null && temp.getAttribute("class").contains("js-tweet-btn")){	       		    	        
	        			temp.click();

	        	        Thread.sleep(200000);		        
	        			break;
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }		        		
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
}
