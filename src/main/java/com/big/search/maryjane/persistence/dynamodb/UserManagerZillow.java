package com.big.search.maryjane.persistence.dynamodb;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

public class UserManagerZillow {

	public UserManagerZillow() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserManagerZillow tmp = new UserManagerZillow();
		tmp.putDomain("askweedman", "XXXXXXXX");
	}
	public HashMap<String, String> getAllUsers(){
		HashMap<String, String> tmpMap = new HashMap<String, String>();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		Map<String, AttributeValue> lastKeyEvaluated = null;
		do {
		    ScanRequest scanRequest = new ScanRequest()
		        .withTableName("zillow_users")
		        .withLimit(10)
		        .withExclusiveStartKey(lastKeyEvaluated);
	
		    ScanResult result = client.scan(scanRequest);
		    for (Map<String, AttributeValue> item : result.getItems()){
		    		tmpMap.put(item.get("username").getS(), item.get("password").getS());
		    }
		    lastKeyEvaluated = result.getLastEvaluatedKey();
		} while (lastKeyEvaluated != null);
		return tmpMap;		
	}

	public void putDomain(String username, String password) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable("zillow_users");
		Timestamp ts = new Timestamp(new Date().getTime());
		Item item = new Item()
		    .withPrimaryKey("username", username)
		    .withString("password", password)
		    .withString("visit_time", ts + "");
		PutItemOutcome outcome = table.putItem(item);		
	    System.out.println("Put Item Outcome: " + outcome.getPutItemResult());
				
	}  
}
