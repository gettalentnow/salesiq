<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="com.salesiq.dom.SiqStat,com.salesiq.mysql.login.StatsManager,java.util.Vector,com.salesiq.dom.SiqCustomer,com.salesiq.mysql.login.CustomerManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">
      <jsp:include page='navbar_top_siq.jsp' />
      <div class="container">
        <div class="jumbotron bg-white mt-3">
        <div id="content" style="text-align: left">
          <h1>View Customer Stats</h1>
          <a href="salesiq.menu.jsp" >Menu</a><BR>
          <a href="salesiq.view.cust.totals.jsp?recal=true" >Recalculate</a><BR>
          <%
            if(request.getParameter("recal")!=null && request.getParameter("recal").equals("true")){
              StatsManager tmpMgr = new StatsManager();
              %><%=tmpMgr.addStat((String)session.getAttribute("uid"))%><%
            }
            Vector<SiqStat> custStat = new Vector<SiqStat>();
            StatsManager tmpMgr = new StatsManager();
            custStat = tmpMgr.getStats((String)session.getAttribute("uid"));
          %>
          <BR>
          <div class="container">

              <div class="row">
                <div class="col-sm" style="text-align: left">
                  Total
                </div>
                <div class="col-sm">
                  None
                </div>
                <div class="col-sm">
                  Cold
                </div>
                <div class="col-sm">
                  Warm
                </div>
                <div class="col-sm">
                  Hot
                </div>
                <div class="col-sm">
                  Calls
                </div>
                <div class="col-sm">
                  Date
                </div>
              </div>
              <%
              for(int j=0;j<custStat.size();j++){
                SiqStat tmpStat = custStat.elementAt(j);
                %>
                  <div class="row">
                    <div class="col-sm" style="text-align: left">
                      <%=tmpStat.getCust_tot()%>
                    </div>
                    <div class="col-sm">
                      <%=tmpStat.getCust_tot_none()%>
                    </div>
                    <div class="col-sm">
                      <%=tmpStat.getCust_tot_cold()%>
                    </div>
                    <div class="col-sm">
                      <%=tmpStat.getCust_tot_warm()%>
                    </div>
                    <div class="col-sm">
                      <%=tmpStat.getCust_tot_hot()%>
                    </div>
                    <div class="col-sm">
                      <%=tmpStat.getCust_tot_cb()%>
                    </div>
                    <div class="col-sm">
                      <%=tmpStat.getTsString()%>
                    </div>
                  </div>
                <%
              }
              %>
        </div>
      </div>
      </div>
    </div>
  </body>
</html>
