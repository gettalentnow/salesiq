package com.salesiq.ops;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


public class ProcessLeads {

	public ProcessLeads() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		ProcessLeads ps = new ProcessLeads();
		System.out.println("<sometext>\n\r\n<someothertext>".split("(\r\n|\r|\n)", -1).length);

		String rawInput = "<sometext>\n\r\n<someothertext>";
		String[] rawInputArray =rawInput.replace("\r\n", "\n")
				.replace("\r", "\n")
				.split("\n", -1);;
		System.out.println(rawInputArray.length);
		System.out.print(ps.processLeads(null, "UTF-8"));
	}
	public String processLeads(String rawInput, String encoding) {
		// TODO Auto-generated method stub
		String formatedOutput ="";
		try{	
			ArrayList<String> customerListRaw = new ArrayList<String>();
			String strLine;
			String name = "";
			if(rawInput==null){
				FileInputStream fstream = new FileInputStream("/Users/armenmerikyan/Desktop/salescal/salesiq/dataLoad.txt");
//				FileInputStream fstream = new FileInputStream("/Users/armenmerikyan/Downloads/text.txt");
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream, encoding));
				while ((strLine = br.readLine()) != null)   {
					System.out.println(strLine);
					name += " " +strLine.trim();
					if(strLine.contains("WEBSITE") || strLine.contains("DIRECTIONS") || strLine.contains("Website") || strLine.contains("Direction")){
						customerListRaw.add(name.trim());
						name="";
					}
				}
				fstream.close();
			}else{
				String[] rawInputArray =rawInput.replace("\r\n", "\n")
						.replace("\r", "\n")
						.replace("Website", "WEBSITE")
						.replace("Directions", "DIRECTIONS")
						.split("\n", -1);;
				for(int k =0;k<rawInputArray.length;k++){
					strLine = rawInputArray[k];
					name += " " +strLine.trim();
					if(strLine.contains("WEBSITE") || strLine.contains("DIRECTIONS")){
						customerListRaw.add(name.trim());
						name="";
					}
				}
			}
	        HashMap<String, String> reviewCode = getReviewCode();
	       
	        
	        
			

			ArrayList<String> customerList = new ArrayList<String>();
			for(int k =0;k<customerListRaw.size();k++){
				if(!customerListRaw.get(k).equals("DIRECTIONS")){
					customerList.add(customerListRaw.get(k));
				}else {
					customerList.add(k-1, customerList.get(k-1) + " " + customerListRaw.get(k));
					
				}
			}
			for(int k =1;k<customerList.size();k++){
				if(customerList.get(k-1).startsWith(customerList.get(k))) customerList.remove(k);
			}

			ArrayList<String> clClean = new ArrayList<String>();
			for(int k =0;k<customerList.size();k++){
				String[] splitNumber = customerList.get(k).split("((\\(\\d{3}\\) ?)|(\\d{3}-))?\\d{3}-\\d{4}");
				
				if(splitNumber.length>2){
					String tmpLI = customerList.get(k);
					for(int g=0;g<splitNumber.length-1;g++){
						String tmpName = tmpLI.substring(0, tmpLI.indexOf(splitNumber[g])+ splitNumber[g].length() + 14);

						if(g ==splitNumber.length-2)tmpName += splitNumber[g+1];
						clClean.add(tmpName);
						tmpLI = tmpLI.substring(tmpLI.indexOf(splitNumber[g]) + splitNumber[g].length() + 14, tmpLI.length());
					}
				}else if(splitNumber.length!=1){
					clClean.add(customerList.get(k));
				}
			}

			for(int k =0;k<clClean.size();k++){
				//System.out.println("" + k + " " + clClean.get(k));
				String noReviews = clClean.get(k).replace("No reviews", "No_reviews");
				String[] splitAll = noReviews.split(" ");
				String realName = "";
				for(int f =0;f<splitAll.length;f++){
					if(reviewCode.containsKey(splitAll[f])){
						f =splitAll.length;
					}else {
						realName += " " + splitAll[f];
					}
				}	

				String[] splitPhone = clClean.get(k).split("((\\(\\d{3}\\) ?)|(\\d{3}-))?\\d{3}-\\d{4}");
				String realPhone = clClean.get(k).substring(splitPhone[0].length(),splitPhone[0].length()+ 14);
				String otherDetail = clClean.get(k).replace(realName.trim(), "");
				otherDetail = otherDetail.replace(realPhone, "");
				//System.out.println(realName.trim() + "%2B" + realPhone + "%2B" + "None" + "%2B" + otherDetail);
				formatedOutput += realName.trim() + "%2B" + realPhone + "%2B" + "None" + "%2B" + otherDetail +System.lineSeparator();
			}		
		} catch (Exception e) {
		    e.printStackTrace();
		}	
			return formatedOutput;
	}
	public static HashMap<String, String> getAllNumbersThreeDigitsPhones() {
		HashMap<String, String> allPhoneNumbers = new HashMap<>();
		for(int k=100;k<999;k++){
			String kString = k+"";
			allPhoneNumbers.put(kString, kString);
		}
/*
        Iterator hmIterator = allPhoneNumbers.entrySet().iterator();
        while (hmIterator.hasNext()) { 
            Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
            System.out.println(mapElement.getKey() ); 
        } 
*/        
        return allPhoneNumbers;
	}	
	public static HashMap<String, String> getAllNumbersThreeDigits() {
		HashMap<String, String> allPhoneNumbers = new HashMap<>();
		for(int k=0;k<999;k++){
			String kString = k+"";
			allPhoneNumbers.put(kString, kString);
			if(k<10) kString = "00" +kString;
			if(k<100 && k>9) kString = "0" +kString;
			allPhoneNumbers.put(kString, kString);
		}
/*
        Iterator hmIterator = allPhoneNumbers.entrySet().iterator();
        while (hmIterator.hasNext()) { 
            Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
            System.out.println(mapElement.getKey() ); 
        } 
*/        
        return allPhoneNumbers;
	}
	public static HashMap<String, String> getReviewCode() {
		HashMap<String, String> allReviewCodes = new HashMap<>();

		allReviewCodes.put("5.0", "5.0");
		allReviewCodes.put("No_reviews","No_reviews"); 
		allReviewCodes.put("Ad","Ad"); 
		for(int k=0;k<5;k++){
			for(int f=0;f<10;f++){
				String kString = k+"." +f;
				allReviewCodes.put(kString, kString);
			}
		}
/*
        Iterator hmIterator = allReviewCodes.entrySet().iterator();
        while (hmIterator.hasNext()) { 
            Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
            System.out.println(mapElement.getKey() ); 
        } 
*/      
        return allReviewCodes;
	}
	public static HashMap<String, String> getAllNumbers4Digits() {
		HashMap<String, String> allPhoneNumbers = new HashMap<>();
		for(int k=0;k<9999;k++){
			String kString = k+"";
			allPhoneNumbers.put(kString, kString);
			if(k<10) kString = "000" +kString;
			if(k<100 && k>9) kString = "00" +kString;
			if(k<1000 && k>99) kString = "0" +kString;
			allPhoneNumbers.put(kString, kString);
		}
/*
        Iterator hmIterator = allPhoneNumbers.entrySet().iterator();
        while (hmIterator.hasNext()) { 
            Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
            System.out.println(mapElement.getKey() ); 
        } 
*/        
        return allPhoneNumbers;
	}	
	public static HashMap<String, String> getAllNumbers4DigitsForPhones() {
		HashMap<String, String> allPhoneNumbers = new HashMap<>();
		for(int k=0;k<9999;k++){
			String kString = k+"";
			if(k<10) kString = "000" +kString;
			if(k<100 && k>9) kString = "00" +kString;
			if(k<1000 && k>99) kString = "0" +kString;
			allPhoneNumbers.put(kString, kString);
		}
/*
        Iterator hmIterator = allPhoneNumbers.entrySet().iterator();
        while (hmIterator.hasNext()) { 
            Map.Entry mapElement = (Map.Entry)hmIterator.next(); 
            System.out.println(mapElement.getKey() ); 
        } 
*/        
        return allPhoneNumbers;
	}	
}
