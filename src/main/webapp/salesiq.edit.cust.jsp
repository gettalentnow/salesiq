<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="com.salesiq.dom.SiqScript,com.salesiq.mysql.login.ScriptManager,java.util.Vector,com.salesiq.dom.SiqCallback,com.salesiq.mysql.login.CallbackManager,com.salesiq.dom.SiqCustomer,com.salesiq.mysql.login.CustomerManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
            <script>
            function loadPage(){
                  logoutFunction();
                  if(document.getElementById("activeScripts") != "undefined"){
                      updateScript();
                  }
            }
            function updateScript(){
            //alert(customerString);
            //alert("Ran agains 1");
            var xmlhttp;
            if (window.XMLHttpRequest)
              {// code for IE7+, Firefox, Chrome, Opera, Safari
              xmlhttp=new XMLHttpRequest();
              }
            else
              {// code for IE6, IE5
              xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
              }
            xmlhttp.onreadystatechange=function()
              {
              if (xmlhttp.readyState==4 && xmlhttp.status==200 )
                {
                   document.getElementById("httpRes").innerHTML = xmlhttp.responseText;
                }
              }

            xmlhttp.open("GET", "salesiq.edit.cust.get.script.jsp?sc_id=" + document.getElementById("activeScripts").value + "&cust_id=" + document.getElementById("cust_id").value,true);
            xmlhttp.send();
            }
            </script>           
            <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()"> 
</head>

    <body onload="loadPage()">


              <jsp:include page='navbar_top_siq.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">


                                          <div id="content" style="text-align: left">
              <h1>Edit Customer Form</h1>
              <a href="salesiq.menu.jsp" >Menu</a><BR>
              <a href="salesiq.mgr.callback.cust.jsp?cust_id=<%=request.getParameter("cust_id")%>" >Add Callback</a><BR>

              <%
              if(request.getParameter("used_script")!=null){
                  com.salesiq.mysql.login.ScriptManager tmpMgr = new com.salesiq.mysql.login.ScriptManager();
                  String tmpout = tmpMgr.addScriptMerged(request.getParameter("activeScriptsName"),request.getParameter("used_script"), request.getParameter("cust_id"), (String)session.getAttribute("uid"), request.getParameter("sc_id"), request.getParameter("medium"));
                  if(!tmpout.startsWith("Error")){
                    %>Script Added  <%
                  }else{
                    %><%=tmpout%><%
                  }
              }
              if(request.getParameter("phone")!=null){
                  com.salesiq.mysql.login.CustomerManager tmpMgr = new com.salesiq.mysql.login.CustomerManager();
                  String tmpout = tmpMgr.editCustomer(request.getParameter("cust_id"), request.getParameter("busname"),request.getParameter("phone"),request.getParameter("custStatus"), request.getParameter("cust_desc"), (String)session.getAttribute("uid"));
                  if(!tmpout.startsWith("Error")){
                    %>Customer Changed  <%
                  }else{
                    %><%=tmpout%><%
                  }
              }
              %>
              <%
                CustomerManager tmpMgr = new CustomerManager();
                SiqCustomer tmpCust = tmpMgr.getCustomer((String)session.getAttribute("uid"),request.getParameter("cust_id"));
              %>
            <form class="customerForm" action="salesiq.edit.cust.jsp" >
                  <input type="hidden" id="cust_id" name="cust_id" value="<%=request.getParameter("cust_id")%>">
                  <div class="form-group">
                    <label for="exampleInputPhone">Name</label>
                    <input type="text" class="form-control" id="busname" name="busname" placeholder="Business Number" value="<%=tmpCust.getName()%>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number" value="<%=tmpCust.getPhone()%>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Status</label>
                    <select id="custStatus" name="custStatus" class="form-control">
                        <option value="None" <%if(tmpCust.getStatus().equals("None")){%>selected<%}%> >None</option>
                        <option value="Hot" <%if(tmpCust.getStatus().equals("Hot")){%>selected<%}%> >Hot</option>
                        <option value="Cold" <%if(tmpCust.getStatus().equals("Cold")){%>selected<%}%> >Cold</option>
                        <option value="Warm" <%if(tmpCust.getStatus().equals("Warm")){%>selected<%}%> >Warm</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Notes</label>
                    <textarea class="form-control" id="cust_desc" name="cust_desc" rows="4"><%=tmpCust.getDesc()%></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </form>

                                </div>
                                <BR><BR>
                                  <%
                                  ScriptManager tmpScMgr = new ScriptManager();
                                  Vector<SiqScript> scList = tmpScMgr.getScripts((String)session.getAttribute("uid"), "Active");
                                  if(scList.size()>0){
                                  %>
                                <div id="content" style="text-align: left">
                                  <HR>
                                  <h4>Available Scripts</h4>
                                  <form class="example" action="salesiq.edit.cust.jsp" >
                                  <div class="form-group">
                                    <label for="exampleInputPhone">Merge Script</label>
                                    <select id="activeScripts" name="activeScripts" class="form-control" onchange="updateScript()" onselect="updateScript()">
                                      <%

                                           for(int j=0;j<scList.size();j++){
                                           SiqScript tmpsc = scList.elementAt(j);
                                      %>
                                        <option value="<%=tmpsc.getSiq_id()%>" ><%=tmpsc.getName()%></option>
                                           <%}%>
                                    </select>
                                  </div>
                                  <div id="httpRes" ></div>
                                  </form>
                                  <BR><BR>
                                  <%}%>
                                  <%
                                    Vector<SiqScript> scListMerged = tmpScMgr.getScriptsUsed((String)session.getAttribute("uid"), request.getParameter("cust_id"));
                                    for(int j=0;j<scListMerged.size();j++){
                                        SiqScript tmpsc = scListMerged.elementAt(j);
                                        %>
                                        <b><%=tmpsc.getName()%></b><BR>
                                        <%=tmpsc.getScript_merged()%><BR>
                                        <i><%=tmpsc.getTsString()%></i><BR>
                                        <HR>
                                        <%
                                    }
                                  %>
                                    <BR><BR>
                                </div>

                                <div class="container">
                                <%
                                  CallbackManager tmpCBMgr = new CallbackManager();
                                  Vector<SiqCallback> cbList = tmpCBMgr.getCallbacks((String)session.getAttribute("uid"), request.getParameter("cust_id"), true);

                                       for(int j=0;j<cbList.size();j++){
                                         SiqCallback tmpcust = cbList.elementAt(j);
                                         %>
                                           <div class="row">
                                             <div class="col-sm" style="text-align: left">
                                               <%=tmpcust.getName()%>
                                             </div>
                                             <div class="col-sm">
                                               <%=tmpcust.getDesc()%>
                                             </div>
                                             <div class="col-sm">
                                               <%=tmpcust.getDateString()%>
                                             </div>
                                             <div class="col-sm">
                                               <%=tmpcust.getTsString()%>
                                             </div>
                                           </div>
                                         <%
                                       }
                                    %>
                              </div>
            </div>
    </div>




    </body>
      <!-- The form -->

</html>
