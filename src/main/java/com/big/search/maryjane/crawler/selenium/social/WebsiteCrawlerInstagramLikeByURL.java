package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA 
 *  
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DataInstagramLocations;
import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.big.search.maryjane.persistence.dynamodb.UserManagerInstagram;
import com.big.search.maryjane.persistence.dynamodb.UserManagerLinkedIn;
import com.big.search.maryjane.persistence.dynamodb.UserManagerZillow;

public class WebsiteCrawlerInstagramLikeByURL extends WebsiteCrawlerInstagramBase{
	private DataInstagramLocations insLocations = null;
	private ArrayList<String> locationList = null;
	private String baseUrlLocation = null;
	
	public WebsiteCrawlerInstagramLikeByURL() {
		// TODO Auto-generated constructor stub
		super();

		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebsiteCrawlerInstagramLikeByURL web = new WebsiteCrawlerInstagramLikeByURL();
		web.setInsLocations(new DataInstagramLocations());
		web.setLocationList(web.getInsLocations().getAll());
		for(int j=0;j<web.getLocationList().size();j++){
			web.setBaseUrlLocation(web.getLocationList().get(j));
			web.getStart();
		}
        	
	}
    public void getURLContent() {        	
    	// https://www.linkedin.com/mynetwork/
    	
		try{
			
			ArrayList<String> linkList = new ArrayList<>();
		    this.getDriver().get(this.getBaseUrlLocation()); 
		    for(int j=0;j<10;j++){
		    	this.getDownTonight();
			}
	        for(int j=0;j<14;j++){
	        	WebElement tmpElement = this.getDriver().switchTo().activeElement();
		        tmpElement.sendKeys(Keys.TAB);
		        System.out.println("ELEMENT TEXT " + tmpElement.getText());
		        System.out.println("ELEMENT TEXT TAG" + tmpElement.getTagName());
		        System.out.println("ELEMENT TEXT CLASS " + tmpElement.getAttribute("href"));
		        Thread.sleep(1000);
		        if(tmpElement.getAttribute("href")!=null && tmpElement.getAttribute("href").startsWith("https://www.instagram.com/p/")){
		        	//tmpElement.click();
		        	//break;
		        	linkList.add(tmpElement.getAttribute("href"));
		        }
	        }    
	        
	        for(int j=0;j<linkList.size();j++){
	        	this.getURLContent(linkList.get(j));
	        	
	        }

	        
	        	                		
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
    public void getURLContent(String url) {    
	    this.getDriver().get(url); 

        for(int j=0;j<30;j++){
        	WebElement tmpElement = this.getDriver().switchTo().activeElement();
	        tmpElement.sendKeys(Keys.TAB);
	        System.out.println("ELEMENT TEXT " + tmpElement.getText());
	        System.out.println("ELEMENT TEXT TAG" + tmpElement.getTagName());
	        System.out.println("ELEMENT TEXT CLASS " + tmpElement.getAttribute("href"));
        }    
        List<WebElement> element = this.getDriver().findElements(By.tagName("span"));
        System.out.println("GET ALL ELEMENTS");		 
        for (WebElement temp : element) {
			try{
        		System.out.println(temp.getAttribute("aria-label"));
	        	if(temp!=null && temp.getAttribute("aria-label")!=null && temp.getAttribute("aria-label").startsWith("Like")){	       		    	        
	        			temp.click();
	        	        Thread.sleep(400);
	        	}	 
	        } catch (Exception expected) {
	        	expected.printStackTrace();
	        }	
        }   	
    }
	public DataInstagramLocations getInsLocations() {
		return insLocations;
	}
	public void setInsLocations(DataInstagramLocations insLocations) {
		this.insLocations = insLocations;
	}
	public ArrayList<String> getLocationList() {
		return locationList;
	}
	public void setLocationList(ArrayList<String> locationList) {
		this.locationList = locationList;
	}
	public String getBaseUrlLocation() {
		return baseUrlLocation;
	}
	public void setBaseUrlLocation(String baseUrlLocation) {
		this.baseUrlLocation = baseUrlLocation;
	}
}
