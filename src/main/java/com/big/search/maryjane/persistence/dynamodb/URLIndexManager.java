package com.big.search.maryjane.persistence.dynamodb;

import java.sql.Timestamp;
import java.util.Date;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;

public class URLIndexManager {

	public URLIndexManager() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public void putURLIndex(String newURL, String search_keywords) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable("URLIndex");
		Timestamp ts = new Timestamp(new Date().getTime());
		Item item = new Item()
		    .withPrimaryKey("url", newURL)
		    .withString("visit_time", ts+"")
		    .withString("search_keywords", search_keywords);
		PutItemOutcome outcome = table.putItem(item);		
	    System.out.println("Put Item Outcome: " + outcome.getPutItemResult());
				
	}
	public void putURLRoot(String root_table, String newRoot) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable(root_table);
		Timestamp ts = new Timestamp(new Date().getTime());
		Item item = new Item()
		    .withPrimaryKey("url", newRoot)
		    .withString("visit_time", ts+"");
		PutItemOutcome outcome = table.putItem(item);		
	    System.out.println("Put Item Outcome: " + outcome.getPutItemResult());
	}	
}
