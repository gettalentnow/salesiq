package com.salesiq.mysql.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import com.google.protobuf.TextFormat.ParseException;
import com.salesiq.dom.SiqCallback;
import com.salesiq.dom.SiqCustomer;

public class CallbackManager {

	public CallbackManager() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		CallbackManager tmp = new CallbackManager();
		Vector<SiqCallback> tmplist = tmp.getCallbacks("fa0bd40c-eb36-481b-ae4f-6e44373fd233", false);
		for(int j=0;j<tmplist.size();j++){
			SiqCallback tmpcb = tmplist.elementAt(j);
			System.out.println("HELL O " + tmpcb.getCust_id());
			System.out.println("HELL O " + tmpcb.getName());
			System.out.println("HELL O " + tmpcb.getTs());
			System.out.println("HELL O " + tmpcb.getDateString());
		}
		tmplist = tmp.getCallbacks("fa0bd40c-eb36-481b-ae4f-6e44373fd233", true);
		for(int j=0;j<tmplist.size();j++){
			SiqCallback tmpcb = tmplist.elementAt(j);
			System.out.println("HELL O " + tmpcb.getCust_id());
			System.out.println("HELL O " + tmpcb.getName());
			System.out.println("HELL O " + tmpcb.getTs());
			System.out.println("HELL O " + tmpcb.getDateString());
		}

		//tmp.addCallback("somename", "some description", "09/11/2019", "08:15:11 PM", "cust_id", "bus_id", "user_id");
	}

	public Vector<SiqCallback> getCallbacks(String siq_user_id, String siq_cust_id, boolean sortByCBDate){
		Vector<SiqCallback> tmpList = new Vector<SiqCallback>();
	    try
	    {

	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_callbacks where siq_callbacks.siq_user_id = ? and siq_callbacks.siq_cust_id = ? order by ts desc";
	      if(sortByCBDate)query = "select * from siq_callbacks where siq_callbacks.siq_user_id = ? and siq_callbacks.siq_cust_id = ? order by cb_datetime desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      preparedStmt.setString (2, siq_cust_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqCallback tmpCust = new SiqCallback();
	    	  tmpCust.setSiq_id(resultSet.getString("cb_id"));
	    	  tmpCust.setName(resultSet.getString("cb_name"));
	    	  tmpCust.setDesc(resultSet.getString("cb_notes"));
	    	  tmpCust.setDate(new Date(resultSet.getTimestamp("cb_datetime").getTime()));
	    	  tmpCust.setCust_id(resultSet.getString("siq_cust_id"));
	    	  tmpCust.setUser_id(resultSet.getString("siq_user_id"));
	    	  tmpCust.setBus_id(resultSet.getString("siq_bus_id"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  tmpList.add(tmpCust);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return tmpList;
	}
	public Vector<SiqCallback> getCallbacks(String siq_user_id, boolean sortByCBDate){
		Vector<SiqCallback> tmpList = new Vector<SiqCallback>();
	    try
	    {

	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_callbacks where siq_callbacks.siq_user_id = ? order by ts desc";
	      if(sortByCBDate)query = "select * from siq_callbacks where siq_callbacks.siq_user_id = ? order by cb_datetime desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqCallback tmpCust = new SiqCallback();
	    	  tmpCust.setSiq_id(resultSet.getString("cb_id"));
	    	  tmpCust.setName(resultSet.getString("cb_name"));
	    	  tmpCust.setDesc(resultSet.getString("cb_notes"));
	    	  tmpCust.setDate(new Date(resultSet.getTimestamp("cb_datetime").getTime()));
	    	  tmpCust.setCust_id(resultSet.getString("siq_cust_id"));
	    	  tmpCust.setUser_id(resultSet.getString("siq_user_id"));
	    	  tmpCust.setBus_id(resultSet.getString("siq_bus_id"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  tmpList.add(tmpCust);
	      }
			resultSet.close();
			preparedStmt.close();
			conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return tmpList;
	}
	public String addCallback(String cbName, String cbDesc, String cbStrDate, String cbStrTime, String cust_id, String bus_id, String user_id){

        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");

		SiqCallback tmpCb = new SiqCallback();
		tmpCb.setName(cbName);
		tmpCb.setDesc(cbDesc);
		tmpCb.setCust_id(cust_id);
		tmpCb.setBus_id(bus_id);
		tmpCb.setUser_id(user_id);
        try {

            Date cbdate = formatter.parse(cbStrDate + " " + cbStrTime);
            System.out.println(cbdate);
            System.out.println(formatter.format(cbdate));
            tmpCb.setDate(cbdate);
            //Timestamp ts = new java.sql.Timestamp(date.getTime());
        } catch (java.text.ParseException e) {
            e.printStackTrace();
        }


		return insertCallback(tmpCb);
	}
	public void mergeCallback(String user_id, String cust_id, String dup_cust_id){
		try{
		      MySQLConstants tmpCon = new MySQLConstants();
		      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	          Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
		      String query = "update siq_callbacks set siq_cust_id = ?"
		        + " where siq_callbacks.siq_cust_id = ? and siq_callbacks.siq_user_id = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, cust_id);
		      preparedStmt.setString (2, dup_cust_id);
		      preparedStmt.setString (3, user_id);
		      preparedStmt.execute();
		      preparedStmt.close();
		      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	    }
	}
	public String insertCallback(SiqCallback newCB){
	    try
	    {

	      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
          Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = " insert into siq_callbacks(cb_id, cb_name, cb_notes, cb_datetime, siq_cust_id, siq_user_id, siq_bus_id, ts)"
	        + " values (?, ?, ?, ?, ?, ?, ?, ?)";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, UUID.randomUUID().toString());
	      preparedStmt.setString (2, newCB.getName());
	      preparedStmt.setString (3, newCB.getDesc());
	      preparedStmt.setTimestamp(4, new java.sql.Timestamp(newCB.getDate().getTime()));
	      preparedStmt.setString (5, newCB.getCust_id());
	      preparedStmt.setString (6, newCB.getUser_id());
	      preparedStmt.setString (7, newCB.getBus_id());
	      preparedStmt.setTimestamp(8, currentTimestamp);
	      preparedStmt.execute();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error " + e.getMessage();
	      return "Error: System Error ";
	    }
		return "Callback Added";
	}
}
