<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="false"  import="com.big.search.maryjane.braintree.ProcessPayment,com.big.search.maryjane.persistence.dynamodb.StripeOrders,java.net.URL" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
request.setAttribute("year", sdf.format(new java.util.Date()));
request.setAttribute("tomcatUrl", "https://tomcat.apache.org/");
request.setAttribute("tomcatDocUrl", "/docs/");
request.setAttribute("tomcatExamplesUrl", "/examples/");
%>
<%
ProcessPayment tmpPay = new ProcessPayment();
String tmpToken = tmpPay.getClientToken();
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="AskBookman.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>ASK BOOKMAN</title>
        <link rel="stylesheet" href="css/examples.css" >
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/jquery-ui.css" >
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/handlebars.js"></script>
        <script src="js/typeahead.bundle.js"></script>
        <script src="js/examples.js"></script>


          <!-- Load the required checkout.js script -->
          <script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>

          <!-- Load the required Braintree components. -->
          <script src="https://js.braintreegateway.com/web/3.39.0/js/client.min.js"></script>
          <script src="https://js.braintreegateway.com/web/3.39.0/js/paypal-checkout.min.js"></script>

          <script>
          paypal.Button.render({
            braintree: braintree,
            client: {
              production: '<%=tmpToken%>'
              // Other configuration
            },
            env: 'production',
            commit: true,
            payment: function (data, actions) {
              return actions.braintree.create({
                flow: 'checkout', // Required
                amount: 95.00, // Required
                currency: 'USD', // Required
                enableShippingAddress: true,
                shippingAddressEditable: false,
                shippingAddressOverride: {
                  email: 'Scruff McGruff'
                }
              });
            },

            onAuthorize: function (payload) {
              // Submit `payload.nonce` to your server.
              document.getElementById("nonce").value = payload.nonce;
              document.getElementById("paymentFrom").submit();
            },
          }, '#submit-button');

          </script>

    </head>
    <body>
<jsp:include page='navbar_top.jsp' />


      <div class="container">
        <div class="jumbotron bg-white">
          <div id="content" style="text-align: left">
        <form action="check_out_done.jsp" id="paymentFrom">
          <h2>Grow your business with digital Ads</h2>
              <input name="nonce" id="nonce" type="hidden" value="" />
            <div class="form-group">
              <label for="accountname">Email</label>
              <input type="text" class="form-control" name="postemail" value="<%=request.getParameter("postemail")%>" >
            </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Type</label>
              <input type="text" class="form-control" name="posttype" value="<%=request.getParameter("posttype")%>" >
            </div>
            <div class="form-group">
              <label for="accountname">Business Name</label>
              <input type="text" class="form-control" name="postname" value="<%=request.getParameter("postname")%>" >
            </div>
            <div class="form-group">
              <label for="accountname">Business Description</label>
              <input type="text" class="form-control" name="postdescription" value="<%=request.getParameter("postdescription")%>" >
            </div>
            <div class="form-group">
              <div id="buybutton" >
<button id="submit-button" class="btn btn-primary"></button>
              </div>
            </div>

          </form>
        </div>
      </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="page-footer font-small blue">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">
          <small>
            <code>
              @askweedman store
              <a href="terms_of_service.html">terms</a> & <a href="terms_of_service.html" >privacy</a>
            </code>
          </small>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142719773-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-142719773-1');
</script>


    </body>
</html>
