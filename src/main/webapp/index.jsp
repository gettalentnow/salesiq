<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="false" import="java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
request.setAttribute("year", sdf.format(new java.util.Date()));
request.setAttribute("tomcatUrl", "https://tomcat.apache.org/");
request.setAttribute("tomcatDocUrl", "/docs/");
request.setAttribute("tomcatExamplesUrl", "/examples/");
%>
<!DOCTYPE html>

<%
String domain = "ASK BOOKMAN";
if(request.getHeader("Host")!=null && request.getHeader("Host").contains("weed")){
  domain = "ASK WEEDMAN";
}
%>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title><%=domain%></title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
    </head>

    <body>


              <jsp:include page='navbar_top.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">


              <h1>stoner web search</h1>



          <form class="example" action="index.jsp" >

            <!-- The form -->
                <div id="prefetch" style="position: relative;">
              <input class="form-control typeahead" size="21" type="text" placeholder="Search.." name="search" value="<%=request.getParameter("search")%>" >
              <button class="btn btn-primary" type="submit"><i class="fa fa-lg fa-search"></i></button>
                </div>
              </form>

          <BR>
            <%if(request.getParameter("search")!=null && !request.getParameter("search").equals("")){%>

          <div id="content_weedman" style="text-align: left">
            <%

              HashMap<String, WebSiteItem> siteMap = new HashMap<String, WebSiteItem>();

              AWSGTNBasicSearch tmpSearch = new AWSGTNBasicSearch();
              siteMap = tmpSearch.cmdQueryCloudSearch(siteMap, request.getParameter("search").replace("+" , " "));

              for (Map.Entry<String,WebSiteItem> entry : siteMap.entrySet())  {
                %><BR><BR>
                <a href="<%=entry.getValue().getLink()%>" >
                <%=entry.getValue().getLink()%></a><BR>
                <%=entry.getValue().getDesc()%>
                <%
              }
            %>
          </div>
            <div id="content" style="text-align: left">
          <%=SearchGTN.searchString(request.getParameter("search"),  "X-Forwarded-For:" + request.getHeader("X-Forwarded-For"))%>
            </div>
            <%}%>
<BR><BR>
            <!--
    <h1>Suggest Website</h1>
    <form class="example_2" action="suggest.jsp">
      <input type="text" placeholder="website" name="suggest" id="suggest"  >
      <button type="submit" class="btn btn-primary"><i class="fa fa-search">Suggest</i></button>
      <BR>
      <BR>
      Suggest a website to be included in <%=domain%> web search engine.<BR>
      <a href="getDomains.jsp" >Active Domain Index</a>
    </form>
              <BR><BR>  Search Result by AskBookman.com
              -->
        </div>
      </div>
      <!-- Footer -->
      <footer class="page-footer font-small blue">

        <!-- Copyright -->

        <div class="footer-copyright text-center py-3">
          <small>
            <code>
          askweedman software
          version 1.1.9
          <a href="gtnsearch/terms_of_service.html">terms</a> & <a href="gtnsearch/terms_of_service.html" >privacy</a>
            </code>
          </small>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142719773-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-142719773-1');
      </script>



    </body>
      <!-- The form -->

</html>
