package com.salesiq.dom;

public class SiqScript extends SiqBaseObject {

	private String script;
	private String script_merged;
	private String cust_id;
	private String sc_u_id;
	private String medium;
	
	public String getMedium() {
		return medium;
	}

	public void setMedium(String medium) {
		this.medium = medium;
	}

	public SiqScript() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public String getScript_merged() {
		return script_merged;
	}

	public void setScript_merged(String script_merged) {
		this.script_merged = script_merged;
	}

	public String getCust_id() {
		return cust_id;
	}

	public void setCust_id(String cust_id) {
		this.cust_id = cust_id;
	}

	public String getSc_u_id() {
		return sc_u_id;
	}

	public void setSc_u_id(String sc_u_id) {
		this.sc_u_id = sc_u_id;
	}

}
