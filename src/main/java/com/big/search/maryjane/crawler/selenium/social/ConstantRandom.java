package com.big.search.maryjane.crawler.selenium.social;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import com.big.search.maryjane.persistence.dynamodb.CommentManager;

public class ConstantRandom {
	private CommentManager comMgr =null;
	private ArrayList<String> comments = null;
	public ConstantRandom() {
		comMgr = new CommentManager();
		comments = comMgr.getAll();
		// TODO Auto-generated constructor stub
	}
	public String getRandomComment(){
		Random generator = new Random();		
		return comments.get(generator.nextInt(comments.size()));
	}
	public CommentManager getComMgr() {
		return comMgr;
	}
	public void setComMgr(CommentManager comMgr) {
		this.comMgr = comMgr;
	}
	public ArrayList<String> getComments() {
		return comments;
	}
	public void setComments(ArrayList<String> comments) {
		this.comments = comments;
	}

}
