Welcome to the open source code repository for @ salescal.com

@ salescal.com is an open source web application, built on Apache Tomcat, Amazon Web Services DynamoDB, Bitbucket Code Repository, Bitbucket Continuous Integration, Amazon Web Services ECS, Amazon Web Services ALB, Amazon Web Services Route53, Amazon Web Services EC2, Amazon Web Services IAM, Amazon Web Services Cloudsearch, Selenium Web Driver, Java, ...   

# Use Case List
1. Web Search Engine
2. News Feed
3. Web Crawler and Index for Search
4. Automation Bot for Social Media Marketing
5. Wix store link support
6. Braintree Payment Processing
7. Custom Relationships Management
8. Callback Manager

# How It Works
- Master branch of this application is hosted at https://salescal.com
- All source code is Open Source and available for review.

# Q&A
1. Can I use @ salescal.com open source software to run my own personal or business website? yes, you will need a bitbucket and amazon aws account. Hosting costs for https://salescal.com is less than $100 mon.

2. Can I use @ salescal.com marketing software online for my personal or business website? yes, we run the search and marketing bot(s) for a small fee, sign-up @ https://salescal.com
