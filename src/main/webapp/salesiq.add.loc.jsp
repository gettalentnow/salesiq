<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="com.salesiq.mysql.login.LocationManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">


              <jsp:include page='navbar_top_siq.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">


                                          <div id="content" style="text-align: left">
              <h1>Add Location Form</h1>
              <a href="salesiq.menu.jsp" >Menu</a><BR>

                            <%
                            if(request.getParameter("name")!=null){
                                com.salesiq.mysql.login.LocationManager tmpMgr = new com.salesiq.mysql.login.LocationManager();
                                String tmpout = tmpMgr.addLocation(request.getParameter("name"),request.getParameter("address"),request.getParameter("address2"), request.getParameter("state"), request.getParameter("city"), request.getParameter("zip"), (String)session.getAttribute("uid"), request.getParameter("loc_desc"), request.getParameter("custStatus"), request.getParameter("year"), request.getParameter("latlong"));
                                if(!tmpout.startsWith("Error")){
                                  %>Location Added  <%
                                }else{
                                  %><%=tmpout%><%
                                }
                            }
                            %>

            <form class="example" action="salesiq.add.loc.jsp" >
                  <div class="form-group">
                    <label for="exampleInputPhone">Location Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Axis Apartments">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Address</label>
                    <input type="text" class="form-control" id="address" name="address" placeholder="1200 South Broadway Street">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Address Line 2</label>
                    <input type="text" class="form-control" id="address2" name="address2" placeholder="Apt 200">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">State</label>
                    <input type="text" class="form-control" id="state" name="state" placeholder="CA">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">City</label>
                    <input type="text" class="form-control" id="city" name="city" placeholder="Beverly Hills">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Zip</label>
                    <input type="text" class="form-control" id="zip" name="zip" placeholder="90210">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">GPS Coordinates Lat,Long</label>
                    <input type="text" class="form-control" id="latlong" name="latlong" placeholder="34.0392915 -118.2589071">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Property Built Year</label>
                    <input type="text" class="form-control" id="year" name="year" placeholder="2003">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Target Market</label>
                    <select id="custStatus" name="custStatus" class="form-control">
                        <option value="Unknown" >Unknown</option>
                        <option value="No" >No</option>
                        <option value="Yes" >Yes</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Notes</label>
                    <textarea class="form-control" id="loc_desc" name="loc_desc" rows="4"></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </form>

                                </div>
            </div>
    </div>




    </body>
      <!-- The form -->

</html>
