package com.gtn.search;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;

public class SearchGTN {


	public static String searchString(String searchString, String user_ip) {
        String urlListReturn ="";
		try{
		// TODO Auto-generated method stub
		searchString = searchString.trim();
		String searchStringPlus = searchString.replace(" ", "+");	
		URL oracle = new URL("https://en.so.com/s?ie=utf-8&src=english_home&q=" + searchStringPlus);
        BufferedReader in = new BufferedReader(
        new InputStreamReader(oracle.openStream()));
        String inputLine;
        String searchResult = "";
        while ((inputLine = in.readLine()) != null){
        	searchResult = searchResult + inputLine;
        }
        in.close();
        String[] searchArray = searchResult.split("href=\"");
        for(int j=1;j<searchArray.length;j++){
        		if(searchArray[j].startsWith("http")){
        			if(!searchArray[j].contains(".so.com") && !searchArray[j].contains("qhres.com")
        					&& !searchArray[j].contains("360safe.com") && !searchArray[j].contains("e.360.cn")){
    		        	String display_name = searchArray[j].substring(0, searchArray[j].indexOf("\""));
    		        	display_name = display_name.trim();
    		        	if(display_name.startsWith("https") == true){
    		        		display_name = display_name.substring(8,display_name.length());
    		        	}else{
    		        		display_name = display_name.substring(7,display_name.length());
    		        	}
    		        	if (display_name.length()>26){
    		        		display_name = display_name.substring(0, 26) + "...";
    		        	}
    		        	urlListReturn = urlListReturn + "<BR> " 
    		        	+ "<a href=\"" + searchArray[j].substring(0, searchArray[j].indexOf("\""))
    		        	+ "\" >" + display_name + "</a>";
    		        	String desSR = searchResult.substring(searchResult.indexOf(searchArray[j].substring(0, searchArray[j].indexOf("\""))),searchResult.length());
    		        	desSR = desSR.substring(desSR.indexOf("res-desc") +10, desSR.length());
    		        	desSR = desSR.substring(0, desSR.indexOf("</p>"));
    		        	urlListReturn = urlListReturn + "<BR> " + desSR + "<BR> ";    		        	
        			}
        		}
        }
        

        	WebSiteSearchManager.logDynamoDb(searchString, user_ip);
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}
		return urlListReturn;
	}
	public static void main(String[] args) {
		try{
			System.out.println(SearchGTN.searchString("standup comedy ", "10.10.10.10"));
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}

}
