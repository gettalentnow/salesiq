package com.big.search.maryjane.crawler.selenium;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.gtn.search.dynamodb.WebSite_Index;

public class WebsiteCrawlerRoot {
	public WebDriver driver = null;
	private HashMap<String, String> urlVisited = null; 
	private static String urlRoot = "https://weedmaps.com";
	private boolean isClickMode = false;
	private  HashMap<String, String> tmpUrlCachFull = null;
	
	public WebsiteCrawlerRoot() {
		// TODO Auto-generated constructor stub
		this.driver = new FirefoxDriver();
		this.urlVisited = new HashMap<String, String>();
		this.tmpUrlCachFull = new HashMap<String, String>();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
		WebsiteCrawlerRoot web = new WebsiteCrawlerRoot();
        web.getLinkList();
        for (Map.Entry<String,String> entry : web.tmpUrlCachFull.entrySet())  {	
        	web.clickLink();
        }
	}

    public void getLinkList() {
		try{
	    	this.driver.get(urlRoot);  		
	        List<WebElement> element = this.driver.findElements(By.tagName("a"));		
	        for (WebElement temp : element) {
				try{
		        	if(temp!=null && temp.getAttribute("href")!=null && temp.getAttribute("href").startsWith("http")){
			        		String newUrl = temp.getAttribute("href").split("#")[0];
			        		this.tmpUrlCachFull.put(newUrl, newUrl);
			        		
		        	}
		        } catch (StaleElementReferenceException expected) {
		        	expected.printStackTrace();
		        }		        
	        }       
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
    public void clickLink() {
		try{
	    	this.driver.get(urlRoot);  		
	        List<WebElement> element = this.driver.findElements(By.tagName("a"));		
	        for (WebElement temp : element) {
				try{
		        	if(temp!=null && temp.getAttribute("href")!=null && temp.getAttribute("href").startsWith("http")){
			        		String newUrl = temp.getAttribute("href").split("#")[0];
			        		if(!urlVisited.containsKey(newUrl)){
			        			urlVisited.put(newUrl, newUrl);
			        			temp.click();
			        	        Thread.sleep(4000);		        
			        			return;
			        		}
			        		
		        	}
		        } catch (StaleElementReferenceException expected) {
		        	expected.printStackTrace();
		        }		        
	        }       
		}catch(Exception ex){
			ex.printStackTrace();
		}       	
    }
}
