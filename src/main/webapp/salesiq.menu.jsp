<%@ page session="true" import="com.salesiq.mysql.login.UserManger,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>           
            <script>
                function loadPage() {
                    logoutFunction();
                }    
            </script>            
            <jsp:include page='signin.javascript.include.jsp' />
    </head>

    <body onload="loadPage()">


              <jsp:include page='navbar_top_siq.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">


                                          <div id="content" style="text-align: left">
              <h1>Main Menu</h1>
              <h3>Customer Experience Management</h3>
              <a href="salesiq.view.callback.jsp" >List All Callbacks</a><BR>
              <a href="salesiq.view.cust.jsp" >List All Customers</a><BR>
              <a href="salesiq.view.cust.totals.jsp" >View Customer Stats</a><BR>
              <a href="salesiq.add.cust.jsp" >Add Customer Form</a><BR>
              <a href="salesiq.upload.cust.jsp" >Upload Multiple Customers</a><BR>
              <a href="salesiq.view.script.jsp" >List All Scripts</a><BR>
              <a href="salesiq.add.script.jsp" >Add Script Form</a><BR>
              <HR>
              <h3>Master Data Management</h3>
              <a href="salesiq.view.cust.dup.jsp" >Find and Merge All Duplicate Customer Records</a><BR>
              <a href="salesiq.view.loc.dup.jsp" >Find and Merge All Duplicate Physical Location Records</a><BR>
              <HR>
              <h3>Data Management Platform</h3>
              <a href="salesiq.view.loc.jsp" >List All Physical Locations</a><BR>
              <a href="salesiq.view.loc.map.jsp" >Map All Physical Locations</a><BR>
              <a href="salesiq.add.loc.jsp" >Add Physical Location Form</a><BR>
                                </div>
            </div>
    </div>




    </body>
      <!-- The form -->

</html>
