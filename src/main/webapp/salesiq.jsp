<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="false" import="java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Salescal.com DMP, MDM, CEM">
        <meta name="keywords" content="Salescal Salescal.com DMP, MDM, CEM">
        <title>SalesCal</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
    </head>

    <body>


              <jsp:include page='navbar_top_siq.jsp' />

              <div class="container">
            <div class="jumbotron bg-white mt-3">
              <h2 class="text-danger">Proud maker of SalesCal CEM, MDM and DMP</h2>
              <h4> The marketing tools that give residential and commercial service providers the opportunity to prosper.</h4>
              <HR>
                <ul class="text-left">
                  <li><a href="https://en.wikipedia.org/wiki/Customer_experience">Customer Experience Management</a> (CEM), Build more meaningful and lasting relationships and connect with your customers across sales, customer service, marketing, communities, apps, analytics, and more.</li>
              
                <li><a href="https://en.wikipedia.org/wiki/Master_data_management">Master Data Management</a> (MDM) is designed to remove duplicates, standardize data and apply rules that maintain data integrity.</li>
              
                <li><a href="https://en.wikipedia.org/wiki/Data_management_platform">Data Management Platform</a> (DMP) is a technology platform used for collecting and managing data, mainly for marketing purposes.</li>
              </ul>
              </p>
            </div>
              </div>

    <footer class="footer">
      <div class="container">
        <span class="text-muted">SalesCal.com &copy; Contact US via Email info@salescal.com <a href="terms_of_service.html">Terms of Service</a> & <a href="privacy_policy.html">Privacy Policy</a></span>
      </div>
    </footer>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-155184458-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-155184458-1');
</script>


    </body>
      <!-- The form -->

</html>
