package com.big.search.maryjane.persistence.dynamodb;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

public class UserManagerTwilio {

	public UserManagerTwilio() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserManagerTwilio tmp = new UserManagerTwilio();
		tmp.putDomain("sid", "token", "+XX", "+XX");
	}
	public HashMap<String, TwilioUserAuth> getAllUsers(){
		HashMap<String, TwilioUserAuth> tmpMap = new HashMap<String, TwilioUserAuth>();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		Map<String, AttributeValue> lastKeyEvaluated = null;
		do {
		    ScanRequest scanRequest = new ScanRequest()
		        .withTableName("twilio_token")
		        .withLimit(10)
		        .withExclusiveStartKey(lastKeyEvaluated);
	
		    ScanResult result = client.scan(scanRequest);
		    for (Map<String, AttributeValue> item : result.getItems()){
		    		TwilioUserAuth tmpAuth = new TwilioUserAuth();
		    		tmpAuth.setSid(item.get("sid").getS());
		    		tmpAuth.setToken(item.get("token").getS());
		    		tmpAuth.setTo(item.get("forwardPhone").getS());
		    		tmpAuth.setFrom(item.get("fromPhone").getS());
		    		
		    		tmpMap.put(item.get("sid").getS(), tmpAuth);
		    }
		    lastKeyEvaluated = result.getLastEvaluatedKey();
		} while (lastKeyEvaluated != null);
		return tmpMap;		
	}

	public void putDomain(String username, String password, String forwardPhone, String fromPhone) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable("twilio_token");
		Timestamp ts = new Timestamp(new Date().getTime());
		Item item = new Item()
		    .withPrimaryKey("sid", username)
		    .withString("token", password)
		    .withString("forwardPhone", forwardPhone)
		    .withString("fromPhone", fromPhone)
		    .withString("visit_time", ts + "");
		PutItemOutcome outcome = table.putItem(item);		
	    System.out.println("Put Item Outcome: " + outcome.getPutItemResult());
				
	}  
}
