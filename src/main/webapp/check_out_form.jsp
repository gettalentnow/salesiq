<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="false"  import="java.net.URL" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
request.setAttribute("year", sdf.format(new java.util.Date()));
request.setAttribute("tomcatUrl", "https://tomcat.apache.org/");
request.setAttribute("tomcatDocUrl", "/docs/");
request.setAttribute("tomcatExamplesUrl", "/examples/");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="AskBookman.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>ASK BOOKMAN</title>
        <link rel="stylesheet" href="css/examples.css" >
        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <link rel="stylesheet" href="css/jquery-ui.css" >
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/handlebars.js"></script>
        <script src="js/typeahead.bundle.js"></script>
        <script src="js/examples.js"></script>

    </head>
    <body>
<jsp:include page='navbar_top.jsp' />


      <div class="container">
        <div class="jumbotron bg-white">
          <div id="content" style="text-align: left">
            <h2>Grow your business with digital Ads</h2>
            <p>Get in front of customers when they’re searching for cannabis businesses like yours on askweedman or other Search and Social Media.
              The @askweedman online advertising network connects advertisers to websites that want to host advertisements. The key function of our ad network is an aggregation of ad supply from publishers and matching it with advertiser's demand.
              Only $95 to start and $25 /mon.
            </p>
          <form action="check_out.jsp" >
          <div class="form-group">
            <label for="accountname">Your Email</label>
            <input type="text" class="form-control" name="postemail" placeholder="james@gmail.com" required >
          </div>
            <div class="form-group">
              <label for="exampleFormControlSelect1">Cannabis Friendly</label>
              <select class="form-control" name="posttype">
                <option value="Job" >Job</option>
                <option value="Property Rental" >Property Rental</option>
                <option value="Event" >Event</option>
                <option value="Dispensary" >Dispensary</option>
                <option value="Delivery" >Delivery</option>
                <option value="Doctor" >Doctor</option>
                <option value="Attorney" >Attorney</option>
                <option value="Brand" >Brand</option>
              </select>
            </div>
            <div class="form-group">
              <label for="accountname">Business Name</label>
              <input type="text" class="form-control" name="postname" placeholder="..." required >
            </div>
            <div class="form-group">
              <label for="accountname">Business Description</label>
              <textarea type="text" class="form-control" name="postdescription" placeholder="Listing Description" required >
              </textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary mb-2">Next</button>
            </div>
          </form>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <footer class="page-footer font-small blue">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">
          <small>
            <code>
          @askbookman store
          <a href="terms_of_service.html">terms</a> & <a href="terms_of_service.html" >privacy</a>
            </code>
          </small>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->
      <!-- Global site tag (gtag.js) - Google Analytics -->
      <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142719773-1"></script>
      <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-142719773-1');
      </script>
    </body>
</html>
