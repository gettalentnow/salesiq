package com.big.search.maryjane.persistence.dynamodb;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

public class DomainManager {

	public DomainManager() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DomainManager tmp = new DomainManager();
		tmp.putDomain("www.facebook.com", "NEW");
	}
	public HashMap<String, String> getDomainsIsRelevantYes(){
		HashMap<String, String> tmpMap = new HashMap<String, String>();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		Map<String, AttributeValue> lastKeyEvaluated = null;
		do {
		    ScanRequest scanRequest = new ScanRequest()
		        .withTableName("domains")
		        .withLimit(10)
		        .withExclusiveStartKey(lastKeyEvaluated);
	
		    ScanResult result = client.scan(scanRequest);
		    for (Map<String, AttributeValue> item : result.getItems()){
		    	if(item.get("isrelevant").getS().equals("YES")){
		    		tmpMap.put(item.get("domain").getS(), item.get("domain").getS());
		    	}
		    }
		    lastKeyEvaluated = result.getLastEvaluatedKey();
		} while (lastKeyEvaluated != null);
		return tmpMap;		
	}
	public HashMap<String, String> getDomainsIsRelevantNo(){
		HashMap<String, String> tmpMap = new HashMap<String, String>();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		Map<String, AttributeValue> lastKeyEvaluated = null;
		do {
		    ScanRequest scanRequest = new ScanRequest()
		        .withTableName("domains")
		        .withLimit(10)
		        .withExclusiveStartKey(lastKeyEvaluated);
	
		    ScanResult result = client.scan(scanRequest);
		    for (Map<String, AttributeValue> item : result.getItems()){
		    	if(item.get("isrelevant").getS().equals("NO")){
		    		tmpMap.put(item.get("domain").getS(), item.get("domain").getS());
		    	}
		    }
		    lastKeyEvaluated = result.getLastEvaluatedKey();
		} while (lastKeyEvaluated != null);
		return tmpMap;		
	}
	public HashMap<String, String> getDomainsNew(){
		HashMap<String, String> tmpMap = new HashMap<String, String>();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		Map<String, AttributeValue> lastKeyEvaluated = null;
		do {
		    ScanRequest scanRequest = new ScanRequest()
		        .withTableName("domains")
		        .withLimit(10)
		        .withExclusiveStartKey(lastKeyEvaluated);
	
		    ScanResult result = client.scan(scanRequest);
		    for (Map<String, AttributeValue> item : result.getItems()){
		    	if(item.get("isrelevant").getS().equals("NEW")){
		    		tmpMap.put(item.get("domain").getS(), item.get("domain").getS());
		    	}
		    }
		    lastKeyEvaluated = result.getLastEvaluatedKey();
		} while (lastKeyEvaluated != null);
		return tmpMap;		
	}
	public void putDomain(String newDomain, String isrelevant) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable("domains");
		Timestamp ts = new Timestamp(new Date().getTime());
		Item item = new Item()
		    .withPrimaryKey("domain", newDomain)
		    .withString("isrelevant", isrelevant)
		    .withString("visit_time", ts + "");
		PutItemOutcome outcome = table.putItem(item);		
	    System.out.println("Put Item Outcome: " + outcome.getPutItemResult());
				
	}	
}
