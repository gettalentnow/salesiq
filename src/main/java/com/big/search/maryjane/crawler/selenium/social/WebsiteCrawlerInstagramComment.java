package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA 
 *  
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.big.search.maryjane.persistence.dynamodb.UserManagerInstagram;
import com.big.search.maryjane.persistence.dynamodb.UserManagerLinkedIn;

public class WebsiteCrawlerInstagramComment {
	public WebDriver driver = null;
	private UserManagerInstagram usrMngr = null;
	private HashMap<String, String> usrMap = null;

	public WebsiteCrawlerInstagramComment() {
		// TODO Auto-generated constructor stub
		this.driver = new FirefoxDriver();
		this.usrMngr = new UserManagerInstagram();
		this.usrMap = this.usrMngr.getAllUsers();

		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
    		
		WebsiteCrawlerInstagramComment web = new WebsiteCrawlerInstagramComment();
		
        for (Map.Entry<String,String> entry : web.usrMap.entrySet())  {		
        	web.getURLContent("https://instagram.com", entry.getKey(), entry.getValue());
        }			
		//web.driver.close();
        	
	}
    public void getURLContent(String url, String username, String password) {
    	System.out.println(url);	        	
    	// https://www.linkedin.com/mynetwork/
    	
		try{
	    	this.driver.get(url + "/accounts/login");  		        		    	
	    	
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };
	                
	        WebDriverWait wait = new WebDriverWait(this.driver, 30);
	        wait.until(pageLoadCondition);
	        // wait ten seconds for Capcha test to manually bypass
	        Thread.sleep(1000);		        

	        List<WebElement> element = this.driver.findElements(By.tagName("input"));	
	   	 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("name"));
		        	if(temp!=null && temp.getAttribute("name")!=null && temp.getAttribute("name").startsWith("username")){	       		    	        
		        			temp.sendKeys(username);
		        	        Thread.sleep(1000);		        
		        	}	 
		        	if(temp!=null && temp.getAttribute("name")!=null && temp.getAttribute("name").startsWith("password")){	       		    	        
	        			temp.sendKeys(password);
	        	        Thread.sleep(2000);		    
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }

	        wait.until(pageLoadCondition);
	        element = this.driver.findElements(By.tagName("button"));	
		   	 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("type"));
		        	if(temp!=null && temp.getAttribute("type")!=null && temp.getAttribute("type").startsWith("submit")){	       		    	        
			        	if(temp.getText().startsWith("Log In")){	       		    	        
		        			temp.click();
		        			break;
			        	}	 
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }	        
	        wait.until(pageLoadCondition);
	        Thread.sleep(5000);		        
	    	this.driver.get(url + "/"); 
	        wait.until(pageLoadCondition);
	        // wait ten seconds for Capcha test to manually bypass
	        Thread.sleep(1000);	
	        wait.until(pageLoadCondition);     
	    	
	        element = this.driver.findElements(By.tagName("button"));	 
	        for (WebElement temp : element) {
	        	if(temp!=null && temp.getText()!=null && temp.getText().startsWith("Not Now")){	  
	        		temp.click();
	        	}	
	        }	        
	           
	        
	        JavascriptExecutor jse = (JavascriptExecutor)this.driver;
	        for(int j=0;j<Constant.scrlCount;j++){
	        	jse.executeScript("window.scrollBy(0,250)", "");
		        wait.until(pageLoadCondition);		        
			}         
	         

	        
	        element = this.driver.findElements(By.tagName("textarea"));	
	        // wait ten seconds for Capcha test to manually bypass

	        	        
	        int i = 0;
	        System.out.println("GET ALL ELEMENTS");		 
	        for (WebElement temp : element) {
				try{
					temp.sendKeys(Keys.TAB);
					temp.clear();
					temp.sendKeys("Some Sample Text Here");
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }
	        wait.until(pageLoadCondition);   
	        
	        element = this.driver.findElements(By.tagName("button"));	
	        // wait ten seconds for Capcha test to manually bypass
	        System.out.println("GET ALL ELEMENTS");		 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getText());
		        	if(temp!=null && temp.getText()!=null && temp.getText().startsWith("Post")){	       		    	        
		        			temp.click();
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }
	        	                		
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
}
