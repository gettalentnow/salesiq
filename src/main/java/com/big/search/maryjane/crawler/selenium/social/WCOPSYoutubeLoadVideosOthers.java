package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA
 *
 */

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.UserManagerTwitter;
import com.big.search.maryjane.persistence.dynamodb.VideoManager;
import com.big.search.maryjane.persistence.dynamodb.VideoManagerOther;

public class WCOPSYoutubeLoadVideosOthers extends WebsiteCrawlerYoutubeBase{
	private int currentAccount = 0;
											//tin foil hat //kill tony //mad flavor	... /mike tyson / rappaport
	private String[] accounts = new String[]{"UCBgldhBkwIxoQp0fxBg8pgA" , "UCwzCMiicL-hBUzyjWiJaseg", 
			"madflavorsworld", "BRealTV", "UCdtNjOwfQpgVK0FyOeLyzrg", "UC4n4JDkCGWV76BkZ6LvnHMg"};
	private String baseUrl = "https://youtube.com";
	//channel/UCMY3AvCq_USwNJH7tD8VtZg/videos
	public WCOPSYoutubeLoadVideosOthers() {
		// TODO Auto-generated constructor stub

    	super();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	WCOPSYoutubeLoadVideosOthers web = new WCOPSYoutubeLoadVideosOthers();
    	web.setDriver(new FirefoxDriver());
    	for(int j=0;j<web.getAccounts().length;j++){
    		web.getURLContent();
    	}
	}
    public void getURLContent() {
    	int tmpCurrentCount = this.getCurrentAccount();
    	this.setCurrentAccount(this.getCurrentAccount()+1);
    	String tmpBaseUrl = this.getBaseUrl() + "/channel/" +this.getAccounts()[tmpCurrentCount] + "/videos";
    	System.out.println(tmpBaseUrl);
		try{

	    	this.getDriver().get(tmpBaseUrl);	
	    	for(int j=0;j<100;j++){
	    		this.getDownTonight();
	    	}
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };	                
	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 60);
	        wait.until(pageLoadCondition);
	        List<WebElement> element = this.getDriver().findElements(By.tagName("a"));	
	        System.out.println("GET ALL ELEMENTS");		 
	        for (WebElement temp : element) {
		        System.out.println("ELEMENT TEXT " + temp.getText());
		        System.out.println("ELEMENT TEXT CLASS " + temp.getAttribute("class"));
		        System.out.println("ELEMENT TEXT CLASS " + temp.getAttribute("href"));
	        	if(temp.getAttribute("href")!=null && temp.getAttribute("href").contains("watch?v=")){
			        System.out.println("Found VIDEO " + (temp.getAttribute("href").split("="))[1]);	 
			        VideoManagerOther tmpVM = new VideoManagerOther();
			        tmpVM.putVideo((temp.getAttribute("href").split("="))[1], this.getAccounts()[tmpCurrentCount]);
	        	}
	        }
	       	
	        wait.until(pageLoadCondition);
	        Thread.sleep(2000);
	        
		}catch(Exception ex){
			ex.printStackTrace();
		}
    }
	public String[] getAccounts() {
		return accounts;
	}
	public void setAccounts(String[] accounts) {
		this.accounts = accounts;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
	public int getCurrentAccount() {
		return currentAccount;
	}
	public void setCurrentAccount(int currentAccount) {
		this.currentAccount = currentAccount;
	}
}
