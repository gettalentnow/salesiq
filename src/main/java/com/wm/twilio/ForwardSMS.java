package com.wm.twilio;

import java.util.HashMap;
import java.util.Map;

import com.big.search.maryjane.persistence.dynamodb.TwilioUserAuth;
import com.big.search.maryjane.persistence.dynamodb.UserManagerInstagram;
import com.big.search.maryjane.persistence.dynamodb.UserManagerTwilio;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ForwardSMS {
		private UserManagerTwilio usrMngr = null;
		private HashMap<String, TwilioUserAuth> usrMap = null;

	  public static void main(String[] args) {
		  ForwardSMS smstmp = new ForwardSMS();
		  smstmp.forwardMsg("test with dynamodb");
	  }
	  
	  

		public String forwardMsg(String msgBody){
			this.setUsrMngr(new UserManagerTwilio());
			this.setUsrMap(this.getUsrMngr().getAllUsers());

		    for (Map.Entry<String,TwilioUserAuth> entry : this.getUsrMap().entrySet())  {

			    Twilio.init(entry.getKey(), entry.getValue().getToken());		   
			    Message message = Message.creator(new PhoneNumber(entry.getValue().getTo()),
			        new PhoneNumber(entry.getValue().getFrom()), 
			        msgBody).create();	
			    return message.getSid();	
		    }
		    return null;
		}	

		public UserManagerTwilio getUsrMngr() {
			return usrMngr;
		}

	public void setUsrMngr(UserManagerTwilio usrMngr) {
		this.usrMngr = usrMngr;
	}

	public HashMap<String, TwilioUserAuth> getUsrMap() {
		return usrMap;
	}

	public void setUsrMap(HashMap<String, TwilioUserAuth> usrMap) {
		this.usrMap = usrMap;
	}
}
