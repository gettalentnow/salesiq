package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA
 *
 */

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;


public class WebsiteCrawlerYoutubeComment extends WebsiteCrawlerYoutubeBase{
	private ConstantRandom randComment = null; 

	public WebsiteCrawlerYoutubeComment() {
		// TODO Auto-generated constructor stub

    	super();
    	randComment = new ConstantRandom();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	WebsiteCrawlerYoutubeComment web = new WebsiteCrawlerYoutubeComment();
    	web.getStart();
    	
	}
    public void getURLContent() {
    	System.out.println(this.getBaseUrl());
		try{

	    	this.getDriver().get(this.getBaseUrl() + "/watch?v=" + this.getCurrentVideo());	
	    	this.getDownTonight();
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };	                
	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 60);
	        wait.until(pageLoadCondition);
	        WebElement tmpElement = this.getDriver().switchTo().activeElement();
	        int cmttab = 0;
	        for(int j=0;j<50;j++){
		        tmpElement = this.getDriver().switchTo().activeElement();
		        tmpElement.sendKeys(Keys.TAB);
		        System.out.println("ELEMENT TEXT " + tmpElement.getText());
		        System.out.println("ELEMENT TEXT CLASS " + tmpElement.getAttribute("class"));
		        Thread.sleep(200);
		        if(tmpElement.getAttribute("class")!=null && tmpElement.getAttribute("class").contains("ytd-comment-simplebox-renderer")){
		        	try{
		        		cmttab= j;
				        tmpElement.click();	
				        tmpElement = this.getDriver().switchTo().activeElement();
				        tmpElement.sendKeys(this.randComment.getRandomComment());
				        System.out.println("ELEMENT TEXT CLICKED");
				        //break;
					}catch(Exception ex){
						ex.printStackTrace();
					}
		        }
		        if(tmpElement.getAttribute("class")!=null && tmpElement.getAttribute("class").contains("ytd-button-renderer style-primary")){
		        	try{
		        		if(j>cmttab){
		        			tmpElement.click();	
		        			System.out.println("ELEMENT TEXT CLICKED");
		        	        wait.until(pageLoadCondition);
		        	        Thread.sleep(1000);
		        			break;
		        		}
					}catch(Exception ex){
						ex.printStackTrace();
					}
		        }
	        }
	       	
	        wait.until(pageLoadCondition);
	        Thread.sleep(1000);
	        
		}catch(Exception ex){
			ex.printStackTrace();
		}
    }
}
