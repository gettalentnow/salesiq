package com.gtn.search.dynamodb;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Date;
import java.sql.Timestamp;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

public class WebSite_Index {

	
	public HashMap<String, String> getAllIndexNoLimit(HashMap<String, String> tmpMap) {
			int count = 0;
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		Map<String, AttributeValue> lastKeyEvaluated = null;
	do {
	    ScanRequest scanRequest = new ScanRequest()
	        .withTableName("website_index")
	        .withLimit(10)
	        .withExclusiveStartKey(lastKeyEvaluated);

	    ScanResult result = client.scan(scanRequest);
	    for (Map<String, AttributeValue> item : result.getItems()){
	       // printItem(item);
	    	count +=1;
	    	System.out.println("COUNT " + count + " " + item.get("url").getS());
		    tmpMap.put(item.get("url").getS(), item.get("url").getS());
	    }
	    lastKeyEvaluated = result.getLastEvaluatedKey();
	} while (lastKeyEvaluated != null);
	
	
		return tmpMap;
	}
	
	public HashMap<String, String> getAllIndex(HashMap<String, String> tmpMap) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		ScanRequest scanRequest = new ScanRequest()
			    .withTableName("website_index")
			    .withLimit(1000);

			ScanResult result = client.scan(scanRequest);
			for (Map<String, AttributeValue> item : result.getItems()){
			    System.out.println(item.get("url").getS());
			    tmpMap.put(item.get("url").getS(), item.get("url").getS());
			}		
		return tmpMap;
	}
	public String logDynamoDb(String user_ip, String keywords_list) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable("website_index");
		Date date= new Date();
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);
		Item item = new Item()
		    .withPrimaryKey("url", user_ip)
		    .withString("visit_time", ts+"")
		    .withString("keyword_list", keywords_list);
		PutItemOutcome outcome = table.putItem(item);		
	     System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());

		return "Item has been added " ;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebSite_Index tmpVisitMgr = new WebSite_Index();
		tmpVisitMgr.getAllIndex(new HashMap<String, String>());
		//System.out.println(tmpVisitMgr.logDynamoDb( "http://10.10.10.11", "armen god jesus peace love awesome"));
	}

}
