<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="false"  pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<%
if(request.getHeader("Host")!=null && request.getHeader("User-Agent")!=null){
  if(request.getHeader("Host").equals("www.askbookman.com") || request.getHeader("Host").equals("askbookman.com")){
    try{
      response.sendRedirect("https://askbookman.com/gtnsearch/quickpay.jsp");
    }catch(Exception ex){
    %><%=ex%><%
    }
  }
}
%>
<%
if(request.getHeader("Host")!=null && request.getHeader("User-Agent")!=null){
  if(request.getHeader("Host").equals("www.salescal.com") || request.getHeader("Host").equals("salescal.com")){
    try{
      response.sendRedirect("https://salescal.com/gtnsearch/salesiq.jsp");
    }catch(Exception ex){
    %><%=ex%><%
    }
  }
}
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Askweedman.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <link rel="stylesheet" href="gtnsearch/css/all.css" >
        <link rel="stylesheet" href="gtnsearch/css/examples.css" >
        <link rel="stylesheet" href="gtnsearch/css/bootstrap.min.css" >
        <link rel="stylesheet" href="gtnsearch/css/jquery-ui.css" >
        <script src="gtnsearch/js/jquery-1.10.2.min.js"></script>
        <script src="gtnsearch/js/bootstrap.min.js"></script>
        <script src="gtnsearch/js/handlebars.js"></script>
        <script src="gtnsearch/js/typeahead.bundle.js"></script>
        <script src="gtnsearch/js/examples.js"></script>

    </head>
    <body>

      <!-- Footer -->
      <footer class="page-footer font-small blue">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">
          <small>
            <code>
           software
          version 1.1.9 build number $BITBUCKET_BUILD_NUMBER
          <a href="gtnsearch/terms_of_service.html">terms</a> & <a href="gtnsearch/terms_of_service.html" >privacy</a>
            </code>
          </small>
        </div>
      </footer>
    </body>
</html>
