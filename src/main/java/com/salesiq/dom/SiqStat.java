package com.salesiq.dom;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SiqStat {
	private String stat_id ; 
	private int cust_tot ; 
	private int cust_tot_cold ; 
	private int cust_tot_warm ; 
	private int cust_tot_hot ; 
	private int cust_tot_none ;
	private int cust_tot_cb ; 
	private String user_id ; 
	private String bus_id ; 
	private Timestamp ts;
	
	public SiqStat() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getStat_id() {
		return stat_id;
	}

	public void setStat_id(String stat_id) {
		this.stat_id = stat_id;
	}

	public int getCust_tot() {
		return cust_tot;
	}

	public void setCust_tot(int cust_tot) {
		this.cust_tot = cust_tot;
	}

	public int getCust_tot_cold() {
		return cust_tot_cold;
	}

	public void setCust_tot_cold(int cust_tot_cold) {
		this.cust_tot_cold = cust_tot_cold;
	}

	public int getCust_tot_warm() {
		return cust_tot_warm;
	}

	public void setCust_tot_warm(int cust_tot_warm) {
		this.cust_tot_warm = cust_tot_warm;
	}

	public int getCust_tot_hot() {
		return cust_tot_hot;
	}

	public void setCust_tot_hot(int cust_tot_hot) {
		this.cust_tot_hot = cust_tot_hot;
	}

	public int getCust_tot_none() {
		return cust_tot_none;
	}

	public void setCust_tot_none(int cust_tot_none) {
		this.cust_tot_none = cust_tot_none;
	}

	public int getCust_tot_cb() {
		return cust_tot_cb;
	}

	public void setCust_tot_cb(int cust_tot_cb) {
		this.cust_tot_cb = cust_tot_cb;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getBus_id() {
		return bus_id;
	}

	public void setBus_id(String bus_id) {
		this.bus_id = bus_id;
	}

	public String getTsString() {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy hh:mm a");
		return formatter.format(new Date(ts.getTime()));
	}
	
	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

}
