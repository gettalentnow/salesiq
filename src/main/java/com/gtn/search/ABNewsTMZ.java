package com.gtn.search;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;
import java.util.Vector;



public class ABNewsTMZ {


	 	
	public static void main(String[] args) {
		try{
			 String urlListReturn = "";
			 Vector<NewsItem> shuffledList = ABNewsTMZ.searchString(new Vector<NewsItem>());
			 for(int j=0;j<shuffledList.size();j++){
				 NewsItem tmpItem = shuffledList.elementAt(j);
		     		urlListReturn = urlListReturn  
		     		+ "<a href=\"" + tmpItem.getLink()
		 		    + "\" >" + tmpItem.getName() + "</a>" 
		 		    + "<BR>" + tmpItem.getDesc()  + "<BR><BR>"; 			 
			 }			
			 System.out.println(urlListReturn);
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}
	}
	
	public static Vector<NewsItem> searchString(Vector<NewsItem> newsItemList) {
        String urlListReturn ="";
        String response = null;
        try {
            URL url = new URL("https://www.tmz.com/category/celebrity-justice/rss.xml");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-GB;     rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13 (.NET CLR 3.5.30729)");
            connection.getResponseCode();
            InputStream stream = connection.getErrorStream();
            if (stream == null) {
                stream = connection.getInputStream();
            }
            try (Scanner scanner = new Scanner(stream)) {
                scanner.useDelimiter("\\Z");
                response = scanner.next();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(response);
        String[] items = response.split("<item>");
        for(int j=1;j<items.length;j++){
        	NewsItem ni = new NewsItem();
        	ni.setName(items[j].substring(items[j].indexOf("<title>")+7, items[j].indexOf("</title>")));
        	ni.setLink(items[j].substring(items[j].indexOf("<link>")+6, items[j].indexOf("</link>")));
        	ni.setDesc(items[j].substring(items[j].indexOf("<description>")+13, items[j].indexOf("</description>")));
        	ni.setDesc(ni.getDesc().replace("<![CDATA[", ""));
        	ni.setDesc(ni.getDesc().replace("<![CDATA", ""));
        	ni.setDesc(ni.getDesc().replace("]]>", ""));
        	ni.setDesc(ni.getDesc().replace("]>", ""));
        	ni.setDesc(ni.getDesc().replace("<p>", ""));
        	ni.setDesc(ni.getDesc().replace("<br />", ""));
        	ni.setDesc(ni.getDesc().replace("</p>", ""));
        	ni.setDesc(ni.getDesc().substring(0, ni.getDesc().indexOf("<a")));
        	//ni.setDesc(ni.getDesc().substring(0, ni.getDesc().indexOf("The post ")));
        	
        	// The post
        	ni.setSource("TMZ");
        	newsItemList.add(ni);
        }
		return newsItemList;
	}
}
