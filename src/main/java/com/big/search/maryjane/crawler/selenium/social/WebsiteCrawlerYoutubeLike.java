package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA
 *
 */

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class WebsiteCrawlerYoutubeLike extends WebsiteCrawlerYoutubeBase{

	public WebsiteCrawlerYoutubeLike() {
		// TODO Auto-generated constructor stub

    	super();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	WebsiteCrawlerYoutubeLike web = new WebsiteCrawlerYoutubeLike();
    	web.getStart();
    	
	}
    public void getURLContent() {
    	System.out.println(this.getBaseUrl());
		try{

	    	this.getDriver().get(this.getBaseUrl() + "/watch?v=" + this.getCurrentVideo());	
	    	for(int a=0;a<10;a++)this.getDownTonight();
	    	
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };	                
	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 60);
	        wait.until(pageLoadCondition);
	        
	        List<WebElement> element = this.getDriver().findElements(By.tagName("button"));	
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("aria-label"));
		        	if(temp!=null && temp.getAttribute("aria-label")!=null && temp.getAttribute("aria-label").startsWith("like this ")){	       		    	        
		        			temp.click();
		        	        Thread.sleep(200);
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }	 
	        
	        wait.until(pageLoadCondition);
	        Thread.sleep(2000);
	        
		}catch(Exception ex){
			ex.printStackTrace();
		}
    }
}
