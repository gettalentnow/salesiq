package com.gtn.search;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Date;
import java.sql.Timestamp;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;

public class WebSiteVisitManager {

	public String logDynamoDb(String user_ip) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
//		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable("website_visit");
		Date date= new Date();
		long time = date.getTime();
		Timestamp ts = new Timestamp(time);
		Item item = new Item()
		    .withPrimaryKey("user_ip", user_ip)
		    .withString("visit_time", ts+"");
		PutItemOutcome outcome = table.putItem(item);		
	     System.out.println("PutItem succeeded:\n" + outcome.getPutItemResult());

		return "Item has been added " ;
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		WebSiteVisitManager tmpVisitMgr = new WebSiteVisitManager();
		System.out.println(tmpVisitMgr.logDynamoDb( "10.10.10.11"));
	}

}
