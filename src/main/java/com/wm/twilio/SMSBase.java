package com.wm.twilio;

import java.util.HashMap;

import com.big.search.maryjane.persistence.dynamodb.TwilioUserAuth;
import com.big.search.maryjane.persistence.dynamodb.UserManagerTwilio;

public class SMSBase {

	private UserManagerTwilio usrMngr = null;
	private HashMap<String, TwilioUserAuth> usrMap = null;
	
	public SMSBase() {
		// TODO Auto-generated constructor stub
		this.setUsrMngr(new UserManagerTwilio());
		this.setUsrMap(this.getUsrMngr().getAllUsers());
	}

	public UserManagerTwilio getUsrMngr() {
		return usrMngr;
	}

	public void setUsrMngr(UserManagerTwilio usrMngr) {
		this.usrMngr = usrMngr;
	}

	public HashMap<String, TwilioUserAuth> getUsrMap() {
		return usrMap;
	}

	public void setUsrMap(HashMap<String, TwilioUserAuth> usrMap) {
		this.usrMap = usrMap;
	}

}
