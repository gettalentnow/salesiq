package com.salesiq.dom;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

public class SiqBaseObject {

	private Timestamp ts;
	private Timestamp ts_update;
	private String siq_id ;
	private String name;
	private String status;
	private String user_id;
	private String bus_id;
	public SiqBaseObject() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	public Timestamp getTs_update() {
		return ts_update;
	}

	public String getTs_updateString() {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy hh:mm a");
		return formatter.format(new Date(ts_update.getTime()));
	}
	
	public void setTs_update(Timestamp ts_update) {
		this.ts_update = ts_update;
	}

	public String getSiq_id() {
		return siq_id;
	}

	public void setSiq_id(String siq_id) {
		this.siq_id = siq_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameShort() {
		if(name == null)return "";
		if(name.length()>17){
			return name.substring(0,16) + "...";
		}
		return name;
	}
	public String getStatus() {
		return status;
	}
	public String getTsString() {
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy hh:mm a");
		return formatter.format(new Date(ts.getTime()));
	}
	
	public void setStatus(String status) {
		this.status = status;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getBus_id() {
		return bus_id;
	}

	public void setBus_id(String bus_id) {
		this.bus_id = bus_id;
	}

}
