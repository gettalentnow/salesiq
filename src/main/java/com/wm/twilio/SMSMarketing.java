package com.wm.twilio;

import java.util.HashMap;
import java.util.Map;

import com.big.search.maryjane.persistence.dynamodb.TwilioUserAuth;
import com.big.search.maryjane.persistence.dynamodb.UserManagerTwilio;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class SMSMarketing extends SMSBase {
	public SMSMarketing() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SMSMarketing tmpMkt = new SMSMarketing();
		tmpMkt.sendMsg("text message from https://askweedman.com");
	}
	public String sendMsg(String msgBody){

	    for (Map.Entry<String,TwilioUserAuth> entry : this.getUsrMap().entrySet())  {
		    Twilio.init(entry.getKey(), entry.getValue().getToken());		   
		    Message message = Message.creator(new PhoneNumber(entry.getValue().getTo()),
		        new PhoneNumber(entry.getValue().getFrom()), 
		        msgBody).create();	
		    return message.getSid();	
	    }
	    return null;
	}	
}
