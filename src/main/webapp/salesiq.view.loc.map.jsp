<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="java.util.Vector,com.salesiq.dom.SiqLocation,com.salesiq.mysql.login.LocationManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>
<html>
  <head>
            <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
    <style>
       /* Set the size of the div element that contains the map */
      #map {
        height: 600px;  /* The height is 400 pixels */
        width: 1080px;  /* The width is the width of the web page */
        text-align: center;
        margin: 0 auto 0 auto;
       }
    </style>
            <script>
                function loadPage() {
                    logoutFunction();
                }
            </script>
            <jsp:include page='signin.javascript.include.jsp' />
    </head>

    <body onload="loadPage()">
    <jsp:include page='navbar_top_siq.jsp' />
    <div class="container">
      <div class="jumbotron bg-white mt-3">
      <div id="content" style="text-align: left">
        <h1>Map All Physical Locations</h1>
        <a href="salesiq.menu.jsp" >Menu</a><BR><BR>
    <!--The div element for the map -->
    <div id="map"></div>
    <script>
// Initialize and add the map
function initMap() {
  // The location of Uluru
  var uluru = {lat: 39.392530,  lng: -101.054925};
  // The map, centered at Uluru
  var map = new google.maps.Map(
  document.getElementById('map'), {zoom: 5, center: uluru});
  // The marker, positioned at Uluru
  <%
  LocationManager tmpMgr = new LocationManager();
  Vector<SiqLocation> custList = new Vector<SiqLocation>();
  custList = tmpMgr.getLocations("a9914eb5-07ac-40fc-8aab-19a5600646b0", false);
  for(int j=0;j<custList.size();j++){
  SiqLocation tmpcust = custList.elementAt(j);
  %>

  var uluru<%=j+""%> = {lat: <%=tmpcust.getLat()%>, lng: <%=tmpcust.getLongitute()%>};
  var marker = new google.maps.Marker({position: uluru<%=j+""%>, map: map, label: '<%=tmpcust.getNameShort()%>'});

  google.maps.event.addListener(marker , 'click', function(){
      var infowindow = new google.maps.InfoWindow({
        content:'<%=tmpcust.getNameShort()%> <BR> <%=tmpcust.getAdd()%><BR>Year Built: <%=tmpcust.getYeat_buildString()%>',
        position: uluru<%=j+""%>,
      });
      infowindow.open(map);
  });
  <% } %>
  $(document).ready(function(){
  google.maps.event.addDomListener(window, 'load', init()); // two calls

});
}

    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCfJ4GphK_kD_YOR8jx-YPPepczW1cB440&callback=initMap">
    </script>
              </div></div></div>
  </body>
</html>
