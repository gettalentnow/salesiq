package com.big.search.maryjane.crawler.selenium.social;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/*
 *  TWITTER BOT WRITEN IN JAVA
 *
 */
 //////////////////////NOT WORKING WILL LOCK IP NOT SURE HOW BUT YOU TUBE DETECTED THE MACHINE LOGIN /////////////////////////////
// RUN ONLY THE PASSWORD and name generator
import java.util.UUID;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.askweedman.maryjane.util.Names;
import com.big.search.maryjane.persistence.dynamodb.UserManagerYoutube;
import com.big.search.maryjane.persistence.dynamodb.UserManagerZillow;
import com.big.search.maryjane.persistence.dynamodb.VideoManager;
import com.big.search.maryjane.persistence.dynamodb.VideoManagerOther;

public class WCOPSZillowSave extends WebsiteCrawlerYoutubeBase{
	//channel/UCMY3AvCq_USwNJH7tD8VtZg/videos
	private UserManagerZillow usrMngrZillow = null;

	public WCOPSZillowSave() {
		// TODO Auto-generated constructor stub

    	super();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	WCOPSZillowSave web = new WCOPSZillowSave();
    	web.getStart();
	}
	public void getStart(){
		this.setUsrMngrZillow(new UserManagerZillow());
		this.setUsrMap(this.getUsrMngrZillow().getAllUsers());		
	    for (Map.Entry<String,String> entry : this.getUsrMap().entrySet())  {	
		    	this.setDriver(new FirefoxDriver());
		    	this.getLogin(entry.getKey(), entry.getValue());
			    this.getURLContent();
	    		this.getDriver().close();
	    }
	}

    public void getLogin( String username, String password) {
    	String tmpBaseUrl = "https://www.zillow.com/user/acct/login/";

		 this.getDriver().get(tmpBaseUrl);

		 //this.getDriver().get(tmpBaseUrl);
	try{
		        ExpectedCondition<Boolean> pageLoadCondition = new
		                ExpectedCondition<Boolean>() {
		                    public Boolean apply(WebDriver driver) {
		                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
		                    }
		                };

		        WebDriverWait wait = new WebDriverWait(this.getDriver(), 30);
		        wait.until(pageLoadCondition);
    	        Thread.sleep(1000);

		        System.out.println("username " + username);

		        System.out.println("password " + password);


				 for(int j=0;j<50;j++){
				        WebElement temp = this.getDriver().switchTo().activeElement();
				        System.out.println("ELEMENT TEXT " + temp.getText());
			        	if(temp!=null && temp.getAttribute("type")!=null && temp.getAttribute("type").startsWith("email")
			        			){
					        System.out.println("username " + username);
		        			temp.sendKeys(username);
		        	        Thread.sleep(1000);
			        	}
			        	if(temp!=null && temp.getAttribute("type")!=null && temp.getAttribute("type").startsWith("password")){
			        		System.out.println("password " + password);
					        temp.sendKeys(password);
		        	        Thread.sleep(1000);
			        	}
			        	if(temp!=null && temp.getAttribute("value")!=null && temp.getAttribute("value").equals("Sign in")){
		        			temp.click();
		        	        wait.until(pageLoadCondition);
		        	        Thread.sleep(500);
		        			break;
			        	}
				        temp.sendKeys(Keys.TAB);
				 }
	        wait.until(pageLoadCondition);
	        Thread.sleep(500);
		}catch(Exception ex){
			ex.printStackTrace();
		}
    }

    public void getURLContent() {
//    	String tmpBaseUrl = "https://www.zillow.com/homedetails/1404-N-Buena-Vista-St-Burbank-CA-91505/20047964_zpid/";
    	String tmpBaseUrl = "https://www.zillow.com/homes/607-N-Curson-Ave-Los-Angeles,-CA,-90036_rb/20785228_zpid/";

		try{
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };

	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 30);
	        wait.until(pageLoadCondition);
	        
			 this.getDriver().get(tmpBaseUrl);
		        List<WebElement> element = this.getDriver().findElements(By.tagName("span"));	
		        for (WebElement temp : element) {
					try{
		        		System.out.println("TEXT AFTER LOGIN " + temp.getText());
		        		System.out.println("TEXT AFTER LOGIN " + temp.getAttribute("class"));
			        	if(temp!=null && temp.getAttribute("class")!=null && temp.getAttribute("class").equals("ds-action-bar-label")
		        		   && temp.getText()!=null && temp.getText().equals("Save")
					        		){	       		    	        
			        			temp.click();
			        	        wait.until(pageLoadCondition);
			        	        Thread.sleep(1000);
			        	        break;
			        	}	 
			        } catch (Exception expected) {
			        	expected.printStackTrace();
			        }	
		        }

		}catch(Exception ex){
			ex.printStackTrace();
		}
    }
	public UserManagerZillow getUsrMngrZillow() {
		return usrMngrZillow;
	}
	public void setUsrMngrZillow(UserManagerZillow usrMngrZillow) {
		this.usrMngrZillow = usrMngrZillow;
	}
}
