package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT base class performs common task for each bot
 *  Armen Merikyan
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.UserManagerTwitter;

public abstract class WebsiteCrawlerTwitterBase {
	private WebDriver driver = null;

	private UserManagerTwitter usrMngr = null;
	private HashMap<String, String> usrMap = null;
	private String baseUrl = "https://twitter.com";

	public WebsiteCrawlerTwitterBase() {
		// TODO Auto-generated constructor stub
		
	}
	public void getStart(){
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
		this.setUsrMngr(new UserManagerTwitter());
		this.setUsrMap(this.getUsrMngr().getAllUsers());
		
		for (Map.Entry<String,String> entry : this.getUsrMap().entrySet())  {	
			this.setDriver(new FirefoxDriver());
	    	this.getLogin(entry.getKey(), entry.getValue());
	    	this.getURLContent();
	    	this.getDriver().close();
	    }
	}
	abstract public void getURLContent();
	
	public void getDownTonight(){
		try{
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };	                
	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 30);
	        wait.until(pageLoadCondition);
	        JavascriptExecutor jse = (JavascriptExecutor)this.getDriver();
	        for(int j=0;j<Constant.scrlCount;j++){
	        	jse.executeScript("window.scrollBy(0,250)", "");
	        	Thread.sleep(100);
		        System.out.println(j);
			}	
		}catch(Exception ex){
			ex.printStackTrace();
		}	
	}
	
    public void getLogin( String username, String password) {
    	System.out.println(this.getBaseUrl());
		try{
	    	this.driver.get(this.getBaseUrl());
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };

	        WebDriverWait wait = new WebDriverWait(this.driver, 30);
	        wait.until(pageLoadCondition);

	    	this.driver.get(this.getBaseUrl() + "/login");
	        // wait ten seconds for Capcha test to manually bypass
	        wait.until(pageLoadCondition);
	        Thread.sleep(2000);


	        List<WebElement> element = this.driver.findElements(By.tagName("input"));

	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("name"));
		        	if(temp!=null && temp.getAttribute("name")!=null && temp.getAttribute("name").startsWith("session[username")){
		        			temp.sendKeys(username);
		        	        Thread.sleep(1000);
		        	}
		        	if(temp!=null && temp.getAttribute("name")!=null && temp.getAttribute("name").startsWith("session[password")){
	        			temp.sendKeys(password);
	        	        Thread.sleep(2000);
		        	}
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }
	        }

	        wait.until(pageLoadCondition);
	        element = this.driver.findElements(By.tagName("button"));

	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("type"));
		        	if(temp!=null && temp.getAttribute("type")!=null && temp.getAttribute("type").startsWith("submit")
		        			&& temp.getText()!=null
		        			){
				        	if(temp.getText().startsWith("Log in")){
			        			temp.click();
			        			break;
				        	}
		        	}
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }
	        }



		}catch(Exception ex){
			ex.printStackTrace();
		}
    }
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	public UserManagerTwitter getUsrMngr() {
		return usrMngr;
	}
	public void setUsrMngr(UserManagerTwitter usrMngr) {
		this.usrMngr = usrMngr;
	}
	public HashMap<String, String> getUsrMap() {
		return usrMap;
	}
	public void setUsrMap(HashMap<String, String> usrMap) {
		this.usrMap = usrMap;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
}
