<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="com.salesiq.ops.LocationMDM,java.util.Vector,com.salesiq.dom.SiqLocation,com.salesiq.mysql.login.LocationManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">
      <jsp:include page='navbar_top_siq.jsp' />
      <div class="container">
        <div class="jumbotron bg-white mt-3">
        <div id="content" style="text-align: left">
          <h1>View Duplicate Locations</h1>
          <a href="salesiq.menu.jsp" >Menu</a><BR>
          <a href="salesiq.view.loc.dup.jsp?mergeDups=true" >Merge Duplicates</a><BR><BR>
          <div class="container">

              <div class="row">
                <div class="col-sm" style="text-align: left">
                  Name
                </div>
                <div class="col-sm">
                  Address
                </div>
                <div class="col-sm">
                  City
                </div>
                <div class="col-sm">
                  Created
                </div>
                <div class="col-sm">
                  Updated
                </div>
              </div>
          <%
            LocationMDM tmpMgr = new LocationMDM();

            if(request.getParameter("mergeDups")!=null){
                tmpMgr.mergLocations((String)session.getAttribute("uid"));
            }
            Vector<SiqLocation> custList = tmpMgr.getLocationsDups((String)session.getAttribute("uid"));

                 for(int j=0;j<custList.size();j++){
                   SiqLocation tmpcust = custList.elementAt(j);
                   if(tmpcust.getDup_loc().size()>0){
                   %>
                     <div class="row">
                       <div class="col-sm" style="text-align: left">
                         <a href="salesiq.edit.loc.jsp?cust_id=<%=tmpcust.getSiq_id()%>" ><%=tmpcust.getNameShort()%></a>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getAdd()%>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getCity()%>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getTsString()%>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getTs_updateString()%>
                       </div>
                     </div>
                   <%

                    Vector<SiqLocation> custListDup = tmpcust.getDup_loc();
                      for(int k=0;k<custListDup.size();k++){
                        SiqLocation tmpcustdup = custListDup.elementAt(k);
                        %>
                          <div class="row">
                            <div class="col-sm" style="text-align: left">
                              Duplicate <a href="salesiq.edit.loc.jsp?loc_id=<%=tmpcustdup.getSiq_id()%>" ><%=tmpcustdup.getNameShort()%></a>
                            </div>
                            <div class="col-sm">
                              <%=tmpcustdup.getAdd()%>
                            </div>
                            <div class="col-sm">
                              <%=tmpcustdup.getCity()%>
                            </div>
                            <div class="col-sm">
                              <%=tmpcustdup.getTsString()%>
                            </div>
                            <div class="col-sm">
                              <%=tmpcustdup.getTs_updateString()%>
                            </div>
                          </div>
                        <%
                      }
                    }
                 }
              %>
        </div>
      </div>
      </div>
    </div>
  </body>
</html>
