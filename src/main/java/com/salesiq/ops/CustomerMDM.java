package com.salesiq.ops;

import java.util.HashMap;
import java.util.Vector;

import com.salesiq.dom.SiqCustomer;
import com.salesiq.mysql.login.CallbackManager;
import com.salesiq.mysql.login.CustomerManager;

public class CustomerMDM {

	public CustomerMDM() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CustomerMDM tmpMDM = new CustomerMDM();
		tmpMDM.mergCustomers("a9914eb5-07ac-40fc-8aab-19a5600646b0");
		/*
		Vector<SiqCustomer> tmplist = tmpMDM.getCustomersDups("a9914eb5-07ac-40fc-8aab-19a5600646b0");
		for(int j=0;j<tmplist.size();j++){
			SiqCustomer tmpcb = tmplist.elementAt(j);
			if(tmpcb.getDup_cust().size()>0){
				for(int k=0;k<tmpcb.getDup_cust().size();k++){
					SiqCustomer tmpDup = tmpcb.getDup_cust().elementAt(k);
					System.out.print("Has Dup " + tmpcb.getName());
					System.out.print("Has Dup " + tmpcb.getPhone());
					System.out.print("Has Dup " + tmpDup.getPhone());
					System.out.print("Has Dup " + tmpcb.getSiq_id());
					System.out.println("Has Dup " + tmpDup.getSiq_id());
				}
			}	
		}
		*/
	}
	public Vector<SiqCustomer> getCustomersDups(String user_id){
		CustomerManager tmp = new CustomerManager();
		Vector<SiqCustomer> tmplist = tmp.getCustomersMDM(user_id);
		for(int j=0;j<tmplist.size();j++){
			SiqCustomer tmpcb = tmplist.elementAt(j);
			System.out.println("HELL O " + tmpcb.getName());
			System.out.println("HELL O " + tmpcb.getTsString());
			System.out.println("HELL O " + tmpcb.getPhone());
			for(int k=0;k<tmplist.size();k++){
				SiqCustomer tmpDUP = tmplist.elementAt(k);
				if(tmpDUP.getPhone() != null && tmpcb.getPhone() != null && tmpDUP.getPhone().equals(tmpcb.getPhone())){
					if(!tmpcb.getSiq_id().equals(tmpDUP.getSiq_id())){
						tmpcb.getDup_cust().add(tmpDUP);
						tmplist.set(j, tmpcb);
					}
				}
			}
		}		
		return tmplist;
	}
	public void mergCustomers(String user_id){
		Vector<SiqCustomer> tmplist = getCustomersDups(user_id);
		HashMap<String,String> removedItems = new HashMap<String, String>();
		for(int j=0;j<tmplist.size();j++){
			SiqCustomer tmpcb = tmplist.elementAt(j);
			if(tmpcb.getDup_cust().size()>0 && !removedItems.containsKey(tmpcb.getSiq_id())){
				for(int k=0;k<tmpcb.getDup_cust().size();k++){
					SiqCustomer tmpDup = tmpcb.getDup_cust().elementAt(k);
					System.out.print("Has Dup " + tmpcb.getName());
					System.out.print("Has Dup " + tmpcb.getPhone());
					System.out.print("Has Dup " + tmpDup.getPhone());
					System.out.print("Has Dup " + tmpcb.getSiq_id());
					System.out.println("Has Dup " + tmpDup.getSiq_id());
					CustomerManager cmtMgr = new CustomerManager();
					cmtMgr.removeCustomer(user_id, tmpDup.getSiq_id());
					removedItems.put(tmpDup.getSiq_id(), tmpDup.getSiq_id());
					CallbackManager cbMgr = new CallbackManager();
					cbMgr.mergeCallback(user_id, tmpcb.getSiq_id(), tmpDup.getSiq_id());
					
				}
			}	
		}		
		//return true;
	}
}
