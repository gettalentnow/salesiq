package com.salesiq.mysql.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import com.salesiq.dom.SiqCallback;
import com.salesiq.dom.SiqCustomer;
import com.salesiq.dom.SiqLocation;
import com.salesiq.dom.SiqScript;
import com.salesiq.ops.SendEmail;
import com.salesiq.ops.SendSMS;

public class ScriptManager {

	public ScriptManager() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ScriptManager tmpMgr = new ScriptManager();
		//tmpMgr.addScriptMerged("tmp name", "the sales script", "status", "user id", "user id");
		//tmpMgr.editScript("c9b916a4-882b-49fc-aa67-1b4fb7feb5b1", "tmp name update", "the sales script", "status", "fa0bd40c-eb36-481b-ae4f-6e44373fd233");
		Vector<SiqScript> tmpSc = tmpMgr.getScriptsUsed( "fa0bd40c-eb36-481b-ae4f-6e44373fd233", "3e7ae3d1-b9dd-43b6-ad64-63475478fe1f");
		System.out.print(tmpSc.size());
	}
	public SiqScript getScript(String siq_user_id, String siq_sc_id){
	    try
	    {

	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_scripts where siq_scripts.siq_user_id = ? and siq_scripts.siq_sc_id = ? order by ts desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      preparedStmt.setString (2, siq_sc_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqScript tmpSc = new SiqScript();
	    	  tmpSc.setSiq_id(resultSet.getString("siq_sc_id"));
	    	  tmpSc.setName(resultSet.getString("script_name"));
	    	  tmpSc.setScript(resultSet.getString("script"));
	    	  tmpSc.setStatus(resultSet.getString("status"));
	    	  tmpSc.setTs(resultSet.getTimestamp("ts"));
	    	  tmpSc.setTs_update(resultSet.getTimestamp("ts_update"));
			  resultSet.close();
			  preparedStmt.close();
			  conn.close();
	    	  return tmpSc;
	      }

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
	    return null;
	}
	public SiqScript getScript(String siq_user_id, String siq_sc_id, String siq_cust_id){
		SiqScript tmpScrpt = getScript(siq_user_id, siq_sc_id);
		CustomerManager tmpMgr = new CustomerManager();
		SiqCustomer tmpCust = tmpMgr.getCustomer(siq_user_id, siq_cust_id);
		tmpScrpt.setScript_merged((tmpScrpt.getScript().replace("<NAME>", tmpCust.getName())).replace("<PHONE>", tmpCust.getPhone()));
	    return tmpScrpt;
	}
	public Vector<SiqScript> getScripts(String siq_user_id, boolean sortByCBDate){
		Vector<SiqScript> tmpList = new Vector<SiqScript>();
	    try
	    {

	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_scripts where siq_scripts.siq_user_id = ? order by ts desc";
	      if(sortByCBDate)query = "select * from siq_scripts where siq_scripts.siq_user_id = ? order by ts_update desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqScript tmpSc = new SiqScript();
	    	  tmpSc.setSiq_id(resultSet.getString("siq_sc_id"));
	    	  tmpSc.setName(resultSet.getString("script_name"));
	    	  tmpSc.setScript(resultSet.getString("script"));
	    	  tmpSc.setStatus(resultSet.getString("status"));
	    	  tmpSc.setTs(resultSet.getTimestamp("ts"));
	    	  tmpSc.setTs_update(resultSet.getTimestamp("ts_update"));
	    	  tmpList.add(tmpSc);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return tmpList;
	}

	public Vector<SiqScript> getScripts(String siq_user_id, String filterByStatus){
		Vector<SiqScript> tmpList = new Vector<SiqScript>();
	    try
	    {

	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_scripts where siq_scripts.siq_user_id = ? and siq_scripts.status = ? order by ts desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      preparedStmt.setString (2, filterByStatus);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqScript tmpSc = new SiqScript();
	    	  tmpSc.setSiq_id(resultSet.getString("siq_sc_id"));
	    	  tmpSc.setName(resultSet.getString("script_name"));
	    	  tmpSc.setScript(resultSet.getString("script"));
	    	  tmpSc.setStatus(resultSet.getString("status"));
	    	  tmpSc.setTs(resultSet.getTimestamp("ts"));
	    	  tmpSc.setTs_update(resultSet.getTimestamp("ts_update"));
	    	  tmpList.add(tmpSc);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return tmpList;
	}

	public Vector<SiqScript> getScriptsUsed(String siq_user_id, String siq_cust_id){
		Vector<SiqScript> tmpList = new Vector<SiqScript>();
	    try
	    {

	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_scripts_used where siq_scripts_used.siq_user_id = ? and siq_scripts_used.siq_cust_id = ? order by ts desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      preparedStmt.setString (2, siq_cust_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqScript tmpSc = new SiqScript();
	    	  tmpSc.setSiq_id(resultSet.getString("siq_sc_id"));
	    	  tmpSc.setName(resultSet.getString("script_name"));
	    	  tmpSc.setScript_merged(resultSet.getString("script_used"));
	    	  tmpSc.setTs(resultSet.getTimestamp("ts"));
	    	  tmpList.add(tmpSc);
	      }
		  resultSet.close();
	      preparedStmt.close();
	      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return tmpList;
	}
	public String insertScript(SiqScript newSc){
	    try
	    {

	      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = " insert into siq_scripts (siq_sc_id, siq_user_id, siq_bus_id, ts, ts_update, script, script_name, status)"
	        + " values (?, ?, ?, ?, ?, ?,?,?)";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, UUID.randomUUID().toString());
	      preparedStmt.setString (2, newSc.getUser_id());
	      preparedStmt.setString (3, newSc.getBus_id());
	      preparedStmt.setTimestamp(4, currentTimestamp);
	      preparedStmt.setTimestamp(5, currentTimestamp);
	      preparedStmt.setString (6, newSc.getScript());
	      preparedStmt.setString (7, newSc.getName());
	      preparedStmt.setString (8, newSc.getStatus());
	      preparedStmt.execute();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error " + e.getMessage();
	      return "Error: System Error ";
	    }
		return "Script Added";
	}
	public String insertScriptMerged(SiqScript newSc){
	    try
	    {

	      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = " insert into siq_scripts_used(siq_sc_u_id, siq_sc_id, siq_cust_id, siq_user_id, siq_bus_id, ts, script_used, script_name, medium)"
	        + " values (?, ?, ?, ?, ?, ?,?,?,?)";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, UUID.randomUUID().toString());
	      preparedStmt.setString (2, newSc.getSiq_id());
	      preparedStmt.setString (3, newSc.getCust_id());
	      preparedStmt.setString (4, newSc.getUser_id());
	      preparedStmt.setString (5, newSc.getBus_id());
	      preparedStmt.setTimestamp(6, currentTimestamp);
	      preparedStmt.setString (7, newSc.getScript_merged());
	      preparedStmt.setString (8, newSc.getName());
	      preparedStmt.setString (9, newSc.getMedium());
	      preparedStmt.execute();
	      preparedStmt.close();
	      conn.close();
	      if(newSc.getMedium()!=null){
				  CustomerManager cmgr = new CustomerManager();
	              SiqCustomer tmpC = cmgr.getCustomer(newSc.getUser_id(), newSc.getCust_id());
				  if(newSc.getMedium().equals("email")) {
					  if (tmpC.getDesc().contains("<EMAIL>")) {
						  String cusomterEmail = tmpC.getDesc().split("<EMAIL>")[1];
						  cusomterEmail = cusomterEmail.split("<EMAIL>")[0];
						  SendEmail sndEmail = new SendEmail();
						  sndEmail.sendEMailScript(newSc.getScript_merged(), cusomterEmail);
					  }

				  }else if(newSc.getMedium().equals("sms")) {
				  	  String customerSMS = new String();
					   customerSMS = tmpC.getPhone();
					  if (tmpC.getDesc().contains("<SMS>")) {
						  customerSMS = tmpC.getDesc().split("<SMS>")[1];
						  customerSMS = customerSMS.split("<SMS>")[0];
					  }
					  SendSMS tmpSend = new SendSMS();
					  tmpSend.sendSMS(customerSMS, newSc.getScript_merged());
				  }
		  }
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      return "Error: System Error " + e.getMessage();
	      //return "Error: System Error ";
	    }
		return "Script Added";
	}
	public String addScript(String scName, String scScript, String scStatus, String siq_user_id){
		try{
			SiqScript tmpSc = new SiqScript();
			tmpSc.setName(scName);
			tmpSc.setStatus(scStatus);
			tmpSc.setScript(scScript);
			tmpSc.setUser_id(siq_user_id);
			return insertScript(tmpSc);

		}catch (Exception e)
	    {
	    	return "Error: System Error ";
	    }
	}
	public String addScriptMerged(String scName, String scScript_Merged, String siq_cust_id, String siq_user_id, String siq_sc_id, String medium){
		try{
			SiqScript tmpSc = new SiqScript();
			tmpSc.setName(scName);
			tmpSc.setScript_merged(scScript_Merged);
			tmpSc.setUser_id(siq_user_id);
			tmpSc.setCust_id(siq_cust_id);
			tmpSc.setSiq_id(siq_sc_id);
			tmpSc.setMedium(medium);
			return insertScriptMerged(tmpSc);

		}catch (Exception e)
	    {
	    	return "Error: System Error ";
	    }
	}

	public String editScript(String scId, String scName, String scScript, String scStatus, String siq_user_id){
		SiqScript tmpSc = new SiqScript();
		tmpSc.setSiq_id(scId);
		tmpSc.setName(scName);
		tmpSc.setScript(scScript);
		tmpSc.setStatus(scStatus);
		tmpSc.setUser_id(siq_user_id);
		return updateScript(tmpSc);
	}

	public String updateScript(SiqScript newSc){
		try{
		      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
		      MySQLConstants tmpCon = new MySQLConstants();
		      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	          Class.forName("com.mysql.jdbc.Driver").newInstance();
		      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
		      String query = "update siq_scripts set script = ?, script_name = ?, status = ?, ts_update = ?"
		        + " where siq_scripts.siq_sc_id = ? and siq_scripts.siq_user_id = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, newSc.getScript());
		      preparedStmt.setString (2, newSc.getName());
		      preparedStmt.setString (3, newSc.getStatus());
		      preparedStmt.setTimestamp(4, currentTimestamp);
		      preparedStmt.setString (5, newSc.getSiq_id());
		      preparedStmt.setString (6, newSc.getUser_id());
		      preparedStmt.execute();
		      preparedStmt.close();
		      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      return "Error: System Error";
	    }
		return "Customer Added";
	}
}
