package com.gtn.search.servlet;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.*;
import javax.servlet.http.*;

import com.gtn.search.ops.SearchWordMaker;
import com.gtn.search.ops.TypeAhead;

// Extend HttpServlet class
public class TypeAheadServlet extends HttpServlet {
 
   /**
	 * 
	 */
	private static final long serialVersionUID = -1516998571043414101L;
	private static HashMap<String, String> wordMap = null;
	private static HashMap<String, String> wordWeedMap = null;

   public void init() throws ServletException {
      //NONE RIGHT NOW
	   String urlWords = "https://s3.us-east-2.amazonaws.com/askbookman/words.txt";
	   String urlWeedWords = "https://s3.us-east-2.amazonaws.com/askbookman/words_weed.txt";
	   SearchWordMaker tmpmaker = new SearchWordMaker();
	   wordMap = tmpmaker.getWords(new HashMap<String, String>(), urlWords);
	   wordWeedMap = tmpmaker.getWords(new HashMap<String, String>(), urlWeedWords);
   }

   public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
      
	    HashMap<String, String> wordCurMap = wordMap;
	    if(request.getHeader("Host")!=null && request.getHeader("Host").contains("weed")){
	    	wordCurMap = wordWeedMap;
	    }
	    if(request.getParameter("reloadwords")!=null && request.getParameter("reloadwords").equals("true")){
	    	this.init();
	    }
      // Set response content type
      response.setContentType("text/html");
      String keyWord = request.getRequestURI();
      String[] keyWordArray = keyWord.split("/");
      keyWord = keyWordArray[keyWordArray.length-1];
      System.out.println(keyWord);
      keyWord = keyWord.substring(0, keyWord.indexOf("json")-1);
      String[] keyWords = keyWord.split("%20"); 
      if(keyWords.length>1){
    	  keyWord = keyWords[keyWords.length -1];
      }
      String tmpResponse = "";
      for (Map.Entry<String,String> entry : wordCurMap.entrySet())  {
    	  if(entry.getKey().startsWith(keyWord)){		
    		  // COPY BELOW 
    		  boolean addItem = true;
    		  if(keyWords.length>1){
    			  for(int j = 0;j<keyWords.length - 1;j++){
    				  if(entry.getKey().equals(keyWords[j].toLowerCase()))addItem=false;
    			  }
    		  }
    		  String newWordWithSpace = entry.getKey();
    		  if(keyWords.length>1){
    			  newWordWithSpace = "";
    			  for(int j = 0;j<keyWords.length - 1;j++){
    				  newWordWithSpace = newWordWithSpace + keyWords[j] + " ";
    			  }		  
    			  newWordWithSpace = newWordWithSpace + entry.getKey();
    		  }
    		  if(addItem)tmpResponse = tmpResponse + "\"" + TypeAhead.camelCase(newWordWithSpace) + "\"" + " ";  
    		  // COPY BELOW
    	  }	  
      }
      for (Map.Entry<String,String> entry : wordCurMap.entrySet())  {
    	  if(entry.getKey().contains(keyWord) && !entry.getKey().startsWith(keyWord)){		
    		  // COPY PUT IT IN A LOOP LATER
    		  boolean addItem = true;
    		  if(keyWords.length>1){
    			  for(int j = 0;j<keyWords.length - 1;j++){
    				  if(entry.getKey().equals(keyWords[j].toLowerCase()))addItem=false;
    			  }
    		  }
    		  String newWordWithSpace = entry.getKey();
    		  if(keyWords.length>1){
    			  newWordWithSpace = "";
    			  for(int j = 0;j<keyWords.length - 1;j++){
    				  newWordWithSpace = newWordWithSpace + keyWords[j] + " ";
    			  }		  
    			  newWordWithSpace = newWordWithSpace + entry.getKey();
    		  }
    		  if(addItem)tmpResponse = tmpResponse + "\"" + TypeAhead.camelCase(newWordWithSpace) + "\"" + " ";  
    		  // COPY
    	  }
      }	  
    	 
      tmpResponse = tmpResponse.trim();
      System.out.println(tmpResponse);
      tmpResponse = tmpResponse.replace("\" \"", "\",\"");
      System.out.println(tmpResponse);
      // Actual logic goes here.
      PrintWriter out = response.getWriter();
      out.println("[" + tmpResponse + "]");
   }

   public void destroy() {
      // do nothing.
   }
}