package com.gtn.search;

public class WebSiteItem {
	public WebSiteItem() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String link ;
	private String name ;
	private String desc ;
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

}
