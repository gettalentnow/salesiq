package com.big.search.maryjane.persistence.dynamodb;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PutItemOutcome;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

public class VideoManagerOther {

	public VideoManagerOther() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		VideoManagerOther tmp = new VideoManagerOther();
		tmp.putVideo("ha ha this was a very funny episode", "YES");
	}
	public ArrayList<String> getAll(){
		ArrayList<String> tmpMap = new ArrayList<String>();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		Map<String, AttributeValue> lastKeyEvaluated = null;
		do {
		    ScanRequest scanRequest = new ScanRequest()
		        .withTableName("youtube_videos_other")
		        .withLimit(10)
		        .withExclusiveStartKey(lastKeyEvaluated);
	
		    ScanResult result = client.scan(scanRequest);
		    for (Map<String, AttributeValue> item : result.getItems()){
		    		tmpMap.add(item.get("id").getS());
		    }
		    lastKeyEvaluated = result.getLastEvaluatedKey();
		} while (lastKeyEvaluated != null);
		return tmpMap;		
	}

	public void putVideo(String id, String account) {
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_WEST_1).build();
		com.amazonaws.services.dynamodbv2.document.DynamoDB dynamoDB = new com.amazonaws.services.dynamodbv2.document.DynamoDB(client);
		Table table = dynamoDB.getTable("youtube_videos_other");
		Timestamp ts = new Timestamp(new Date().getTime());
		Item item = new Item()
		    .withPrimaryKey("id", id)
		    .withString("account", account)
		    .withString("visit_time", ts + "");
		PutItemOutcome outcome = table.putItem(item);		
	    System.out.println("Put Item Outcome: " + outcome.getPutItemResult());
				
	}  
}
