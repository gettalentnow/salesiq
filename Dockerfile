FROM java:8-jre
#https://github.com/docker-library/tomcat

ENV CATALINA_HOME /usr/local/tomcat
ENV PATH $CATALINA_HOME/bin:$PATH

RUN mkdir -p "$CATALINA_HOME"
WORKDIR $CATALINA_HOME

# see https://www.apache.org/dist/tomcat/tomcat-8/KEYS

ENV TOMCAT_MAJOR 8
ENV TOMCAT_VERSION 8.5.50
ENV TOMCAT_TGZ_URL https://www.apache.org/dist/tomcat/tomcat-$TOMCAT_MAJOR/v$TOMCAT_VERSION/bin/apache-tomcat-$TOMCAT_VERSION.tar.gz

RUN set -x \
    && curl -fSL "$TOMCAT_TGZ_URL" -o tomcat.tar.gz \
    && curl -fSL "$TOMCAT_TGZ_URL.asc" -o tomcat.tar.gz.asc \
    && tar -xvf tomcat.tar.gz --strip-components=1 \
    && rm bin/*.bat \
    && rm tomcat.tar.gz*

RUN rm -rf $CATALINA_HOME/webapps/examples/*
RUN rm -rf $CATALINA_HOME/webapps/ROOT/favicon.ico
COPY tomcatlib/. $CATALINA_HOME/lib/
COPY index_new.jsp $CATALINA_HOME/webapps/ROOT/index.jsp
COPY mysql_new_new_new.properties $CATALINA_HOME/conf/mysql.properties
ADD target/gtn-search-1.0-SNAPSHOT.war $CATALINA_HOME/webapps/gtnsearch.war

EXPOSE 8080
CMD ["catalina.sh", "run"]
