package com.gtn.search.cloudsearch;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import com.gtn.search.WebSiteItem;

public class AWSGTNBasicSearch {

	public AWSGTNBasicSearch() {
		super();
		
		// TODO Auto-generated constructor stub
	}
	public static void main(String[] args) {
		AWSGTNBasicSearch search = new AWSGTNBasicSearch();
		HashMap<String, WebSiteItem> siteMap = search.cmdQueryCloudSearch(new HashMap<String, WebSiteItem>(), "higherpath | blog -weed");
        for (Map.Entry<String,WebSiteItem> entry : siteMap.entrySet())  {
        	System.out.println(entry.getKey()); 
        	System.out.println(entry.getValue().getDesc()); 
        }      
		//  for (Map.Entry<String,WebSiteItem> entry : siteMap.n)  {
      //  	System.out.println(entry.getValue().getLink()); 
      //  	System.out.println(entry.getValue().getDesc()); 
      //  }
	}
	public HashMap<String, WebSiteItem> cmdQueryCloudSearch(HashMap<String, WebSiteItem> searchMap, String searchString) {
		try{

			searchString = URLEncoder.encode(searchString, "UTF-8");
			URL oracle = new URL("https://search-askweedman-pfmoradg42ubszstfdomnhptxq.us-west-1.cloudsearch.amazonaws.com/2013-01-01/search?size=50&q=" + searchString);
	        BufferedReader in = new BufferedReader(
	        new InputStreamReader(oracle.openStream()));
	        String inputLine;
	        String searchResult = "";
	        while ((inputLine = in.readLine()) != null){
	        	searchResult = searchResult + inputLine;
	        }
	        in.close();
	        System.out.println(searchResult);
	        String[] resultArray = searchResult.split("\"id\"");
	        for(int j=1;j<resultArray.length;j++){
	        	WebSiteItem tmpItem = new WebSiteItem();
		        //System.out.println("ITEM : " + resultArray[j].split("url\":\"")[1].split("\"")[0]);
		        //System.out.println("KEY : " + resultArray[j].split("keyword_list\":\"")[1].split("\"")[0]);
		        tmpItem.setLink(resultArray[j].split("url\":\"")[1].split("\"")[0]);
		        String desc = resultArray[j].split("keyword_list\":\"")[1].split("\"")[0];
		        tmpItem.setDesc(resultArray[j].split("keyword_list\":\"")[1].split("\"")[0]);
		        if(tmpItem.getDesc().length()>300){
		        	String tmpDescEnd = tmpItem.getDesc().substring(300,tmpItem.getDesc().length());		        	
		        	tmpItem.setDesc(tmpItem.getDesc().substring(0,300) + tmpDescEnd.split(" ")[0] + " ...");
		        }		        
		        searchMap.put(tmpItem.getLink(), tmpItem);
	        }
		} catch(Exception ex){
			System.out.println(ex.getMessage());
		}    
		return searchMap;
	}

}
