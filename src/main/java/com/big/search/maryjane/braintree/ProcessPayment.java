package com.big.search.maryjane.braintree;

import java.math.BigDecimal;

import com.big.search.maryjane.persistence.dynamodb.BrainTreeToken;
import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionRequest;

public class ProcessPayment {

	public ProcessPayment() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ProcessPayment tmpPayment = new ProcessPayment();
		System.out.println(tmpPayment.processPayment("19388e69-5726-0f51-2cb9-6fee10e27358"));

	}
	public String getClientToken(){
		BrainTreeToken tmpToken = new BrainTreeToken();
		BraintreeGateway gateway = new BraintreeGateway(tmpToken.getAll().get(0));
		return gateway.clientToken().generate();
	}
	public String processPayment(String nonce){
		BrainTreeToken tmpToken = new BrainTreeToken();
		BraintreeGateway gateway = new BraintreeGateway(tmpToken.getAll().get(0));
			TransactionRequest request = new TransactionRequest()
					  .amount(new BigDecimal("95.00"))
					  .paymentMethodNonce(nonce)
					  .options()
					    .submitForSettlement(true)
					    .done();
		
					Result<Transaction> result = gateway.transaction().sale(request);	
					
					return result.getMessage();
	}		
}
