<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="java.util.Vector,com.salesiq.dom.SiqCustomer,com.salesiq.mysql.login.CustomerManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">
      <jsp:include page='navbar_top_siq.jsp' />
      <div class="container">
        <div class="jumbotron bg-white mt-3">
        <div id="content" style="text-align: left">
          <h1>View Customers</h1>
          <a href="salesiq.menu.jsp" >Menu</a><BR>
          Sort by customer <a href="salesiq.view.cust.jsp" >Created</a> or
          <a href="salesiq.view.cust.jsp?sortBy=tsupdate" >Updated</a> Date<BR>
          Filter by customer
          <a href="salesiq.view.cust.jsp?filterBy=None" >None</a>,
          <a href="salesiq.view.cust.jsp?filterBy=Cold" >Cold</a>,
          <a href="salesiq.view.cust.jsp?filterBy=Warm" >Warm</a> or
          <a href="salesiq.view.cust.jsp?filterBy=Hot" >Hot</a>
          Status<BR><BR>
          <div class="container">

              <div class="row">
                <div class="col-sm" style="text-align: left">
                  Name
                </div>
                <div class="col-sm">
                  Phone
                </div>
                <div class="col-sm">
                  Status
                </div>
                <div class="col-sm">
                  Created
                </div>
                <div class="col-sm">
                  Updated
                </div>
              </div>
          <%
            CustomerManager tmpMgr = new CustomerManager();
            Vector<SiqCustomer> custList = new Vector<SiqCustomer>();
            if(request.getParameter("filterBy")!=null){
              custList = tmpMgr.getCustomers((String)session.getAttribute("uid"), request.getParameter("filterBy"));
            }else if(request.getParameter("sortBy")!=null && request.getParameter("sortBy").equals("tsupdate")){
              custList = tmpMgr.getCustomers((String)session.getAttribute("uid"), true);
            }else{
              custList = tmpMgr.getCustomers((String)session.getAttribute("uid"), false);
            }
                 for(int j=0;j<custList.size();j++){
                   SiqCustomer tmpcust = custList.elementAt(j);
                   %>
                     <div class="row">
                       <div class="col-sm" style="text-align: left">
                         <a href="salesiq.edit.cust.jsp?cust_id=<%=tmpcust.getSiq_id()%>" ><%=tmpcust.getNameShort()%></a>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getPhone()%>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getStatus()%>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getTsString()%>
                       </div>
                       <div class="col-sm">
                         <%=tmpcust.getTs_updateString()%>
                       </div>
                     </div>
                   <%
                 }
              %>
        </div>
      </div>
      </div>
    </div>
  </body>
</html>
