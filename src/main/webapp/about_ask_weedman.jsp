<%@ page session="false" import="com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
request.setAttribute("year", sdf.format(new java.util.Date()));
request.setAttribute("tomcatUrl", "https://tomcat.apache.org/");
request.setAttribute("tomcatDocUrl", "/docs/");
request.setAttribute("tomcatExamplesUrl", "/examples/");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>About Us</title>

            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
    </head>

    <body>
      <div class="container">
        <div class="jumbotron bg-white mt-3">
          <h1>About ASKWEEDMAN.COM </h1>
          <p>
            Cannabis, Weed, Mary Jane, Marijuana, 420 friendly web search engine for dispensaries, blogs, delivery services, smoke shops, housing rentals, jobs, events, social clubs...
            </p>
          <h1>How It Works</h1>
          <p>
            Searching for Text, You can search both text and literal fields (url, keyword_list, visit_time) for a text string.<BR>

            You can use the following prefixes to designate individual terms as required, optional, or to be excluded from the search results:<BR>
            + matching documents must contain the term. This is the default—separating terms with a space is equivalent to preceding them with the + prefix.<BR>
            - exclude documents that contain the term from the search results. The - operator only applies to individual terms. For example, to exclude documents that contain the term star in the default search field, specify: -star. Searching for search?q=-star wars retrieves all documents that do not contain the term star, but do contain the term wars.<BR>
            | include documents that contain the term in the search results, even if they don't contain the other terms. The | operator only applies to individual terms. For example, to include documents that contain either of two terms, specify: term1 |term2. Searching for search?q=star wars |trek includes documents that contain both star and wars, or the term trek.<BR>
          </p>
        </div>
      </div>
      <!-- Footer -->
      <footer class="page-footer font-small blue">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
          <a href="https://askbookman.com"> ASKBOOKMAN.COM</a> <a href="terms_of_service.html">Terms of Service</a> & <a href="terms_of_service.html" >Privacy Policy</a>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->


    </body>

</html>
