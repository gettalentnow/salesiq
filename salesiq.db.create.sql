CREATE TABLE `siq_callbacks` (
  `cb_id` varchar(50) NOT NULL,
  `cb_name` varchar(45) DEFAULT NULL,
  `cb_notes` varchar(100) DEFAULT NULL,
  `cb_datetime` datetime DEFAULT NULL,
  `ts` timestamp NULL DEFAULT NULL,
  `siq_cust_id` varchar(45) DEFAULT NULL,
  `siq_user_id` varchar(45) DEFAULT NULL,
  `siq_bus_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `siq_customers` (
  `siq_cust_id` varchar(50) NOT NULL,
  `siq_cust_name` varchar(45) DEFAULT NULL,
  `siq_cust_phone` varchar(45) DEFAULT NULL,
  `siq_cust_status` varchar(45) DEFAULT NULL,
  `siq_user_id` varchar(50) DEFAULT NULL,
  `siq_bus_id` varchar(50) DEFAULT NULL,
  `ts` varchar(45) DEFAULT NULL,
  `siq_cust_desc` varchar(500) DEFAULT NULL,
  `ts_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`siq_cust_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `siq_cust_totals` (
  `siq_cust_tot_id` varchar(50) NOT NULL,
  `ts` timestamp NULL DEFAULT NULL,
  `total_cust` int(11) DEFAULT NULL,
  `total_cust_hot` int(11) DEFAULT NULL,
  `total_cust_none` int(11) DEFAULT NULL,
  `total_cust_warm` int(11) DEFAULT NULL,
  `total_cust_cold` int(11) DEFAULT NULL,
  `siq_user_id` varchar(50) DEFAULT NULL,
  `siq_bus_id` varchar(50) DEFAULT NULL,
  `total_cb` int(11) DEFAULT NULL,
  PRIMARY KEY (`siq_cust_tot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `siq_locations` (
  `siq_loc_id` varchar(50) NOT NULL,
  `siq_user_id` varchar(50) DEFAULT NULL,
  `siq_bus_id` varchar(50) DEFAULT NULL,
  `loc_name` varchar(100) DEFAULT NULL,
  `loc_add` varchar(100) DEFAULT NULL,
  `loc_add2` varchar(100) DEFAULT NULL,
  `loc_state` varchar(45) DEFAULT NULL,
  `loc_city` varchar(100) DEFAULT NULL,
  `loc_zip` varchar(50) DEFAULT NULL,
  `loc_built_date` date DEFAULT NULL,
  `loc_notes` varchar(300) DEFAULT NULL,
  `loc_target_status` varchar(45) DEFAULT NULL,
  `loc_lat_long` point DEFAULT NULL,
  `ts` timestamp NULL DEFAULT NULL,
  `ts_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`siq_loc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `siq_scripts` (
  `siq_sc_id` varchar(50) NOT NULL,
  `siq_user_id` varchar(50) DEFAULT NULL,
  `siq_bus_id` varchar(50) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT NULL,
  `ts_update` timestamp NULL DEFAULT NULL,
  `script` varchar(1000) DEFAULT NULL,
  `script_name` varchar(50) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`siq_sc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `siq_scripts_used` (
  `siq_sc_u_id` varchar(50) NOT NULL,
  `siq_user_id` varchar(50) DEFAULT NULL,
  `siq_bus_id` varchar(50) DEFAULT NULL,
  `siq_cust_id` varchar(50) DEFAULT NULL,
  `script_used` varchar(1200) DEFAULT NULL,
  `ts` timestamp NULL DEFAULT NULL,
  `script_name` varchar(50) DEFAULT NULL,
  `siq_sc_id` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`siq_sc_u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `siq_users` (
  `siq_user_id` varchar(50) NOT NULL,
  `siq_user_phone` varchar(45) DEFAULT NULL,
  `siq_user_password` varchar(45) DEFAULT NULL,
  `ts` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`siq_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
