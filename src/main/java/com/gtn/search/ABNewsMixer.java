package com.gtn.search;

import java.util.TreeMap;
import java.util.Vector;

public class ABNewsMixer {

	public ABNewsMixer() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ABNewsMixer tmpMixer = new ABNewsMixer();
		System.out.println(tmpMixer.searchString("10.10.10.11", "JAVA TEST"));
	}
	public String searchString(String user_ip, String hostname) {
		 Vector<NewsItem> newsList = new Vector<NewsItem>();
		 
		 try{
			 newsList = ABNews.searchString(newsList);
			 System.out.println("TEST 1");
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 System.out.println("News LIST SIZE: " + newsList.size());
		 try{
			 newsList = ABNewsTMZ.searchString(newsList);
			 System.out.println("TEST 2");
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 System.out.println("News LIST SIZE: " + newsList.size());
		 try{
			 System.out.println("TEST 3");
			 newsList = ABNewsReuters.searchString(newsList);
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 System.out.println("News LIST SIZE: " + newsList.size());
		 try{
			 System.out.println("TEST 4");
			 newsList = ABNewsHighTimes.searchString(newsList);
		 }catch(Exception ex){
			 ex.printStackTrace();
		 }
		 String urlListReturn ="";

		 Vector<NewsItem> shuffledList = new Vector<NewsItem>();
		 System.out.println("News LIST SIZE: " + newsList.size());

		 for(int j=0;j<newsList.size();j++){
			 NewsItem tmpItem = newsList.elementAt(j);
			 if(tmpItem.getName().length()<2 || tmpItem.getDesc().length()<2){
				 newsList.removeElementAt(j);
			 }
		 }
		 int nextItem = 1;
		 while(newsList.size()>0){
			 for(int j=0;j<newsList.size();j++){
				 NewsItem tmpItem = newsList.elementAt(j);
				 if(tmpItem.getSource().equals("REUTERS") && nextItem ==1){
					shuffledList.add(newsList.elementAt(j));
					newsList.removeElementAt(j);
					break;
				 }	
				 if(tmpItem.getSource().equals("TMZ") && nextItem ==2){
					shuffledList.add(newsList.elementAt(j));
					newsList.removeElementAt(j);
					break;
				 }
				 if(tmpItem.getSource().equals("ESPN") && nextItem ==3){
					shuffledList.add(newsList.elementAt(j));
					newsList.removeElementAt(j);
					break;
				 }
				 if(tmpItem.getSource().equals("HIGHTIMES") && nextItem ==4){
					shuffledList.add(newsList.elementAt(j));
					newsList.removeElementAt(j);
					break;
				 }
			 }
			 nextItem += 1;
			 if(nextItem>4){
				 nextItem =1;
			 }
		 }
		 for(int j=0;j<shuffledList.size();j++){
			 NewsItem tmpItem = shuffledList.elementAt(j);
	     		urlListReturn = urlListReturn  
	     		+ "<a href=\"" + tmpItem.getLink()
	 		    + "\" >" + tmpItem.getName() + "</a>" 
	 		    + "<BR>" + tmpItem.getDesc()  + "<BR><BR>"; 			 
		 }
		 WebSiteEventManager tmpEventMgr = new WebSiteEventManager();
		 tmpEventMgr.logDynamoDb(user_ip, hostname, "NEWS-VISIT");
		 WebSiteVisitManager tmpVisitMgr = new WebSiteVisitManager();
		 tmpVisitMgr.logDynamoDb(user_ip);
		 return urlListReturn;
	}
}
