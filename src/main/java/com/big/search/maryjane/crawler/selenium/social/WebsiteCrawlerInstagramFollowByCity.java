package com.big.search.maryjane.crawler.selenium.social;
/*
 *  TWITTER BOT WRITEN IN JAVA 
 *  
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DataInstagramLocations;
import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.big.search.maryjane.persistence.dynamodb.UserManagerInstagram;
import com.big.search.maryjane.persistence.dynamodb.UserManagerLinkedIn;
import com.big.search.maryjane.persistence.dynamodb.UserManagerZillow;

public class WebsiteCrawlerInstagramFollowByCity extends WebsiteCrawlerInstagramBase{
	private DataInstagramLocations insLocations = null;
	private ArrayList<String> locationList = null;
	private String baseUrlLocation = null;
	
	public WebsiteCrawlerInstagramFollowByCity() {
		// TODO Auto-generated constructor stub
		super();

		
		
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		WebsiteCrawlerInstagramFollowByCity web = new WebsiteCrawlerInstagramFollowByCity();
		web.setInsLocations(new DataInstagramLocations());
		web.setLocationList(web.getInsLocations().getAll());
		web.getStart();
	}

	public void getStart(){
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
		this.setUsrMngr(new UserManagerInstagram());
		this.setUsrMap(this.getUsrMngr().getAllUsers());
		
	    for (Map.Entry<String,String> entry : this.getUsrMap().entrySet())  {	
	    	System.out.println("PROCESSING USER " + entry.getKey());
			this.setDriver(new FirefoxDriver());
	    	this.getLogin(entry.getKey(), entry.getValue());
			for(int j=0;j<this.getLocationList().size();j++){
				this.setBaseUrlLocation(this.getLocationList().get(j));
		    	this.getURLContent();
			}
	    	this.getDriver().close();        	
	    }
	}
	
    public void getURLContent() {        	
    	// https://www.linkedin.com/mynetwork/
    	
		try{
			
			ArrayList<String> linkList = new ArrayList<>();
		    this.getDriver().get(this.getBaseUrlLocation()); 
		    for(int j=0;j<10;j++){
		    	this.getDownTonight();
			}
	        for(int j=0;j<14;j++){
	        	WebElement tmpElement = this.getDriver().switchTo().activeElement();
		        tmpElement.sendKeys(Keys.TAB);
		        System.out.println("ELEMENT TEXT " + tmpElement.getText());
		        System.out.println("ELEMENT TEXT TAG" + tmpElement.getTagName());
		        System.out.println("ELEMENT TEXT CLASS " + tmpElement.getAttribute("href"));
		        Thread.sleep(1000);
		        if(tmpElement.getAttribute("href")!=null && tmpElement.getAttribute("href").startsWith("https://www.instagram.com/p/")){
		        	//tmpElement.click();
		        	//break;
		        	linkList.add(tmpElement.getAttribute("href"));
		        }
	        }    
	        
	        for(int j=0;j<linkList.size();j++){
	        	this.getURLContent(linkList.get(j));
	        	
	        }

	        
	        	                		
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
    public void getURLContent(String url) {    
	    this.getDriver().get(url); 

        for(int j=0;j<30;j++){
        	WebElement tmpElement = this.getDriver().switchTo().activeElement();
	        tmpElement.sendKeys(Keys.TAB);
	        System.out.println("ELEMENT TEXT " + tmpElement.getText());
	        System.out.println("ELEMENT TEXT TAG" + tmpElement.getTagName());
	        System.out.println("ELEMENT TEXT CLASS " + tmpElement.getAttribute("href"));
        }    
        List<WebElement> element = this.getDriver().findElements(By.tagName("button"));	
        for (WebElement temp : element) {
			try{
        		System.out.println(temp.getAttribute("type"));
	        	if(temp!=null && temp.getAttribute("type")!=null && temp.getAttribute("type").startsWith("button")
	        			&& temp.getText()!=null && temp.getText().startsWith("Follow")
	        			){	       		    	        
	        			temp.click();
	        	        Thread.sleep(400);
	        	}	 
	        } catch (Exception expected) {
	        	expected.printStackTrace();
	        }	
        }    	
    }
	public DataInstagramLocations getInsLocations() {
		return insLocations;
	}
	public void setInsLocations(DataInstagramLocations insLocations) {
		this.insLocations = insLocations;
	}
	public ArrayList<String> getLocationList() {
		return locationList;
	}
	public void setLocationList(ArrayList<String> locationList) {
		this.locationList = locationList;
	}
	public String getBaseUrlLocation() {
		return baseUrlLocation;
	}
	public void setBaseUrlLocation(String baseUrlLocation) {
		this.baseUrlLocation = baseUrlLocation;
	}
}
