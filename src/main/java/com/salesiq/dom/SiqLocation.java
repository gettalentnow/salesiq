package com.salesiq.dom;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class SiqLocation extends SiqBaseObject{

	private String add ;
	private String add2;
	private String city ;
	private String state;
	private String zip;
	
	// LAT LONG FORMAT newLoc.getLatlong() 
	// latLong = "POINT(" + request.getParameter("pickCity_lat") + " " + request.getParameter("pickCity_lng") + ")";
	// https://github.com/armenmerikyan/mytruckboard/blob/master/trucking-new/dispatch_edit_customers.jsp
	
	private String latlong;


	private String lat;



	private String longitute;
	// 
	private Date yeat_build;
	private String target_mk_status;
	private String notes;

	private Vector<SiqLocation> dup_loc = new Vector<SiqLocation>();
	
	public SiqLocation() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getAdd() {
		return add;
	}

	public void setAdd(String add) {
		this.add = add;
	}

	public String getAdd2() {
		return add2;
	}

	public void setAdd2(String add2) {
		this.add2 = add2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public String getLatlong() {
		return latlong;
	}

	public void setLatlong(String latlong) {
		this.latlong = latlong;
	}

	public Date getYeat_build() {
		return yeat_build;
	}

	public String getYeat_buildString() {
		SimpleDateFormat formatter = new SimpleDateFormat("YYYY");
		return formatter.format(new Date(yeat_build.getTime()));
	}
	
	public void setYeat_build(Date yeat_build) {
		this.yeat_build = yeat_build;
	}

	public String getTarget_mk_status() {
		return target_mk_status;
	}

	public void setTarget_mk_status(String target_mk_status) {
		this.target_mk_status = target_mk_status;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Vector<SiqLocation> getDup_loc() {
		return dup_loc;
	}

	public void setDup_loc(Vector<SiqLocation> dup_loc) {
		this.dup_loc = dup_loc;
	}
	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}
	public String getLongitute() {
		return longitute;
	}

	public void setLongitute(String longitute) {
		this.longitute = longitute;
	}
}
