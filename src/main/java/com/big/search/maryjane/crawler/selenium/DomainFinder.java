package com.big.search.maryjane.crawler.selenium;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.gtn.search.dynamodb.WebSite_Index;
import com.gtn.search.ops.SearchWordMaker;
import com.gtn.search.ops.TypeAhead;
import com.gtn.search.sel.SeleniumWeb;
public class DomainFinder {
	public WebDriver driver = null;
	private DomainManager dmMngr = null;
	private HashMap<String, String> isRelevantNo = null;
	private HashMap<String, String> isRelevantYes = null;

	public DomainFinder() {
		// TODO Auto-generated constructor stub
		dmMngr = new DomainManager();
		isRelevantNo = dmMngr.getDomainsIsRelevantNo();
		isRelevantYes = dmMngr.getDomainsIsRelevantYes();
	}

	  public static void main(String[] args) {
		  	String[] statesList = {"arizona", "washington", "new-mexico", "new-jersey", "new-york", "oregon", "pennsylvania"
		  		,"texas","montana","michigan", "massachusetts", "maryland", "main", "illinois", "hawaii", "florida"	
		  	};
		  	
	    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
	    	DomainFinder web = new DomainFinder();
	    	HashMap<String, String> tmpMap = new HashMap<String, String>();
	    	for(int k=0;k<statesList.length;k++){
		    	tmpMap =  web.getURLContent("https://potguide.com/" + statesList[k] +"/marijuana-dispensaries/", tmpMap);
		    	web.driver.quit();
	
		        for (Map.Entry<String,String> entry : tmpMap.entrySet())  {
					String newUrl = entry.getValue();
					System.out.print("NEXT URL STEP 11 :" + newUrl);		        	
		        }
	    	}
	    }
	    
	    public HashMap<String, String> getURLContent(String url, HashMap<String, String> urlVisited) {
	    		//// CHECK CODE FOR ROOT NO RELEVANT RETURN SAME MAP
	    	    if(urlVisited.containsKey(url))return urlVisited;
	    	    
	    		try{
	        		if(!(new URL(url)).getHost().equals("")){
	        			if(this.isRelevantNo.containsKey((new URL(url)).getHost()))return urlVisited;		        	
	        		}
	        	}catch(MalformedURLException exmal){
	        		exmal.printStackTrace();				        	
		        }	    	 	
	    		//////
	    		
	        	System.out.println("Start Get URL : " + url);	        	
				try{
					this.driver = new FirefoxDriver();
			    	this.driver.get(url);  	
			    	urlVisited.put(url, url);		        		    	
			        ExpectedCondition<Boolean> pageLoadCondition = new
			                ExpectedCondition<Boolean>() {
			                    public Boolean apply(WebDriver driver) {
			                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
			                    }
			                };
			        WebDriverWait wait = new WebDriverWait(this.driver, 30);
			        wait.until(pageLoadCondition);
			        Thread.sleep(3000);		        
			        List<WebElement> elementExpand = this.driver.findElements(By.tagName("input"));
			        for (WebElement temp : elementExpand) {
			        	if(temp!=null && temp.getAttribute("type")!=null && temp.getAttribute("type").startsWith("button")){
				        	if(temp!=null && temp.getAttribute("value")!=null && temp.getAttribute("value").startsWith("View ")){
				        		temp.click();
				        	}			        		
			        	}       
			        }
			        
			        List<WebElement> element = this.driver.findElements(By.tagName("a"));		 
			        HashMap<String, String> tmpUrlCach = new HashMap<String, String>();	 
			        HashMap<String, String> tmpUrlCachFull = new HashMap<String, String>();
			        for (WebElement temp : element) {
						System.out.println("NEXT URL STEP 00 : ");
						//temp.
						try{
				        	if(temp!=null && temp.getAttribute("href")!=null && temp.getAttribute("href").startsWith("http")){
					        	try{
									System.out.println("NEXT URL STEP 000 :" + temp.getAttribute("href"));	
					        		String newUrl = temp.getAttribute("href").split("#")[0];
									System.out.println("NEXT URL STEP 0000 :" + newUrl);	
					        		tmpUrlCachFull.put(newUrl, newUrl);
					        		if(!(new URL(newUrl)).getHost().equals("")){
					        			tmpUrlCach.put((new URL(newUrl)).getHost(), (new URL(newUrl)).getHost());			        	
					        		}
					        	}catch(MalformedURLException exmal){
					        		exmal.printStackTrace();				        	
						        }
				        	}
				        } catch (StaleElementReferenceException expected) {
				        	expected.printStackTrace();
				        }
				        
			        }
			        this.driver.quit();
			        for (Map.Entry<String,String> entry : tmpUrlCach.entrySet())  {			        	
				        	String newUrl = entry.getValue();
							System.out.println("NEXT URL STEP 0 :" + newUrl);	
		        			if(!this.isRelevantNo.containsKey(newUrl) && !this.isRelevantYes.containsKey(newUrl)){
					        	this.dmMngr.putDomain(newUrl, "NEW");		        				
		        			}
			        }
			        for (Map.Entry<String,String> entry : tmpUrlCachFull.entrySet())  {
							String newUrl = entry.getValue();
							System.out.println("NEXT URL STEP 1 :" + newUrl);	
								if(!newUrl.endsWith("jpeg") && 
										!newUrl.endsWith("gif")	&& 
										!newUrl.endsWith("jpg")	&& 
										!newUrl.endsWith("png")	&& 
										!newUrl.contains("admin") &&	
										!newUrl.contains("sign")	
										){
									System.out.print("NEXT URL STEP 2 :" + newUrl);	
									//urlVisited = this.getURLContent(newUrl, urlVisited);
								}
					}        
				}catch(Exception ex){
					ex.printStackTrace();
				}
	        return urlVisited;
	    }
}
