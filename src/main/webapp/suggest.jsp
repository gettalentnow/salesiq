<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="false" import="com.gtn.search.WebSiteSuggestManager" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<%
java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy");
request.setAttribute("year", sdf.format(new java.util.Date()));
request.setAttribute("tomcatUrl", "https://tomcat.apache.org/");
request.setAttribute("tomcatDocUrl", "/docs/");
request.setAttribute("tomcatExamplesUrl", "/examples/");
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="AskBookman.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Ask Bookman</title>

        <link rel="stylesheet" href="css/bootstrap.min.css" >
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-3.4.1.min.js"></script>
    </head>

    <body>
      <div class="container">
        <div class="jumbotron mt-3">
          <h1>Suggest Website</h1>
          <BR><%=WebSiteSuggestManager.logDynamoDb(request.getParameter("suggest"), "X-Forwarded-For:" + request.getHeader("X-Forwarded-For"))%>
          <BR>  Site Has Been Recorded by AskBookman.com
        </div>
      </div>
      <!-- Footer -->
      <footer class="page-footer font-small blue">

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3">© 2019 Copyright:
          <a href="https://askbookman.com"> ASKBOOKMAN.COM</a>
        </div>
        <!-- Copyright -->

      </footer>
      <!-- Footer -->


    </body>
      <!-- The form -->
    </body>

</html>
