package com.big.search.maryjane.crawler.selenium;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.DomainManager;
import com.gtn.search.dynamodb.WebSite_Index;

public class WebsiteCrawler {
	public WebDriver driver = null;
	private DomainManager dmMngr = null;
	private HashMap<String, String> isRelevantNo = null;
	private HashMap<String, String> isRelevantYes = null;
	private HashMap<String, String> urlVisited = null; 
	
	public WebsiteCrawler() {
		// TODO Auto-generated constructor stub
		this.dmMngr = new DomainManager();
		this.driver = new FirefoxDriver();
		this.urlVisited = new HashMap<String, String>();
		this.isRelevantNo = dmMngr.getDomainsIsRelevantNo();
		this.isRelevantYes = dmMngr.getDomainsIsRelevantYes();
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
		WebsiteCrawler web = new WebsiteCrawler();
		
        for (Map.Entry<String,String> entry : web.isRelevantYes.entrySet())  {		
        	web.getURLContent("http://" + entry.getValue());
        }
	}

    public void getURLContent(String url) {
	    if(this.urlVisited.containsKey(url))return ;
	    this.urlVisited.put(url, url);		    
		try{
    		if(!(new URL(url)).getHost().equals("")){
    			if(this.isRelevantNo.containsKey((new URL(url)).getHost()))return ;	
    			if(!this.isRelevantYes.containsKey((new URL(url)).getHost()))return ;	        	
    		}
    	}catch(MalformedURLException exmal){    		
    		exmal.printStackTrace();		
    		return ;
        }	    	 	
    	System.out.println(url);	        	
		try{
	    	this.driver.get(url);  		        		    	
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };
	                
	        WebDriverWait wait = new WebDriverWait(this.driver, 30);
	        wait.until(pageLoadCondition);
	        Thread.sleep(10000);		        
	        
	        
	        
	        List<WebElement> element = this.driver.findElements(By.tagName("a"));		 
	        HashMap<String, String> tmpUrlCachFull = new HashMap<String, String>();
	        for (WebElement temp : element) {
				try{
		        	if(temp!=null && temp.getAttribute("href")!=null && temp.getAttribute("href").startsWith("http")){
			        		String newUrl = temp.getAttribute("href").split("#")[0];
			        		tmpUrlCachFull.put(newUrl, newUrl);
		        	}
		        } catch (StaleElementReferenceException expected) {
		        	expected.printStackTrace();
		        }		        
	        }
			WebSite_Index tmpDBInd  = new WebSite_Index();
			tmpDBInd.logDynamoDb(url,this.driver.findElement(By.tagName("body")).getText().toLowerCase());
			for (Map.Entry<String,String> entry : tmpUrlCachFull.entrySet())  {
					String newUrl = entry.getValue();
						if(!newUrl.endsWith("jpeg") && 
								!newUrl.endsWith("gif")	&& 
								!newUrl.endsWith("jpg")	&& 
								!newUrl.endsWith("png")	&& 
								!newUrl.contains("admin") &&	
								!newUrl.contains("sign")	
								){
							this.getURLContent(newUrl);
						}
			}        
		}catch(Exception ex){
			ex.printStackTrace();
		}    	
    }
}
