package com.gtn.search.ops;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;

public class TypeAhead {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
		ScanRequest scanRequest = new ScanRequest()
		    .withTableName("website_search");
		ScanResult result = client.scan(scanRequest);
		HashMap<String, String> newWordList = new HashMap<String, String>();
		for (Map<String, AttributeValue> item : result.getItems()){
		    System.out.println(item.get("search_word").getS());
		    String tmpWord = item.get("search_word").getS().toLowerCase();		
		    String newTmpWord = TypeAhead.camelCase(tmpWord);
			newWordList.put(newTmpWord, newTmpWord);
		}
        // using for-each loop for iteration over Map.entrySet() 
		String tmpJson = "";
        for (Map.Entry<String,String> entry : newWordList.entrySet())  {
            System.out.println("Key = " + entry.getKey() + 
                             ", Value = " + entry.getValue()); 
            tmpJson = tmpJson + "\""+ entry.getValue() + "\""+ " ";
        }    
        tmpJson = tmpJson.trim();
        tmpJson = "[" + tmpJson.replace("\" \"", "\",\"") + "]";
        System.out.println(tmpJson);
        try{
	        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/armenmerikyan/Desktop/gettalentnow/get-talent-now/src/main/webapp/data/countries.json"));
	        writer.write(tmpJson);
	        writer.close();		
        }catch(Exception Ex){
        	Ex.printStackTrace();
        }
		
	}
	public static String camelCase(String tmpWord) {
	    String[] tmpWordArray = tmpWord.split(" ");
	    for(int j=0;j<tmpWordArray.length;j++){
	    	tmpWordArray[j] = tmpWordArray[j].replaceFirst(tmpWordArray[j].substring(0, 1), tmpWordArray[j].substring(0, 1).toUpperCase());
	    }
	    String newTmpWord = "";
	    for(int j=0;j<tmpWordArray.length;j++){
	    	newTmpWord = newTmpWord + tmpWordArray[j] + " ";
	    }
	    newTmpWord = newTmpWord.trim();		
		return newTmpWord;
	}
}
