package com.gtn.search;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class ABDomainExists {

	public static void main(String[] args) {
		HashMap<String, WebSiteItem> siteMap = new HashMap<String, WebSiteItem>();
		ABDomainExists tmpExists = new ABDomainExists();
		siteMap = tmpExists.getTitles(siteMap, "Operation Paper Clip");
        for (Map.Entry<String,WebSiteItem> entry : siteMap.entrySet())  {
        	System.out.println(entry.getKey()); 
        }
	}
	public HashMap<String, WebSiteItem> getTitles(HashMap<String, WebSiteItem> siteList, String urlItem) {
		urlItem = urlItem.trim();
		String[] words = urlItem.split(" ");
		String wordUrl = "";
        for(int k =0;k<words.length;k++){     
        	wordUrl = wordUrl + words[k];
        }
    	siteList = getTitle(siteList, wordUrl, urlItem);
    	wordUrl = "";
        for(int k =words.length-1;k>=0;k--){     
        	wordUrl = wordUrl + words[k];
        }
    	siteList = getTitle(siteList, wordUrl, urlItem);
		return siteList;
	}
	public HashMap<String, WebSiteItem> getTitle(HashMap<String, WebSiteItem> siteList, String urlItem, String originalKey) {
		// TODO Auto-generated method stub
        String searchResult = "";
        String[] rootDomArray = {".com", ".edu", ".gov"};
        for(int k =0;k<rootDomArray.length;k++){        	
		try{
			
			URL url = new URL("http://" + urlItem + rootDomArray[k]);
	        HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
	        httpConn.setConnectTimeout(2000);
	        httpConn.setReadTimeout(5000);			
	        int responseCode = httpConn.getResponseCode();
	        if (responseCode == HttpURLConnection.HTTP_OK) {
	            BufferedReader in = new BufferedReader(new InputStreamReader(
	            		httpConn.getInputStream(), "UTF-8"));
	            String inputLine;
	            StringBuilder a = new StringBuilder();
	            while ((inputLine = in.readLine()) != null)
	                a.append(inputLine);
	            in.close();

	            searchResult = a.toString();        	
	        }
	        
	        System.out.println("https://" + urlItem + rootDomArray[k]);

	        System.out.println(searchResult);
	        if(!searchResult.trim().equals("")){
		        searchResult = searchResult.substring(searchResult.indexOf("<title"),searchResult.length());
		        searchResult = searchResult.substring(searchResult.indexOf(">") +1,searchResult.length());
		        searchResult = searchResult.substring(0,searchResult.indexOf("</title>"));
	        }
	        if(searchResult==null || searchResult.equals("") || searchResult.toLowerCase().equals("null")){
	        	searchResult = originalKey;
	        }
	        WebSiteItem webItem = new WebSiteItem();
	        webItem.setDesc(searchResult);
	        webItem.setName(searchResult);
	        webItem.setLink("http://" + urlItem + rootDomArray[k]);
	        siteList.put(webItem.getLink(), webItem);
	        //System.out.println(searchResult);
		} catch(Exception ex){
			System.out.println(" HELL NO " + ex.getMessage());
		}  
        }

		return siteList;
	}

}
