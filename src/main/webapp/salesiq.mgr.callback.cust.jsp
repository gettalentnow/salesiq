<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="com.salesiq.mysql.login.CallbackManager,com.salesiq.dom.SiqCustomer,com.salesiq.mysql.login.CustomerManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>
        <script src="js/jquery.js"></script>
        <script src="js/jquery-ui.js"></script>  

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>

            <script src="js/jquery-ui.js"></script>
            <link rel="stylesheet" href="css/jquery-ui.css">

              <script>
              $( function() {
                $( "#cbdate" ).datepicker();
              } );
              </script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">


              <jsp:include page='navbar_top_siq.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">


                                          <div id="content" style="text-align: left">
              <h1>Add Callback Form</h1>
              <a href="salesiq.menu.jsp" >Menu</a><BR>
              <%
              if(request.getParameter("cbname")!=null){
                  com.salesiq.mysql.login.CallbackManager tmpMgr = new com.salesiq.mysql.login.CallbackManager();
                  String tmpTime = request.getParameter("cbHH") + ":" + request.getParameter("cbMM") + ":00 " + request.getParameter("cbAMPM");
                  String tmpout = tmpMgr.addCallback(request.getParameter("cbname"), request.getParameter("cbdetail"), request.getParameter("cbdate"), tmpTime, request.getParameter("cust_id"), "", (String)session.getAttribute("uid"));
                  if(!tmpout.startsWith("Error")){
                    %>Callback Added  <BR><%
                  }else{
                    %><%=tmpout%><%
                  }
              }
              %>
              <%
                CustomerManager tmpMgr = new CustomerManager();
                SiqCustomer tmpCust = tmpMgr.getCustomer((String)session.getAttribute("uid"),request.getParameter("cust_id"));
              %>
              <a href="salesiq.edit.cust.jsp?cust_id=<%=request.getParameter("cust_id")%>" ><%=tmpCust.getName()%></a><BR>

            <form class="example" action="salesiq.mgr.callback.cust.jsp" >
                  <input type="hidden" id="cust_id" name="cust_id" value="<%=request.getParameter("cust_id")%>">
                  <div class="form-group">
                    <label for="exampleInputPhone">Name</label>
                    <input type="text" class="form-control" id="cbname" name="cbname" placeholder="Name" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Detail</label>
                    <input type="text" class="form-control" id="cbdetail" name="cbdetail" placeholder="Description" >
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Callback Date and Time</label>
                    <input type="text" class="form-control" name="cbdate" id="cbdate" placeholder="08/22/2020">
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-2">
                        <input type="text" class="form-control" name="cbHH" placeholder="08">
                      </div>
                      <div class="col-sm-2">
                        <input type="text" class="form-control" name="cbMM" placeholder="30">
                      </div>
                      <div class="col-sm-2">
                        <select id="custStatus" name="cbAMPM" class="form-control">
                            <option value="AM" >AM</option>
                            <option value="PM" >PM</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </form>

                                </div>
            </div>
    </div>




    </body>
      <!-- The form -->

</html>
