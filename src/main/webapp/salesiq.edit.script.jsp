<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="java.util.Vector,com.salesiq.dom.SiqCallback,com.salesiq.mysql.login.ScriptManager,com.salesiq.dom.SiqScript,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">


              <jsp:include page='navbar_top_siq.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">


                                          <div id="content" style="text-align: left">
              <h1>Edit Script Form</h1>
              <a href="salesiq.menu.jsp" >Menu</a><BR>
              Note: You can use tags to merge customer data with scripts <code> &lt;NAME&gt;  &lt;PHONE&gt; </code> <BR>
              Example: Hi, This is SalesIQ, Is this &lt;NAME&gt; company, we would like to offer &lt;NAME&gt; an awesome deal
              <BR><BR>

              <%
              if(request.getParameter("name")!=null){
                  com.salesiq.mysql.login.ScriptManager tmpMgr = new com.salesiq.mysql.login.ScriptManager();
                  String tmpout = tmpMgr.editScript(request.getParameter("sc_id"), request.getParameter("name"),request.getParameter("script"), request.getParameter("status"), (String)session.getAttribute("uid"));
                  if(!tmpout.startsWith("Error")){
                    %>Script Changed  <%
                  }else{
                    %><%=tmpout%><%
                  }
              }
              %>
              <%
                ScriptManager tmpMgr = new ScriptManager();
                SiqScript tmpCust = tmpMgr.getScript((String)session.getAttribute("uid"),request.getParameter("sc_id"));
              %>
            <form class="example" action="salesiq.edit.script.jsp" >
                  <input type="hidden" id="sc_id" name="sc_id" value="<%=request.getParameter("sc_id")%>">
                  <div class="form-group">
                    <label for="exampleInputPhone">Name</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<%=tmpCust.getName()%>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Status</label>
                    <select id="status" name="status" class="form-control">
                        <option value="None" <%if(tmpCust.getStatus().equals("None")){%>selected<%}%> >None</option>
                        <option value="Not Finished" <%if(tmpCust.getStatus().equals("Not Finished")){%>selected<%}%> >Not Finished</option>
                        <option value="Not Active" <%if(tmpCust.getStatus().equals("Not Active")){%>selected<%}%> >Not Active</option>
                        <option value="Active" <%if(tmpCust.getStatus().equals("Active")){%>selected<%}%> >Active</option>
                        <option value="Not Working" <%if(tmpCust.getStatus().equals("Not Working")){%>selected<%}%> >Not Working</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Script</label>
                    <textarea class="form-control" id="script" name="script" rows="4"><%=tmpCust.getScript()%></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </form>

                                </div>

            </div>
    </div>




    </body>
      <!-- The form -->

</html>
