package com.salesiq.mysql.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;
import java.util.Vector;

import com.salesiq.dom.SiqCallback;
import com.salesiq.dom.SiqCustomer;
import com.salesiq.dom.SiqLocation;

public class LocationManager {

	public LocationManager() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LocationManager tmpMgr = new LocationManager();
		// tmpMgr.addLocation("test", "test add",  "test add2",  "test state",  "test city",  "test zip",  "test userid",  "test notest",  "status",  "1977", "1 2");
		// tmpMgr.editLocation("7d205ed7-7434-488a-a9ef-441b3442e375", "test update", "test add",  "test add2",  "test state",  "test city",  "test zip",  "test userid",  "test notest",  "status",  "1977", "1 2");

		Vector<SiqLocation> tmplist = tmpMgr.getLocations("a9914eb5-07ac-40fc-8aab-19a5600646b0", false);
		for(int j=0;j<tmplist.size();j++){
			SiqLocation tmpcb = tmplist.elementAt(j);
			System.out.println("HELL O " + tmpcb.getSiq_id());
			System.out.println("HELL O " + tmpcb.getName());
			System.out.println("HELL O " + tmpcb.getTs());
			System.out.println("HELL O " + tmpcb.getTs_update());
			System.out.println("HELL O " + tmpcb.getLatlong());
		}
	}
	public SiqLocation getLocation(String siq_user_id, String siq_loc_id ){
		SiqLocation tmpCust = new SiqLocation();
	    try
	    {
	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query_fields = "loc_name, loc_city, loc_zip, loc_add, loc_add2, loc_state, siq_loc_id, siq_user_id, ts, ts_update, loc_notes, loc_target_status, loc_built_date, ST_X(loc_lat_long) as loc_lat, ST_Y(loc_lat_long) as loc_lng";
	      String query = "select " + query_fields + " from siq_locations where siq_locations.siq_user_id = ? and siq_locations.siq_loc_id = ? order by ts desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      preparedStmt.setString (2, siq_loc_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  tmpCust.setName(resultSet.getString("loc_name"));
	    	  tmpCust.setCity(resultSet.getString("loc_city"));
	    	  tmpCust.setZip(resultSet.getString("loc_zip"));
	    	  tmpCust.setAdd(resultSet.getString("loc_add"));
	    	  tmpCust.setAdd2(resultSet.getString("loc_add2"));
	    	  tmpCust.setState(resultSet.getString("loc_state"));
	    	  tmpCust.setSiq_id(resultSet.getString("siq_loc_id"));
	    	  tmpCust.setUser_id(resultSet.getString("siq_user_id"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  tmpCust.setTs_update(resultSet.getTimestamp("ts_update"));
	    	  tmpCust.setNotes(resultSet.getString("loc_notes"));
	    	  tmpCust.setTarget_mk_status(resultSet.getString("loc_target_status"));
	    	  tmpCust.setYeat_build(new Date(resultSet.getTimestamp("loc_built_date").getTime()));
	    	  tmpCust.setLatlong(resultSet.getString("loc_lat") + " " + resultSet.getString("loc_lng"));
			  tmpCust.setLat(resultSet.getString("loc_lat"));
			  tmpCust.setLongitute(resultSet.getString("loc_lng"));
			  resultSet.close();
	    	  preparedStmt.close();
	    	  conn.close();
	    	  return tmpCust;
	      }

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return tmpCust;
	}
	public Vector<SiqLocation> getLocations(String siq_user_id, boolean sortByTs_update){
		Vector<SiqLocation> custList = new Vector<SiqLocation>();
	    try
	    {
	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query_fields = "loc_name, loc_city, loc_zip, loc_add, loc_add2, loc_state, siq_loc_id, siq_user_id, ts, ts_update, loc_notes, loc_target_status, loc_built_date, ST_X(loc_lat_long) as loc_lat, ST_Y(loc_lat_long) as loc_lng";
	      String query = "select " + query_fields + " from siq_locations where siq_locations.siq_user_id = ? order by ts desc";
	      if(sortByTs_update)query = "select " + query_fields + " from siq_locations where siq_locations.siq_user_id = ? order by ts_update desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqLocation tmpCust = new SiqLocation();
	    	  tmpCust.setName(resultSet.getString("loc_name"));
	    	  tmpCust.setCity(resultSet.getString("loc_city"));
	    	  tmpCust.setZip(resultSet.getString("loc_zip"));
	    	  tmpCust.setAdd(resultSet.getString("loc_add"));
	    	  tmpCust.setAdd2(resultSet.getString("loc_add2"));
	    	  tmpCust.setState(resultSet.getString("loc_state"));
	    	  tmpCust.setSiq_id(resultSet.getString("siq_loc_id"));
	    	  tmpCust.setUser_id(resultSet.getString("siq_user_id"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  tmpCust.setTs_update(resultSet.getTimestamp("ts_update"));
	    	  tmpCust.setNotes(resultSet.getString("loc_notes"));
	    	  tmpCust.setTarget_mk_status(resultSet.getString("loc_target_status"));
	    	  tmpCust.setYeat_build(new Date(resultSet.getTimestamp("loc_built_date").getTime()));
	    	  tmpCust.setLatlong(resultSet.getString("loc_lat") + " " + resultSet.getString("loc_lng"));
			  tmpCust.setLat(resultSet.getString("loc_lat"));
			  tmpCust.setLongitute(resultSet.getString("loc_lng"));
	    	  custList.add(tmpCust);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return custList;
	}
	public String addLocation(String locName, String locAdd, String locAdd2, String locState, String locCity, String locZip, String siq_user_id, String locNotes, String locISTargetMK, String year_built, String lat_long){
		try{
			SiqLocation tmpLoc = new SiqLocation();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		            Date ybdate = formatter.parse("01/01/" + year_built + " 00:01:01 AM");
		            System.out.println(ybdate);
		            System.out.println(formatter.format(ybdate));
		            tmpLoc.setYeat_build(ybdate);
			tmpLoc.setName(locName);
			tmpLoc.setUser_id(siq_user_id);
			tmpLoc.setAdd(locAdd);
			tmpLoc.setAdd2(locAdd2);
			tmpLoc.setState(locState);
			tmpLoc.setCity(locCity);
			tmpLoc.setZip(locZip);
			tmpLoc.setTarget_mk_status(locISTargetMK);
			tmpLoc.setNotes(locNotes);
			tmpLoc.setLatlong(lat_long);

			return insertLocation(tmpLoc);

		}catch (Exception e)
	    {
	    	return "Error: System Error ";
	    }
	}
	public String insertLocation(SiqLocation newLoc){
	    try
	    {

	      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = " insert into siq_locations (siq_loc_id, siq_user_id, siq_bus_id, loc_name, loc_add, loc_add2, loc_state, loc_city, loc_zip, loc_built_date, loc_notes, loc_target_status, loc_lat_long, ts, ts_update)"
	        + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?, ?, ST_GeomFromText(?), ?, ?)";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, UUID.randomUUID().toString());
	      preparedStmt.setString (2, newLoc.getUser_id());
	      preparedStmt.setString (3, newLoc.getBus_id());
	      preparedStmt.setString (4, newLoc.getName());
	      preparedStmt.setString (5, newLoc.getAdd());
	      preparedStmt.setString (6, newLoc.getAdd2());
	      preparedStmt.setString (7, newLoc.getState());
	      preparedStmt.setString (8, newLoc.getCity());
	      preparedStmt.setString (9, newLoc.getZip());
	      preparedStmt.setTimestamp(10, new java.sql.Timestamp(newLoc.getYeat_build().getTime()));
	      preparedStmt.setString (11, newLoc.getNotes());
	      preparedStmt.setString (12, newLoc.getTarget_mk_status());
	      preparedStmt.setString (13, "POINT(" + newLoc.getLatlong() + ")");
	      preparedStmt.setTimestamp(14, currentTimestamp);
	      preparedStmt.setTimestamp(15, currentTimestamp);
	      preparedStmt.execute();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error " + e.getMessage();
	      return "Error: System Error ";
	    }
		return "Customer Added";
	}

	public String updateLocation(SiqLocation newLoc){
		try{
		      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
		      MySQLConstants tmpCon = new MySQLConstants();
		      // create a mysql database connection
		      //String myDriver = "org.gjt.mm.mysql.Driver";
		      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
		      //Class.forName(myDriver);
	          Class.forName("com.mysql.jdbc.Driver").newInstance();
		      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
		      String query = "update siq_locations set loc_name = ?, loc_add = ?, loc_add2 = ?, loc_state = ?, loc_city = ?, loc_zip = ?, loc_built_date = ?, loc_notes = ?, loc_target_status = ?,  loc_lat_long = ST_GeomFromText(?),  ts_update = ?"
		        + " where siq_locations.siq_loc_id = ? and siq_locations.siq_user_id = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, newLoc.getName());
		      preparedStmt.setString (2, newLoc.getAdd());
		      preparedStmt.setString (3, newLoc.getAdd2());
		      preparedStmt.setString (4, newLoc.getState());
		      preparedStmt.setString (5, newLoc.getCity());
		      preparedStmt.setString (6, newLoc.getZip());
		      preparedStmt.setTimestamp(7, new java.sql.Timestamp(newLoc.getYeat_build().getTime()));

		      preparedStmt.setString (8, newLoc.getNotes());
		      preparedStmt.setString (9, newLoc.getTarget_mk_status());
		      preparedStmt.setString (10, "POINT(" + newLoc.getLatlong() + ")");

		      preparedStmt.setTimestamp(11, currentTimestamp);
		      preparedStmt.setString (12, newLoc.getSiq_id());
		      preparedStmt.setString (13, newLoc.getUser_id());
		      preparedStmt.execute();
		      preparedStmt.close();
		      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      return "Error: System Error";
	    }
		return "Customer Added";
	}
	public String editLocation(String siq_loc_id, String locName, String locAdd, String locAdd2, String locState, String locCity, String locZip, String siq_user_id, String locNotes, String locISTargetMK, String year_built, String lat_long){
		try{
			SiqLocation tmpLoc = new SiqLocation();
			SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		            Date ybdate = formatter.parse("01/01/" + year_built + " 00:01:01 AM");
		            System.out.println(ybdate);
		            System.out.println(formatter.format(ybdate));
		            tmpLoc.setYeat_build(ybdate);
			tmpLoc.setName(locName);
			tmpLoc.setUser_id(siq_user_id);
			tmpLoc.setSiq_id(siq_loc_id);
			tmpLoc.setAdd(locAdd);
			tmpLoc.setAdd2(locAdd2);
			tmpLoc.setState(locState);
			tmpLoc.setCity(locCity);
			tmpLoc.setZip(locZip);
			tmpLoc.setTarget_mk_status(locISTargetMK);
			tmpLoc.setNotes(locNotes);
			tmpLoc.setLatlong(lat_long);

			return updateLocation(tmpLoc);

		}catch (Exception e)
	    {
	    	return "Error: System Error ";
	    }
	}

	public String removeLocation(String user_id, String cust_id){
		try{
		      MySQLConstants tmpCon = new MySQLConstants();
		      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	          Class.forName("com.mysql.jdbc.Driver").newInstance();
		      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
		      String query = "delete from siq_locations where siq_locations.siq_loc_id = ? and siq_locations.siq_user_id = ?";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, cust_id);
		      preparedStmt.setString (2, user_id);
		      preparedStmt.execute();
		      preparedStmt.close();
		      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      return "Error: System Error";
	    }
		return "Customer Removed";
	}

}
