package com.salesiq.mysql.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.UUID;

public class UserManger {

	public UserManger() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		UserManger tmp = new UserManger();
		String tmpStr = tmp.signUp(null, null);
		System.out.println(tmpStr);
		tmpStr = tmp.signUp("", "");
		System.out.println(tmpStr);
		tmpStr = tmp.signUp("3109036012", "test123");
		System.out.println(tmpStr);
		tmpStr = tmp.signIn("3109036012", "test123");
		System.out.println(tmpStr);
		tmpStr = tmp.signIn("3109036012", "test12f");
		System.out.println(tmpStr);
	}
	public String signIn(String phone, String password){
		if(phone ==null || phone.equals("") ||
			password ==null || password.equals("")
				){
			return "Error: User phone and password can't be blank.";
		}
		return this.getUserID(phone, password);
	}
	public String getUserID(String phone, String password){
	    try
	    {
	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select siq_user_id from siq_users where siq_users.siq_user_phone = ? and siq_users.siq_user_password = ?";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, phone);
	      preparedStmt.setString (2, password);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	      	String siq_user_id = resultSet.getString("siq_user_id");
	      	resultSet.close();
	      	preparedStmt.close();
	      	conn.close();
	        return siq_user_id;
	      }

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      return "Error: System Error";  //+ e.getMessage();
	    }
		return "Error: User phone and password not found";
	}
	public String signUp(String phone, String password){
		if(phone ==null || phone.equals("") ||
			password ==null || password.equals("")
				){
			return "Error: User phone and password can't be blank.";
		}
		return this.insertUser(phone, password);
	}
	public String insertUser(String phone, String password){
	    try
	    {
		  java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	      MySQLConstants tmpCon = new MySQLConstants();
	      // create a mysql database connection
	      //String myDriver = "org.gjt.mm.mysql.Driver";
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	      //Class.forName(myDriver);
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select count(*) as total from siq_users where siq_users.siq_user_phone = ?";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, phone);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      int count = 0;
	      while (resultSet.next()) {
	        count = resultSet.getInt("total");
	      }
	      if(count>0)return "Error: User phone number already registered please sign-in";

	      query = " insert into siq_users (siq_user_id, siq_user_phone, siq_user_password, ts)"
	        + " values (?, ?, ?, ?)";
	      preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, UUID.randomUUID().toString());
	      preparedStmt.setString (2, phone);
	      preparedStmt.setString   (3, password);
	      preparedStmt.setTimestamp(4, currentTimestamp);
	      preparedStmt.execute();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      return "Error: System Error";
	    }
		return "Registration finished";
	}
}
