package com.salesiq.ops;

import com.salesiq.mysql.login.MySQLConstants;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

// Armen merikyan Salescal.com
public class SendSMS {

    public SendSMS() {
    }
    public static void main(String[] args) {
        SendSMS newSMS = new SendSMS();
        newSMS.sendSMS("+13109036014" , "test");
    }
    public void sendSMS(String phoneTo, String smsMsg) {
        String ACCOUNT_SID =
                "ACaf4a172bc599e47893bcd1fd94dde1f0";
        MySQLConstants tmpConst = new MySQLConstants();

        String AUTH_TOKEN =        tmpConst.getTwillio_password();
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);

        Message message = Message
                .creator(new PhoneNumber(phoneTo), // to
                        new PhoneNumber(tmpConst.getTwillio_fromphone()), // from
                        smsMsg)
                .create();

        System.out.println(message.getSid());

    }
}
