package com.gtn.search.ops;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

import com.big.search.maryjane.persistence.dynamodb.CommentManager;
import com.big.search.maryjane.persistence.dynamodb.DataFirstNames;

public class LoadFirstNamesFromFile {

	public LoadFirstNamesFromFile() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try{	
			FileInputStream fstream = new FileInputStream("/Users/armenmerikyan/Desktop/gettalentnow/get-talent-now/src/main/webapp/data/first_names.csv");
			BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
			while ((strLine = br.readLine()) != null)   {
			  System.out.println (strLine);
			  new DataFirstNames().putName(strLine);
			}
			fstream.close();		
		} catch (Exception e) {
		    e.printStackTrace();
		}		

	}

}
