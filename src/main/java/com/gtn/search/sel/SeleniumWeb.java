package com.gtn.search.sel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.gtn.search.dynamodb.WebSite_Index;
import com.gtn.search.ops.SearchWordMaker;
import com.gtn.search.ops.TypeAhead;

public class SeleniumWeb  {
	public WebDriver driver = null;
	public String[]  rootDomain = {
			"https://speedweed.com"
	};    	
	public String[]  rootDomainBad = {
			"google",
			"twitter",
			"facebook",
			"youtube",
			"instagram",
			"sitemaps",
			"shopify",
			"weedmaps","dutchie",
			"leafly",
			"wpengine",
			"linkedin",
			"pinterest",	
			"copyrigh",
			"loc.gov", "twitch",
			"getnetwise","feldentertainment","mapbox", "cartoon", "airbnb"
	};	
	public SeleniumWeb() {
		super();
		// TODO Auto-generated constructor stub
//        FirefoxBinary firefoxBinary = new FirefoxBinary();
 //       firefoxBinary.addCommandLineOptions("--headless");
        FirefoxOptions firefoxOptions = new FirefoxOptions();
  //      firefoxOptions.setBinary(firefoxBinary);
        firefoxOptions.setHeadless(true);
       // firefoxOptions.set
	
    	
    	
    	//this.driver = new FirefoxDriver(firefoxOptions);
	}


    public static void main(String[] args) {
    	System.setProperty("webdriver.gecko.driver", "/Users/armenmerikyan/Downloads/geckodriver");
    	SeleniumWeb web = new SeleniumWeb();


    	HashMap<String, String> tmpMap = new HashMap<String, String>();
    	WebSite_Index cmdDynamoDB = new WebSite_Index();
    	tmpMap = cmdDynamoDB.getAllIndexNoLimit(tmpMap);
    	tmpMap =  web.getURLContent(web.rootDomain[0], tmpMap);
    	web.driver.quit();
    }
    
    public HashMap<String, String> getURLContent(String url, HashMap<String, String> urlVisited) {
        	System.out.println("Start Get URL : " + url);
    		for(int j=0;j<rootDomain.length;j++){
    			if(!url.startsWith(rootDomain[j])){
    	        	System.out.println("Get URL NOT ROOT DOMAIN : " + url);
    	        	System.out.println("Get URL NOT ROOT DOMAIN : " + rootDomain[j]);
    				return urlVisited;
    			}
    		}
    		for(int j=0;j<rootDomainBad.length;j++){
    			if(url.contains(rootDomainBad[j])){
    	        	System.out.println("Get URL BAD DOMAIN : " + url);
    	        	System.out.println("Get URL BAD DOMAIN : " + rootDomainBad[j]);
    				return urlVisited;
    			}
    		}    		
			try{
				this.driver = new FirefoxDriver();
		    	this.driver.get(url);  	
		    	urlVisited.put(url, url);		        		    	
		        ExpectedCondition<Boolean> pageLoadCondition = new
		                ExpectedCondition<Boolean>() {
		                    public Boolean apply(WebDriver driver) {
		                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
		                    }
		                };
		        WebDriverWait wait = new WebDriverWait(this.driver, 30);
		        wait.until(pageLoadCondition);
		        Thread.sleep(6000);		        
		    	//WebElement sign = this.driver.findElement(By.linkText("yes"));
		    	//sign.click();		  
		        List<WebElement> elementAll = this.driver.findElements(By.tagName("body"));		
		        StringBuffer keywordsBfr = new StringBuffer();		        
		        for (WebElement temp : elementAll) {
					//System.out.println(temp.getText());		
		        	keywordsBfr.append(temp.getText().toLowerCase() + " ");
		        }
		        System.out.println("Get URL SAVING: " + url);
				WebSite_Index tmpDBInd  = new WebSite_Index();
				tmpDBInd.logDynamoDb(url, keywordsBfr.toString());
				
		        List<WebElement> element = this.driver.findElements(By.tagName("a"));		 
		        HashMap<String, String> tmpUrlCach = new HashMap<String, String>();

		        for (WebElement temp : element) {
		        	if(temp.getAttribute("href")!=null && temp.getAttribute("href").startsWith("http")){
			        	String newUrl = temp.getAttribute("href").split("#")[0];
			        	tmpUrlCach.put(newUrl, newUrl);
		        	}
		        }
		        this.driver.quit();
		        for (Map.Entry<String,String> entry : tmpUrlCach.entrySet())  {
					if(!urlVisited.containsKey(entry.getValue())){	
						String newUrl = entry.getValue();
						System.out.print("NEXT URL STEP 1 :" + newUrl);	
						//if(!urlVisited.containsKey(newUrl)){
							//System.out.println(temp.getText());	
							///////ADD URL AND KEYWORDS TO DYNAMODB 
							/*
							StringBuffer wordIndexToSave = new StringBuffer();
							HashMap<String, String> tmpMap = new HashMap<String, String>();
 							SearchWordMaker tmpWordMaker = new SearchWordMaker();
 							tmpMap = tmpWordMaker.getWordsFromString(tmpMap, keywordsBfr.toString().replace(System.lineSeparator(), " "));
 					        for (Map.Entry<String,String> entry : tmpMap.entrySet())  {
 					        	wordIndexToSave.append(TypeAhead.camelCase(entry.getKey()) + " "); 
 					        }
 					        */
							
							//SeleniumWeb web = new SeleniumWeb();
							if(!newUrl.endsWith("jpeg") && 
									!newUrl.endsWith("gif")	&& 
									!newUrl.contains("admin") &&	
									!newUrl.contains("sign")	
									){
								System.out.print("NEXT URL STEP 3 :" + newUrl);	
								urlVisited = this.getURLContent(newUrl, urlVisited);
							}
							//web.driver.quit();
						//}
					}
				}        
		        
		        //System.out.println("Page title is: " + driver.getTitle());
		        //System.out.println("Page title is: " + driver.getTitle());
			}catch(Exception ex){
				ex.printStackTrace();
			}
        return urlVisited;
    }
}