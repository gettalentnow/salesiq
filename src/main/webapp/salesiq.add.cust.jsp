<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="com.salesiq.mysql.login.CustomerManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">


              <jsp:include page='navbar_top_siq.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">


                                          <div id="content" style="text-align: left">
              <h1>Add Customer Form</h1>
              <a href="salesiq.menu.jsp" >Menu</a><BR>

                            <%
                            if(request.getParameter("phone")!=null){
                                com.salesiq.mysql.login.CustomerManager tmpMgr = new com.salesiq.mysql.login.CustomerManager();
                                String tmpout = tmpMgr.addCustomer(request.getParameter("busname"),request.getParameter("phone"),request.getParameter("custStatus"), request.getParameter("cust_desc"), (String)session.getAttribute("uid"));
                                if(!tmpout.startsWith("Error")){
                                  %>Customer Added  <%
                                }else{
                                  %><%=tmpout%><%
                                }
                            }
                            %>

            <form class="example" action="salesiq.add.cust.jsp" >
                  <div class="form-group">
                    <label for="exampleInputPhone">Name</label>
                    <input type="text" class="form-control" id="busname" name="busname" placeholder="Busness Name">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Phone</label>
                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Phone Number">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Status</label>
                    <select id="custStatus" name="custStatus" class="form-control">
                        <option value="None" >None</option>
                        <option value="Hot" >Hot</option>
                        <option value="Cold" >Cold</option>
                        <option value="Warm" >Warm</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPhone">Notes</label>
                    <textarea class="form-control" id="cust_desc" name="cust_desc" rows="4"></textarea>
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
            </form>

                                </div>
            </div>
    </div>




    </body>
      <!-- The form -->

</html>
