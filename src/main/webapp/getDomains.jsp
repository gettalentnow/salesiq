<%@ page session="false" import="java.util.HashMap,java.util.Map,com.big.search.maryjane.persistence.dynamodb.DomainManager" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>

<!DOCTYPE html>

<%
String domain = "ASK BOOKMAN";
if(request.getHeader("Host")!=null && request.getHeader("Host").contains("weed")){
  domain = "ASK WEEDMAN";
}
%>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title><%=domain%></title>

            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>
    </head>

    <body>
      <div class="container">
        <div class="jumbotron bg-white mt-3">
          <h1><%=domain%></h1>
          <jsp:include page="getDomainsContent.jsp" /> 
      </div>
      </div>
    </body>
      <!-- The form -->

</html>
