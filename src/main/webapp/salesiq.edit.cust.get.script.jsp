<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="java.util.Vector,com.salesiq.dom.SiqCallback,com.salesiq.mysql.login.ScriptManager,com.salesiq.dom.SiqScript,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
              <%
                ScriptManager tmpMgr = new ScriptManager();
                SiqScript tmpCust = tmpMgr.getScript((String)session.getAttribute("uid"),request.getParameter("sc_id"), request.getParameter("cust_id"));
              %>
              <input type="hidden" id="cust_id" name="cust_id" value="<%=request.getParameter("cust_id")%>">
              <input type="hidden" id="sc_id" name="sc_id" value="<%=request.getParameter("sc_id")%>">
              <input type="hidden" id="activeScriptsName" name="activeScriptsName" value="<%=tmpCust.getName()%>">
              <div class="form-group">
                <label for="exampleInputPhone">Script</label>
                <textarea class="form-control" id="used_script" name="used_script" rows="4"><%=tmpCust.getScript_merged()%></textarea>
              </div>
              <input type="radio" name="medium" value="email" />Email
              <input type="radio" name="medium" value="sms" />SMS
              <input type="radio" name="medium" value="phoneoutbound" checked />Phone Outbound
              <input type="radio" name="medium" value="phoneinbound" />Phone Inbound
              <button type="submit" class="btn btn-primary">Submit</button>
