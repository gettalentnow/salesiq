<%--
Licensed to the Apache Software Foundation (ASF) under one or more
contributor license agreements.  See the NOTICE file distributed with
this work for additional information regarding copyright ownership.
The ASF licenses this file to You under the Apache License, Version 2.0
(the "License"); you may not use this file except in compliance with
the License.  You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
--%>
<%@ page session="true" import="com.salesiq.ops.ProcessLeads,com.salesiq.mysql.login.CustomerManager,java.util.Map,com.gtn.search.cloudsearch.AWSGTNBasicSearch,java.util.HashMap,com.gtn.search.ABDomainExists,com.gtn.search.WebSiteItem,com.gtn.search.SearchGTN" pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="ASKBOOKMAN.com internet search engine">
        <meta name="keywords" content="internet search engine">
        <title>Sales IQ</title>

            <link rel="stylesheet" href="css/all.css" >
            <link rel="stylesheet" href="css/examples.css" >
            <link rel="stylesheet" href="css/bootstrap.min.css" >
            <link rel="stylesheet" href="css/jquery-ui.css" >
            <script src="js/jquery-1.10.2.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/handlebars.js"></script>
            <script src="js/typeahead.bundle.js"></script>
            <script src="js/examples.js"></script>


            <script>
            var myVar ;

            async function procCustomers() {
              myVar = setInterval(function(){procCustomersLine();}, 4000);
            }
            async function procCustomersLine() {

              if(document.getElementById("customers").value.trim().length < 1){
                stopProcCustomers();
                return;
              }
              var lines = document.getElementById("customers").value.split('\n');
              var newCusList = '';
              for(var i = 1;i < lines.length;i++){
                newCusList += lines[i] + '\n';
              }
              document.getElementById("customers").value = newCusList;
              updateCustomerReq(lines[0]);
            }
            function stopProcCustomers() {
              clearInterval(myVar);
            }
            function updateCustomerReq(customerString){
                  //alert(customerString);
                  //alert("Ran agains 1");
        					var xmlhttp;
        					if (window.XMLHttpRequest)
        					  {// code for IE7+, Firefox, Chrome, Opera, Safari
        					  xmlhttp=new XMLHttpRequest();
        					  }
        					else
        					  {// code for IE6, IE5
        					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        					  }
        					xmlhttp.onreadystatechange=function()
        					  {
        					  if (xmlhttp.readyState==4 && xmlhttp.status==200 )
        					    {
                         document.getElementById("httpRes").innerHTML = xmlhttp.responseText;
        					    	 //urlStringPrev = "NEW SEARCH";
                    		 //document.getElementById("location").value = locationName;
        						     //document.getElementById(gps).innerHTML = "<img src=\"images/notification_done.png\" width=\"14px\" />";
        					    }
        					  }
                  document.getElementById("httpRes").innerHTML =  customerString;
        					xmlhttp.open("GET", "salesiq.upload.cust.insert.jsp?cs=" + customerString ,true);
        					xmlhttp.send();
          	}
            </script>
            <script>
              function loadPage() {
                  logoutFunction();
              }    
          </script>            
          <jsp:include page='signin.javascript.include.jsp' />
  </head>

  <body onload="loadPage()">


              <jsp:include page='navbar_top_siq.jsp' />

      <div class="container">
        <div class="jumbotron bg-white mt-3">
          <%
              String leads = "";
              if(request.getParameter("customersRAW")!=null){
                  ProcessLeads psLeads = new ProcessLeads();
                  leads = psLeads.processLeads(request.getParameter("customersRAW"), "UTF-8");
              }
          %>

                                          <div id="content" style="text-align: left">
              <h1>Add Customers Form</h1>
              <a href="salesiq.menu.jsp" >Menu</a><BR>
              Format: <code>CUSTOMER NAME %2B PHONE %2B None|Hot|Warn|Cold %2B notes</code><BR>
              Example: <code>John Smith %2B 555 555 5555 %2B None %2B likes MMA</code><BR>
              <div id="httpRes" ></div>
              <form class="example" >
                  <div class="form-group">
                    <label for="exampleFormControlTextarea1">Multiple Customers Upload Form</label>
                    <textarea class="form-control" id="customers" rows="10"><%=leads%></textarea>
                  </div>
              </form>
              <button type="submit" class="btn btn-danger" onclick="stopProcCustomers()">Stop</button>
              <button type="submit" class="btn btn-success" onclick="procCustomers()">Start</button>

                                </div>
                                <div id="content" style="text-align: left">
                                  <HR>

              <h1>Load Raw Data</h1>
              <form  class="example" action="salesiq.upload.cust.jsp" method="post">
                  <div class="form-group">
                    <textarea class="form-control" name="customersRAW" id="customersRAW" rows="20"></textarea>
                  </div>
                  <button type="submit" class="btn btn-danger" >Fix Data</button>
              </form>

                                </div>

            </div>
    </div>




    </body>
      <!-- The form -->

</html>
