package com.salesiq.mysql.login;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.UUID;
import java.util.Vector;

import com.salesiq.dom.SiqCustomer;
import com.salesiq.dom.SiqStat;

public class StatsManager {

	public StatsManager() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StatsManager tmpMgr = new StatsManager();
		tmpMgr.addStat("fa0bd40c-eb36-481b-ae4f-6e44373fd233");
		Vector<SiqStat> tmplist = tmpMgr.getStats("fa0bd40c-eb36-481b-ae4f-6e44373fd233");
		for(int j=0;j<tmplist.size();j++){
			SiqStat tmpcb = tmplist.elementAt(j);
			System.out.println("HELL O " + tmpcb.getCust_tot());
			System.out.println("HELL O " + tmpcb.getTs());
		}
	}
	public String addStat(String user_id){
		SiqStat newStat = new SiqStat();
		//newStat.setBus_id(bus_id);
		newStat.setUser_id(user_id);
	    try
	    {
		      MySQLConstants tmpCon = new MySQLConstants();
		      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
	          Class.forName("com.mysql.jdbc.Driver").newInstance();
		      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
		      String query = "select count(*) as tot_count from siq_customers where siq_customers.siq_user_id = ? order by ts desc";
		      PreparedStatement preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, user_id);
		      ResultSet resultSet = preparedStmt.executeQuery();
		      while (resultSet.next()) {
		    	  newStat.setCust_tot(resultSet.getInt("tot_count"));
		      }
		      query = "select count(*) as tot_count from siq_customers where siq_customers.siq_user_id = ? and siq_customers.siq_cust_status = ? order by ts desc";
		      preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, user_id);
		      preparedStmt.setString (2, "None");
		      resultSet = preparedStmt.executeQuery();
		      while (resultSet.next()) {
		    	  newStat.setCust_tot_none(resultSet.getInt("tot_count"));
		      }
		      query = "select count(*) as tot_count from siq_customers where siq_customers.siq_user_id = ? and siq_customers.siq_cust_status = ? order by ts desc";
		      preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, user_id);
		      preparedStmt.setString (2, "Hot");
		      resultSet = preparedStmt.executeQuery();
		      while (resultSet.next()) {
		    	  newStat.setCust_tot_hot(resultSet.getInt("tot_count"));
		      }
		      query = "select count(*) as tot_count from siq_customers where siq_customers.siq_user_id = ? and siq_customers.siq_cust_status = ? order by ts desc";
		      preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, user_id);
		      preparedStmt.setString (2, "Warm");
		      resultSet = preparedStmt.executeQuery();
		      while (resultSet.next()) {
		    	  newStat.setCust_tot_warm(resultSet.getInt("tot_count"));
		      }
		      query = "select count(*) as tot_count from siq_customers where siq_customers.siq_user_id = ? and siq_customers.siq_cust_status = ? order by ts desc";
		      preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, user_id);
		      preparedStmt.setString (2, "Cold");
		      resultSet = preparedStmt.executeQuery();
		      while (resultSet.next()) {
		    	  newStat.setCust_tot_cold(resultSet.getInt("tot_count"));
		      }
		      query = "select count(*) as tot_count from siq_callbacks where siq_callbacks.siq_user_id = ? ";
		      preparedStmt = conn.prepareStatement(query);
		      preparedStmt.setString (1, user_id);
		      resultSet = preparedStmt.executeQuery();
		      while (resultSet.next()) {
		    	  newStat.setCust_tot_cb(resultSet.getInt("tot_count"));
		      }
		      resultSet.close();
		      preparedStmt.close();
		      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error " + e.getMessage();
	      return "Error: System Error ";
	    }
	    return insertStat(newStat);
	}
	public String insertStat(SiqStat newStat){
	    try
	    {

	      java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Calendar.getInstance().getTime().getTime());
	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = " insert into siq_cust_totals (siq_cust_tot_id, siq_user_id, siq_bus_id, ts, total_cust, "
	      		+ "total_cust_none, total_cust_hot, total_cust_warm, total_cust_cold, total_cb)"
	        + " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, UUID.randomUUID().toString());
	      preparedStmt.setString (2, newStat.getUser_id());
	      preparedStmt.setString (3, newStat.getBus_id());
	      preparedStmt.setTimestamp(4, currentTimestamp);
	      preparedStmt.setInt(5, newStat.getCust_tot());
	      preparedStmt.setInt(6, newStat.getCust_tot_none());
	      preparedStmt.setInt(7, newStat.getCust_tot_hot());
	      preparedStmt.setInt(8, newStat.getCust_tot_warm());
	      preparedStmt.setInt(9, newStat.getCust_tot_cold());
	      preparedStmt.setInt(10, newStat.getCust_tot_cb());
	      preparedStmt.execute();
	      preparedStmt.close();
	      conn.close();

	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error " + e.getMessage();
	      return "Error: System Error ";
	    }
		return "Customer Added";
	}
	public Vector<SiqStat> getStats(String siq_user_id){
		Vector<SiqStat> custList = new Vector<SiqStat>();
	    try
	    {
	      MySQLConstants tmpCon = new MySQLConstants();
	      String myUrl = "jdbc:mysql://salesiq.couvxu0um0yq.us-east-2.rds.amazonaws.com/salesiq";
          Class.forName("com.mysql.jdbc.Driver").newInstance();
	      Connection conn = DriverManager.getConnection(myUrl, "admin", tmpCon.getPassword());
	      String query = "select * from siq_cust_totals where siq_cust_totals.siq_user_id = ? order by ts desc";
	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	      preparedStmt.setString (1, siq_user_id);
	      ResultSet resultSet = preparedStmt.executeQuery();
	      while (resultSet.next()) {
	    	  SiqStat tmpCust = new SiqStat();
	    	  tmpCust.setCust_tot(resultSet.getInt("total_cust"));
	    	  tmpCust.setCust_tot_cold(resultSet.getInt("total_cust_cold"));
	    	  tmpCust.setCust_tot_hot(resultSet.getInt("total_cust_hot"));
	    	  tmpCust.setCust_tot_none(resultSet.getInt("total_cust_none"));
	    	  tmpCust.setCust_tot_warm(resultSet.getInt("total_cust_warm"));
	    	  tmpCust.setCust_tot_cb(resultSet.getInt("total_cb"));
	    	  tmpCust.setTs(resultSet.getTimestamp("ts"));
	    	  custList.add(tmpCust);
	      }
	      resultSet.close();
	      preparedStmt.close();
	      conn.close();
	    }
	    catch (Exception e)
	    {
	      System.err.println("Got an exception!");
	      System.err.println(e.getMessage());
	      //return e.getMessage();
	      //return "Error: System Error";
	    }
		return custList;
	}
}
