package com.big.search.maryjane.crawler.selenium.social;
import java.util.ArrayList;
/*
 *  TWITTER BOT base class performs common task for each bot
 *  Armen Merikyan
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.big.search.maryjane.persistence.dynamodb.UserManagerYoutube;
import com.big.search.maryjane.persistence.dynamodb.VideoManager;
import com.big.search.maryjane.persistence.dynamodb.VideoManagerOther;

public abstract class WebsiteCrawlerYoutubeBase {
	private WebDriver driver = null;

	private UserManagerYoutube usrMngr = null;
	private VideoManager vidMngr = null;
	private VideoManagerOther vidMngrOther = null;
	private HashMap<String, String> usrMap = null;
	private ArrayList<String> videoMap = null;
	private ArrayList<String> videoMapOther = null;
	private String baseUrl = "https://youtube.com";
	private String currentVideo ;

	public WebsiteCrawlerYoutubeBase() {
		// TODO Auto-generated constructor stub
    	System.setProperty("webdriver.gecko.driver", Constant.driverLocation);
		
	}
	
	public void getStart(){
		this.setUsrMngr(new UserManagerYoutube());
		this.setVidMngr(new VideoManager());
		this.setUsrMap(this.getUsrMngr().getAllUsers());
		this.setVideoMap(this.getVidMngr().getAll());
		
	    for (Map.Entry<String,String> entry : this.getUsrMap().entrySet())  {	
		    	this.setDriver(new FirefoxDriver());
		    	this.getLogin(entry.getKey(), entry.getValue());
		    	for (int i = 0; i < this.getVideoMap().size(); i++) {	
			    	System.out.println("PROCESSING USER " + entry.getKey());
			    	currentVideo = this.getVideoMap().get(i);
			    	this.getURLContent();	    
			    }	
	    		this.getDriver().close();
	    }
	}
	public void getStartOther(){
		this.setUsrMngr(new UserManagerYoutube());
		this.setVidMngrOther(new VideoManagerOther());
		this.setUsrMap(this.getUsrMngr().getAllUsers());
		this.setVideoMapOther(this.getVidMngrOther().getAll());
	    for (Map.Entry<String,String> entry : this.getUsrMap().entrySet())  {	
	    	this.setDriver(new FirefoxDriver());
	    	this.getLogin(entry.getKey(), entry.getValue());
	    	for (int i = 0; i < this.getVideoMapOther().size(); i++) {	
		    	System.out.println("PROCESSING USER " + entry.getKey());
		    	currentVideo = this.getVideoMapOther().get(i);
		    	this.getURLContent();	    
		    }	
    		this.getDriver().close();
	    }		
	}
	public void getStartOnce(){
		this.setUsrMngr(new UserManagerYoutube());
		this.setVidMngr(new VideoManager());
		this.setUsrMap(this.getUsrMngr().getAllUsers());
		this.setVideoMap(this.getVidMngr().getAll());
		
	    for (Map.Entry<String,String> entry : this.getUsrMap().entrySet())  {	
		    	this.setDriver(new FirefoxDriver());
		    	this.getLogin(entry.getKey(), entry.getValue());
		    	System.out.println("PROCESSING USER " + entry.getKey());
		    	currentVideo = this.getVideoMap().get(0);
		    	this.getURLContent();	
	    		this.getDriver().close();
	    }
	}	
	public void getStartOnly(){
		this.setVidMngr(new VideoManager());
		this.setVideoMap(this.getVidMngr().getAll());
    	this.setDriver(new FirefoxDriver()); 
    	for (int i = 0; i < this.getVideoMap().size(); i++) {	
	    	currentVideo = this.getVideoMap().get(i);
	    	this.getURLContent();	    
	    }	
		this.getDriver().close();	    
	}	
	abstract public void getURLContent();
	
	public void getDownTonight(){
		try{
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };	                
	        WebDriverWait wait = new WebDriverWait(this.getDriver(), 30);
	        wait.until(pageLoadCondition);
	        JavascriptExecutor jse = (JavascriptExecutor)this.getDriver();
	        for(int j=0;j<Constant.scrlCount;j++){
	        	jse.executeScript("window.scrollBy(0,250)", "");
	        	Thread.sleep(100);
		        System.out.println(j);
			}	
		}catch(Exception ex){
			ex.printStackTrace();
		}	
	}
	
    public void getLogin( String username, String password) {
    	System.out.println(this.getBaseUrl());
		try{
			
	    	this.driver.get(this.getBaseUrl()+ "/signin");  		        		    	
	    	
	        ExpectedCondition<Boolean> pageLoadCondition = new
	                ExpectedCondition<Boolean>() {
	                    public Boolean apply(WebDriver driver) {
	                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
	                    }
	                };
	                
	        WebDriverWait wait = new WebDriverWait(this.driver, 60);
	        wait.until(pageLoadCondition);
	        // wait ten seconds for Capcha test to manually bypass
	        Thread.sleep(1000);		        

	        List<WebElement> element = this.driver.findElements(By.tagName("input"));	
	   	 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("name"));
		        	if(temp!=null && temp.getAttribute("name")!=null && temp.getAttribute("name").startsWith("identifier")){	       		    	        
		        			temp.sendKeys(username);
		        	        Thread.sleep(1000);		        
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }

	        wait.until(pageLoadCondition);
	        element = this.driver.findElements(By.tagName("span"));	
		   	 
	        for (WebElement temp : element) {
				try{
	        		System.out.println(temp.getAttribute("type"));
		        	if(temp!=null && temp.getText()!=null && temp.getText().startsWith("Next")){	       		    	        
		        			temp.click();
		        			break;
		        	}	 
		        } catch (Exception expected) {
		        	expected.printStackTrace();
		        }	
	        }	        
	         wait.until(pageLoadCondition);  	         
	         Thread.sleep(5000);	
	         element = this.driver.findElements(By.tagName("input"));	
		   	 
		        for (WebElement temp : element) {
					try{
		        		System.out.println(temp.getAttribute("name"));
			        	if(temp!=null && temp.getAttribute("name")!=null && temp.getAttribute("name").startsWith("password")){	       		    	        
			        			temp.sendKeys(password);
			        	        Thread.sleep(1000);		        
			        	}	 
			        } catch (Exception expected) {
			        	expected.printStackTrace();
			        }	
		        }	 

		        element = this.driver.findElements(By.tagName("span"));	
		        for (WebElement temp : element) {
					try{
		        		System.out.println(temp.getAttribute("type"));
			        	if(temp!=null && temp.getText()!=null && temp.getText().startsWith("Next")){	       		    	        
			        			temp.click();
			        			break;
			        	}	 
			        } catch (Exception expected) {
			        	expected.printStackTrace();
			        }	
		        }		         
		         Thread.sleep(20000);	
		}catch(Exception ex){
			ex.printStackTrace();
		}
    }
	public WebDriver getDriver() {
		return driver;
	}
	public void setDriver(WebDriver driver) {
		this.driver = driver;
	}
	public UserManagerYoutube getUsrMngr() {
		return usrMngr;
	}
	public void setUsrMngr(UserManagerYoutube usrMngr) {
		this.usrMngr = usrMngr;
	}
	public HashMap<String, String> getUsrMap() {
		return usrMap;
	}
	public void setUsrMap(HashMap<String, String> usrMap) {
		this.usrMap = usrMap;
	}
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String getCurrentVideo() {
		return currentVideo;
	}

	public void setCurrentVideo(String currentVideo) {
		this.currentVideo = currentVideo;
	}

	public ArrayList<String> getVideoMap() {
		return videoMap;
	}

	public void setVideoMap(ArrayList<String> videoMap) {
		this.videoMap = videoMap;
	}

	public VideoManager getVidMngr() {
		return vidMngr;
	}

	public void setVidMngr(VideoManager vidMngr) {
		this.vidMngr = vidMngr;
	}

	public VideoManagerOther getVidMngrOther() {
		return vidMngrOther;
	}

	public void setVidMngrOther(VideoManagerOther vidMngrOther) {
		this.vidMngrOther = vidMngrOther;
	}

	public ArrayList<String> getVideoMapOther() {
		return videoMapOther;
	}

	public void setVideoMapOther(ArrayList<String> videoMapOther) {
		this.videoMapOther = videoMapOther;
	}
}
